/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      spacing: {
        "89%": "89%",
        "92%": "92%",
      },
      colors: {
        ngreen: {
          100: "#d1f5ea",
          200: "#a3ebd5",
          300: "#75e1c1",
          400: "#47d7ac",
          500: "#3aab94",
          600: "#2d807b",
          700: "#205463",
        },
        nblue: {
          100: "#8893a5",
          200: "#4e5e78",
          300: "#13294b",
          400: "#06041f",
          500: "#2E008B",
        },
        ngray: {
          100: "#eff1f4",
          200: "#e0e3e8",
          300: "#e0e3e8",
          400: "#e0e3e8",
        },
        npurple: {
          100: "#cbbfe2",
          200: "#9680c5",
          300: "#6240a8",
          400: "#2e008b",
        },
        nyellow: {
          100: "#fdf4dc",
          200: "#fcebb8",
          300: "#fce195",
          400: "#fbd872",
          500: "#fab46d",
          600: "#f99068",
        },
        npink: {
          100: "#fcd0d7",
          200: "#fba3af",
          300: "#f97586",
          400: "#f8485e",
          500: "#bf4059",
          600: "#863854",
          700: "#4c3150",
        },
        nsilver: {
          100: "#717f93",
        },
      },
    },
  },
  plugins: [],
};
