import { Outlet, createBrowserRouter, RouterProvider } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import { ConfirmProvider } from "material-ui-confirm";
import {
  LoginPage,
  HomePage,
  AssessmentPage,
  ProfilePage,
  AssessmentHistoryPage,
  AccessDenied,
  ClientListPage,
  ErrorPage,
  AllClientsPage,
  ClientAssessmentHistoryPage,
} from "./pages";
import { AuthRequired, Header } from "./components";
import { AuthProvider } from "./hooks";
import { ThankYou } from "./pages/ThankYou";
import { Role } from "./typings";
import "@fontsource/inter";
import { useState } from "react";
import { DiscoveryThankyou } from "./pages/DiscoveryThankyou";

const App = () => {
  const [showLogin, setShowLogin] = useState<boolean>(false);
  const [refreshHeader, setRefreshHeader] = useState<boolean>(false);
  const [clientId, setClientId] = useState<string>("");

  const AppLayout = () => {
    return (
      <>
        <Header showLogin={showLogin} refreshHeader={refreshHeader} />
        <ConfirmProvider>
          <Outlet />
        </ConfirmProvider>
        <Toaster
          toastOptions={{
            success: {
              style: {
                background: "#d1f5ea",
              },
              iconTheme: {
                primary: "#47d7ac",
                secondary: "#fff",
              },
            },
            error: {
              style: {
                background: "#fcd0d7",
              },
            },
          }}
        />
      </>
    );
  };

  const router = createBrowserRouter([
    {
      path: "",
      element: (
        <AuthProvider>
          <AppLayout />
        </AuthProvider>
      ),
      children: [
        {
          path: "/",
          element: <HomePage setShowLogin={setShowLogin} />,
        },
        {
          path: "/login",
          element: <LoginPage setShowLogin={setShowLogin} />,
        },
        {
          path: "/home",
          element: <HomePage setShowLogin={setShowLogin} />,
        },
        {
          path: "/clients",
          element: (
            <AuthRequired roles={[Role.ADMIN]}>
              <AllClientsPage setClientId={setClientId} />
            </AuthRequired>
          ),
        },
        {
          path: "/all-assessments",
          element: (
            <AuthRequired roles={[Role.ADMIN]}>
              <ClientListPage />
            </AuthRequired>
          ),
        },
        {
          path: "/assessment-history",
          element: (
            <AuthRequired roles={[Role.USER, Role.ADMIN]}>
              <AssessmentHistoryPage />
            </AuthRequired>
          ),
        },
        {
          path: "/client-assessment-history",
          element: (
            <AuthRequired roles={[Role.ADMIN]}>
              <ClientAssessmentHistoryPage clientId={clientId} />
            </AuthRequired>
          ),
        },
        {
          path: "/assessment",
          element: (
            <AuthRequired roles={[Role.USER, Role.ADMIN]}>
              <AssessmentPage />
            </AuthRequired>
          ),
        },
        {
          path: "/profile",
          element: (
            <AuthRequired roles={[Role.USER]}>
              <ProfilePage
                setRefreshHeader={setRefreshHeader}
                refreshHeader={refreshHeader}
              />
            </AuthRequired>
          ),
        },
        {
          path: "/thank-you",
          element: (
            <AuthRequired roles={[Role.USER]}>
              <ThankYou />
            </AuthRequired>
          ),
        },
        {
          path: "/discovery-thank-you",
          element: (
            <AuthRequired roles={[Role.USER]}>
              <DiscoveryThankyou />
            </AuthRequired>
          ),
        },
        {
          path: "/unauthorized",
          element: <AccessDenied />,
        },
      ],
      errorElement: <ErrorPage />,
    },
  ]);
  return <RouterProvider router={router} />;
};

export { App };
