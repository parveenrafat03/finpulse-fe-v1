import { useState } from "react";
import "quill/dist/quill.snow.css";
import ReactQuill from "react-quill";

const TextEditor = (props: any) => {
  const { onDataFromChild, initialText } = props;

  const [text, setText] = useState(initialText || "");
  const handleProcedureContentChange = (content: any) => {
    setText(content);
    onDataFromChild(content);
  };

  var modules = {
    toolbar: [
      ["bold", "italic", "underline", "strike"],
      [{ list: "ordered" }, { list: "bullet" }],
    ],
  };

  var formats = [
    "header",
    "height",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "color",
    "bullet",
    "indent",
    "link",
    "image",
    "align",
    "size",
  ];

  return (
    <div>
      <div>
        <ReactQuill
          theme="snow"
          modules={modules}
          formats={formats}
          placeholder="write your content ...."
          onChange={handleProcedureContentChange}
          style={{ height: "220px", width: "100%" }}
          value={text}
        ></ReactQuill>
      </div>
    </div>
  );
};

export default TextEditor;
