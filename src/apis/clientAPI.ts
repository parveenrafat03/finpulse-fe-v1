import toast from "react-hot-toast";
import { IQuestionAnswerMapping } from "../utils/questionaire";
import { IClientProfile } from "../pages/Profile";
import {
  AssessmentStatus,
  IAssessmentDetails,
} from "../interfaces/assessment-details.interface";
import axios from "axios";

export const getConfiguration = async () => {
  try {
    const response = await fetch(`configuration.json`, {
      method: "GET",
    });
    if (response.ok) {
      return await response.json();
    }
  } catch (error) {
    console.log(error);
  }
  return {};
};

export const ClientAPI = {
  saveProfile: async (data: IClientProfile): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(API_URL.saveProfile, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      toast.error("Failed to update the client info!");
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        toast.error("Failed to update the client info!");
      } else {
        toast.success("Client info saved successfully!");
        return responseData;
      }
    }
  },
  getClientProfile: async (clientId: string): Promise<IClientProfile> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(`${API_URL.getClientProfile}${clientId}`);
    const responseData = await response.json();
    if (responseData.statusCode !== 200) {
      //toast.error("Failed to read the client info!");
      // return {
      //   client_id: "",
      //   client_name: "",
      //   customer_name: "",
      //   email_address: "",
      //   locations: "",
      //   potential_application_of_cloud: "",
      //   customer_role: "",
      //   contact_no: "",
      //   last_updated_on: "",
      // };
      return null;
    } else {
      const data = responseData.body.success as IClientProfile;
      return data;
    }
  },
  getClientsList: async (): Promise<IClientProfile[]> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(API_URL.getClientsList);
    const responseData = await response.json();
    if (responseData.statusCode !== 200) {
      toast.error("Failed to read the client info!");
      return [] as IClientProfile[];
    } else {
      return responseData.body.success as IClientProfile[];
    }
  },
  fetchAssessmentsHistory: async (
    clientId: string
  ): Promise<IAssessmentDetails[]> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/getassessment/history/${clientId}`
      `${API_URL.fetchAssessmentsHistory}${clientId}`
    );
    if (!response.ok) {
      return [] as IAssessmentDetails[];
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        // toast.error("Apologies! Failed to fetch Assessments");
        return [] as IAssessmentDetails[];
      } else {
        return responseData.body.response_data as IAssessmentDetails[];
      }
    }
  },
  fetchMaturityAssessmentQuestions: async (): Promise<unknown> => {
    const response = await fetch(`maturity_assessment_survey.json`, {
      method: "GET",
    });
    if (!response.ok) {
      return [];
    } else {
      return response.json();
    }
  },
  fetchDiscoveryAssessmentQuestions: async (): Promise<unknown> => {
    const response = await fetch(`discovery_assessment_survey.json`, {
      method: "GET",
    });
    if (!response.ok) {
      return [];
    } else {
      return response.json();
    }
  },
  initiateMaturitySurvey: async (clientId: string) => {
    const API_URL = (await getConfiguration()).api;
    const dt = new Date()
      .toLocaleDateString("en-US", { month: "short", year: "numeric" })
      .replace(" ", "_");
    const response = await fetch(API_URL.initiateMaturitySurvey, {
      method: "POST",
      body: JSON.stringify({
        client_id: clientId,
        assessment_name: `Assessment_${dt}`,
        assessment_status: AssessmentStatus.IN_PROGRESS,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      toast.error("Failed to start a new assessment!");
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        toast.error("Failed to start a new assessment!");
      } else {
        toast.success("A new assessment is created");
        return responseData.body.assessment_id;
      }
    }
  },
  saveMaturitySurvey: async (
    payload: IMaturitySurveyPayload
  ): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/submitsurvey/response`,
      API_URL.saveMaturitySurvey,
      {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      return [];
    } else {
      return response.json();
    }
  },
  getCapabilityScores: async (
    payload: IGetScoresPayload
  ): Promise<any | unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(API_URL.getCapabilityScores, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      return [];
    } else {
      const responseData = await response.json();
      return responseData.body;
    }
  },
  deleteRecommendation: async (
    payload: IDeleteRecommendation
  ): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/delete/recommendation`,
      API_URL.deleteRecommendation,
      {
        method: "DELETE",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      return [];
    } else {
      return response.json();
    }
  },
  fetchMaturitySurveyResponse: async (
    clientId: string,
    assessmentId: string
  ): Promise<ISurveyResponse> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/getsurvey/responses/${clientId}/${assessmentId}`,
      `${API_URL.fetchMaturitySurveyResponse}${clientId}/${assessmentId}`,
      {
        method: "GET",
      }
    );
    if (!response.ok) {
      return {
        survey_responses: [],
      };
    } else {
      const responseData = await response.json();
      return responseData.body as ISurveyResponse;
    }
  },
  saveDiscoverySurvey: async (
    payload: IMaturitySurveyPayload
  ): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/submitdiscovery/survey`,
      `${API_URL.saveDiscoverySurvey}`,
      {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      return [];
    } else {
      return response.json();
    }
  },
  fetchDiscoverySurveyResponse: async (
    clientId: string
  ): Promise<ISurveyResponse> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/getdiscovery/survey/${clientId}`,
      `${API_URL.fetchDiscoverySurveyResponse}${clientId}`,
      {
        method: "GET",
      }
    );
    if (!response.ok) {
      return {
        survey_responses: [],
      };
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        return {
          assessment_status: AssessmentStatus.IN_PROGRESS,
          survey_responses: [],
        };
      } else {
        return responseData.body as Promise<ISurveyResponse>;
      }
    }
  },
  fetchRecommendations: async (
    assessmentId: string
  ): Promise<IRecommendationResponse[]> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/get/recommendations/${assessmentId}`,
      `${API_URL.fetchRecommendations}${assessmentId}`,
      {
        method: "GET",
      }
    );
    if (!response.ok) {
      return [] as unknown as IRecommendationResponse[];
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        //toast.error("Failed to fetch recommendation!");
        return [] as unknown as IRecommendationResponse[];
      }
      return responseData.body.response as IRecommendationResponse[];
    }
  },
  saveRecommendation: async (data: any): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/submit/recommendation`,
      `${API_URL.saveRecommendation}`,
      {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      toast.error("Failed to save recommendation!");
    } else {
      toast.success("Recommendation info saved successfully!");
      return response.json();
    }
  },
  updateRecommendation: async (data: any): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/update/edit-recommendation`,
      `${API_URL.updateRecommendation}`,
      {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      toast.error("Failed to update recommendation!");
    } else {
      toast.success("Recommendation info updated successfully!");
      return response.json();
    }
  },
  deleteSurveyResponse: async (assessment_id: string): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const payload = {
      assessment_id: assessment_id,
    };
    const response = await fetch(
      // `${CONFIG.API_BASE_PATH}/delete_survey/response`,
      `${API_URL.deleteSurveyResponse}`,
      {
        method: "DELETE",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      toast.error("Failed to delete assessment!");
      return [];
    } else {
      toast.success("Assessment deleted successfully!");
      return response.json();
    }
  }, //CORS =>
  submitTargetScore: async (payload: ITargetScorePayload): Promise<unknown> => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(API_URL.submitTargetScore, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      toast.error("Failed to save target score!");
    } else {
      toast.success("Target score saved successfully!");
      return response.json();
    }
  },
  generateReport: async (clientId: string, assessmentId: string) => {
    const API_URL = (await getConfiguration()).api;

    // Define the request body
    const requestBody = {
      client_id: "" + clientId,
      assessment_id: "" + assessmentId,
    };

    await axios
      .post(API_URL.generateReport, requestBody, {
        responseType: "blob", // Important to treat the response as a Blob
      })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const contentDisposition = response.headers["content-disposition"];
        console.log("Content-Disposition:", contentDisposition);
        let filename = "download.pdf"; // Default file name
        if (contentDisposition) {
          const filenameRegex = /filename="([^"]+)"/;
          const matches = filenameRegex.exec(contentDisposition);
          if (matches != null && matches[1]) {
            filename = matches[1];
            console.log("Extracted filename:", filename);
          }
        }
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        toast.success("Report generated successfully!");
      })
      .catch((error) => {
        console.error("Error generating report:", error);
        toast.error("Failed to generate Report!");
      });
  },
};

export interface ITargetScorePayload {
  client_id: string;
  assessment_id: string;
  capability_id: string;
  capability_target_score: string;
}

export interface IMaturitySurveyPayload {
  client_id: string;
  assessment_id: string;
  surveyresponses: IQuestionAnswerMapping[];
  assessment_status: AssessmentStatus;
}

export interface IGetScoresPayload {
  client_id: string;
  assessment_id: string;
}
export interface IDeleteRecommendation {
  client_id: string;
  assessment_id: string;
  capability_id: string; // replace
  recommendation_number: number;
}

export interface ISurveyResponse {
  survey_responses: IQuestionAnswerMapping[];
  assessment_status?: AssessmentStatus;
  discovery_last_modified_date?: string;
}

export interface IRecommendationResponse {
  assessment_id: string;
  capability_id: string;
  client_id: string;
  last_modified_date: string;
  recommendation: string;
  recommendation_number: string;
}

interface INewAssessment {
  assessment_id: string;
}
