import { faWarning } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const AccessDenied = () => (
  <div className="w-10/12 lg:w-1/2 mx-auto mt-5 md:mt-8 lg:mt-14 text-center rounded-lg border-t-8 border-npink-400 bg-white p-5 md:px-14 md:py-11">
    <FontAwesomeIcon icon={faWarning} className="text-3xl text-npink-500" />
    <h1 className="text-3xl mt-5">Access Denied!</h1>
    <p className="mt-5 text-xm mb-5">
      You are trying to access a page which is restricted for the current role.
    </p>
    <a href="/" className="text-ngreen-500 underline">
      Go to Home
    </a>
  </div>
);

export { AccessDenied };
