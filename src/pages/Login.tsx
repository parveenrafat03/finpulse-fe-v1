import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { withLoader } from "../components";
import { FormContext, useAuth, useForm } from "../hooks";
import { IAuthContextType, ILoginForm } from "../hooks/useAuth";
import { IWithLoaderProps } from "../components/withLoader";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import iconCircleExclamation from "../assets/icons/icon-circle-exclamation.svg";
import "../pages-css/login.css";

interface LoginProps {
  setShowLogin: (value: boolean) => void;
}

type ComplexProps = LoginProps & IWithLoaderProps;

const loginFormSchema: yup.ObjectSchema<ILoginForm> = yup.object({
  username: yup.string().required(),
  password: yup.string().required(),
});

const Login: React.FC<ComplexProps> = ({ setShowLogin, showLoader }) => {
  // Variables: To holding states
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState<string>("");
  const [passwordError, setPasswordError] = useState<string>("");
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const navigate = useNavigate();
  const auth: IAuthContextType = useAuth();

  // Function: To validate username on change
  const handleUsernameChange = (e: any) => {
    setUsername(e.target.value);
    if (e.target.value.length == 0) {
      setUsernameError("Username is required");
    } else {
      setUsernameError("");
    }
  };

  // Function: To validate password on change
  const handlePasswordChange = (e: any) => {
    setPassword(e.target.value);
    if (e.target.value.length == 0) {
      setPasswordError("Password is required");
    } else {
      setPasswordError("");
    }
  };

  // Function: To show/hide password
  const handleTogglePassword = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  // Function: To attempt login on submit
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    showLoader?.({
      show: true,
      message: "Logging you in..",
    });
    if (validation()) {
      await auth.login({
        username: username,
        password: password,
      });
    }
    showLoader?.({
      show: false,
    });
  };

  // Function: To move back to home page
  const handleBack = async (e: any) => {
    e.preventDefault();
    navigate("/");
  };

  // Function: To validate user credentials for inaccuracy
  const validation = (): boolean => {
    var response = true;
    if (username == undefined || username == null || username.length == 0) {
      setUsernameError("Username is required");
      response = false;
    }
    if (password == undefined || password == null || password.length == 0) {
      setPasswordError("Password is required");
      response = false;
    }
    return response;
  };

  useEffect(() => {
    setShowLogin(false);
    if (auth.client) {
      navigate("/");
    }
  }, [auth, showLoader]);

  return (
    <div className="login-container">
      <div className="login-card rounded-lg border-t-8 border-ngreen-400 bg-white">
        <div className="title">Login</div>
        <div className="info">
          <img src={iconCircleExclamation} style={{ display: "inline" }} />{" "}
          Please use the username and password that you have received on your
          email
        </div>
        <form onSubmit={handleSubmit}>
          <div className="input-container">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              id="username"
              value={username}
              onChange={handleUsernameChange}
              placeholder="Enter your username"
              autoComplete="off"
            />
            <span>{usernameError}</span>
          </div>
          <div className="input-container">
            <label htmlFor="password">Password</label>
            <input
              type={showPassword ? "text" : "password"}
              id="password"
              value={password}
              onChange={handlePasswordChange}
              placeholder="Enter your password"
              autoComplete="off"
            />
            <div className="toggle-password">
              <button type="button" onClick={handleTogglePassword} className="">
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </button>
            </div>
            <span>{passwordError}</span>
          </div>
          <div className="input-menu">
            <button onClick={handleBack} className="btn-normal">
              Go Back
            </button>
            <button type="submit" className="btn-success">
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const LoginPage = withLoader(Login);

export { LoginPage };
