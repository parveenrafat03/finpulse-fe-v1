import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../hooks";
import IconSort from "../assets/icons/icon-sort.svg";
import { IWithLoaderProps, withLoader } from "../components/withLoader";
import "../pages-css/clientAssessmentHistory.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowLeft,
  faCircle,
  faCircleCheck,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { AssessmentStatus, IAssessmentDetails } from "../interfaces";
import { IClientProfile } from "./Profile";
import { ClientAPI } from "../apis";
import { ISurveyResponse } from "../apis/clientAPI";
import { getFormattedDate } from "../utils/assessment_utils";
import axios from "axios";

interface ClientAssessmentHistoryProps {
  clientId: string;
}

type ComplexProps = ClientAssessmentHistoryProps & IWithLoaderProps;

const ClientAssessmentHistory: React.FC<ComplexProps> = ({
  clientId,
  showLoader,
}) => {
  const [sortOrder, setSortOrder] = useState<string>("ASC");
  const [clientInfo, setClientInfo] = useState<IClientProfile>(null);
  const [discoveryAssessments, setDiscoveryAssessments] =
    useState<IAssessmentDetails[]>();
  const [maturityAssessments, setMaturityAssessments] =
    useState<IAssessmentDetails[]>();
  const auth = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    const sessionCheck = () => {
      if (!auth.isSessionValid()) {
        auth.logout();
      }
    };
    sessionCheck();
    auth.markLastActivity();
    const intervalId = setInterval(sessionCheck, 15 * 1000);
    return () => clearInterval(intervalId);
  }, []);

  const goBackToAllClients = () => {
    navigate("/clients");
  };

  const moveToViewDiscovery = () => {
    navigate("/assessment?type=discovery");
  };

  const sortByStartDate = () => {
    auth.markLastActivity();
    var data = maturityAssessments;
    if (sortOrder === "ASC") {
      data.sort((a, b) => a.created_date.localeCompare(b.created_date));
    } else {
      data.sort((a, b) => b.created_date.localeCompare(a.created_date));
    }
    setMaturityAssessments(data);
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  const sortByLastUpdated = () => {
    auth.markLastActivity();
    var data = maturityAssessments;
    if (sortOrder === "ASC") {
      data.sort((a, b) =>
        a.last_modified_date.localeCompare(b.last_modified_date)
      );
    } else {
      data.sort((a, b) =>
        b.last_modified_date.localeCompare(a.last_modified_date)
      );
    }
    setMaturityAssessments(data);
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  const generateReport = (assessmentId: string) => {
    auth.markLastActivity();
    const apiURL = "http://52.2.201.27:5003/create-dynamic-pdf"; // Replace with your actual API URL
    showLoader?.({ show: true, message: "Generating Report.." });
    const clientId: string = auth.client ? auth.client.client_id : "";

    // Define the request body
    const requestBody = {
      client_id: "" + clientId,
      assessment_id: "" + assessmentId,
    };

    axios
      .post(apiURL, requestBody, {
        responseType: "blob", // Important to treat the response as a Blob
      })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const contentDisposition = response.headers["content-disposition"];
        console.log("Content-Disposition:", contentDisposition);
        let filename = "download.pdf"; // Default file name
        if (contentDisposition) {
          const filenameRegex = /filename="([^"]+)"/;
          const matches = filenameRegex.exec(contentDisposition);
          if (matches != null && matches[1]) {
            filename = matches[1];
            console.log("Extracted filename:", filename);
          }
        }
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        showLoader?.({ show: false });
      })
      .catch((error) => {
        console.error("Error generating report:", error);
        showLoader?.({ show: false });
      });
  };

  useEffect(() => {
    void (async () => {
      if (clientId === "") {
        navigate("/clients");
      }
      try {
        showLoader?.({ show: true, message: "Checking assessments status.." });
        //auth.setClientId(clientId);
        const [clientInfo, discoverySurvey, maturitySurveys]: [
          IClientProfile,
          ISurveyResponse,
          IAssessmentDetails[],
        ] = await Promise.all([
          ClientAPI.getClientProfile(clientId),
          ClientAPI.fetchDiscoverySurveyResponse(clientId),
          ClientAPI.fetchAssessmentsHistory(clientId),
        ]);
        setClientInfo(clientInfo);
        const assessmentDetail: IAssessmentDetails = {
          client_id: clientId,
          assessment_id: "",
          assessment_name: "",
          assessment_status: discoverySurvey.assessment_status!,
          created_date: new Date().toISOString(),
          last_modified_date: new Date().toISOString(),
        };
        setDiscoveryAssessments([assessmentDetail]);
        maturitySurveys.sort((a, b) =>
          b.assessment_id.localeCompare(a.assessment_id)
        );
        maturitySurveys.forEach((value, index) => {
          maturitySurveys[index].previousAssessmentId =
            index < maturitySurveys.length - 1
              ? maturitySurveys[index + 1].assessment_id
              : "0";
        });
        setMaturityAssessments(maturitySurveys);
        showLoader?.({ show: false });
      } catch (error) {
        console.log(error);
        showLoader?.({ show: false });
      }
    })();
  }, [clientId, showLoader]);

  return (
    <div className="cah-page-container w-[89%]">
      <div className="cah-header">
        <div className="cah-header-left">
          <FontAwesomeIcon
            icon={faArrowLeft}
            className="cah-back"
            onClick={goBackToAllClients}
          />
          <div className="cah-page-title">{clientInfo?.client_name}</div>
        </div>
        <div className="cah-header-right">
          <button
            className="cah-view-discovery-report"
            onClick={moveToViewDiscovery}
          >
            View Discovery Report
          </button>
        </div>
      </div>
      <div className="cah-info">
        <div className="cah-info-left">
          <span>{clientInfo?.email_address}</span>
          <span className="cah-separator"></span>
          <span>{clientInfo?.customer_name}</span>
          <span className="cah-separator"></span>
          <span>{clientInfo?.locations}</span>
        </div>
        <div className="cah-info-right"></div>
      </div>
      <div className="cah-title">Maturity Assessments</div>
      <div className="cah-content">
        {maturityAssessments?.length === 0 ? (
          <div className="cah-no-data">No data found</div>
        ) : (
          <div className="cah-layout-list">
            <div className="cah-list-row cah-theader">
              <div className="col-3">Name</div>
              <div className="col-2">
                Start Date <img src={IconSort} onClick={sortByStartDate} />
              </div>
              <div className="col-2">
                Last Updated <img src={IconSort} onClick={sortByLastUpdated} />
              </div>
              <div className="col-2">Status</div>
              <div className="col-2">Action</div>
            </div>
            {maturityAssessments?.map(
              (assessment: IAssessmentDetails, index: number) => (
                <div
                  key={index + 1}
                  className={
                    "cah-list-row x-" +
                    (index % 2 === 0 ? "list-row-odd" : "list-row-even")
                  }
                >
                  <div className="cah-list-col col-3">
                    {assessment.assessment_name}
                  </div>
                  <div className="cah-list-col col-2">
                    <span>{getFormattedDate(assessment.created_date)}</span>
                  </div>
                  <div className="cah-list-col col-2">
                    <span>
                      {assessment.last_modified_date
                        ? getFormattedDate(assessment.last_modified_date)
                        : ""}
                    </span>
                  </div>
                  <div className="cah-list-col col-2">
                    {assessment.assessment_status ===
                    AssessmentStatus.IN_PROGRESS ? (
                      <div className="inprogress">
                        <FontAwesomeIcon
                          icon={faCircle}
                          className="text-nyellow-600"
                        />{" "}
                        In Progress
                      </div>
                    ) : assessment.assessment_status ===
                      AssessmentStatus.DONE ? (
                      <div className="submitted">
                        <FontAwesomeIcon
                          icon={faCircleCheck}
                          className="text-ngreen-600"
                        />{" "}
                        Submitted
                      </div>
                    ) : (
                      <div className="completed">
                        <FontAwesomeIcon icon={faStar} />
                        Completed
                      </div>
                    )}
                  </div>
                  <div className="cah-list-col col-2 cah-actions">
                    {assessment.assessment_status ===
                    AssessmentStatus.COMPLETED ? (
                      <button
                        className="cah-action-navigate"
                        onClick={() => {
                          generateReport(assessment.assessment_id);
                        }}
                      >
                        View Report
                      </button>
                    ) : assessment.assessment_status ===
                      AssessmentStatus.DONE ? (
                      <button
                        className="cah-action-navigate"
                        onClick={() => {
                          navigate(
                            "/assessment?type=maturity&id=" +
                              assessment.assessment_id +
                              "&pid=" +
                              assessment.previousAssessmentId
                          );
                        }}
                      >
                        {assessment.assessment_status === AssessmentStatus.DONE
                          ? "Add Recommendations"
                          : "View Response"}
                      </button>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              )
            )}
          </div>
        )}
      </div>
    </div>
  );
};

const ClientAssessmentHistoryPage = withLoader(ClientAssessmentHistory);

export { ClientAssessmentHistoryPage };
