import { useState, useEffect, useMemo } from "react";
import { Section } from "../components/Section";
import { SectionQuestions } from "../components/SectionQuestions";
import { ISection, ResponseType } from "../interfaces/section.interface";
import { ClientAPI } from "../apis";
//import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Tabs, Tab, Slider } from "@mui/material";
import axios from "axios";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch, { SwitchProps } from "@mui/material/Switch";
import Checkbox from "@mui/material/Checkbox";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { faCircle as solidCircle } from "@fortawesome/free-solid-svg-icons/faCircle";
import { styled } from "@mui/material/styles";

import {
  IQuestionAnswerMapping,
  allSectionsCompleted,
  getFirstPendingSection,
  getQuestionAnswerMappingForSection,
  getQuestionParentSectionMapping,
} from "../utils/questionaire";
import {
  IDeleteRecommendation,
  IMaturitySurveyPayload,
  ISurveyResponse,
} from "../apis/clientAPI";
import { IWithLoaderProps, withLoader } from "../components/withLoader";
import {
  ISectionToActivate,
  SectionType,
  isActiveSectionFinal,
  nextSection,
  previousSection,
} from "../utils/section_util";
import { useAuth } from "../hooks";
import { ConfirmSubmission } from "../components/ConfirmSubmission";
import { usePrompt } from "../hooks";
import { AssessmentStatus, AssessmentType } from "../interfaces";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faStar } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { Role } from "../typings";
import { IClientProfile } from "./Profile";
import {
  IAssessmentDetails,
  ICapabilityScore,
  IDomain,
  IRecommendation,
  ISurveyDetails,
} from "../interfaces/assessment-details.interface";
import { TabPanel } from "./TabPanel";
import { marks } from "./constants";
import { formatDate, getDate, getTime } from "../utils/assessment_utils";
import "../pages-css/assessment.css";
import { CustomSlider } from "../components/customSlider";
import SectionRecommend from "../components/SectionRecommend";

const Assessment = ({ showLoader }: IWithLoaderProps) => {
  function useQuery() {
    const { search } = useLocation();
    return useMemo(() => new URLSearchParams(search), [search]);
  }
  const query = useQuery();
  const assessmentType = query.get("type") as AssessmentType;
  const assessmentId = query.get("id");
  const previousAssessmentId = query.get("pid");
  const activeSectionId = query.get("asid");
  const auth = useAuth();
  const navigate = useNavigate();
  const clientId: string = auth.client ? auth.client.client_id : "";

  const [selectedTab, setSelectedTab] = useState(0);
  const [sectionsChanged, setSectionsChanged] = useState(false);
  const [sectionItems, setSectionItems] = useState<ISection[]>([]);
  const [activeMainSection, setActiveMainSection] = useState<ISection>(
    sectionItems[0]
  );
  const [activeSubSection, setActiveSubSection] = useState<ISection>();
  const [isDirty, setIsDirty] = useState(false);
  const [displayConfirmSubmissionDialog, setDisplayConfirmSubmissionDialog] =
    useState(false);
  const [submitAttempted, setSubmitAttempted] = useState(false);
  const [assessmentStatus, setAssessmentStatus] = useState<AssessmentStatus>();
  const [maturityAssessments, setMaturityAssessments] =
    useState<IAssessmentDetails[]>();
  const [recommendations, setRecommendations] = useState<IRecommendation[]>();
  const [scores, setScores] = useState<ICapabilityScore[]>([]);
  const [previousScores, setPreviousScores] = useState<ICapabilityScore[]>([]);
  const [dataFromChild, setDataFromChild] = useState<string | null>(null);
  const [editClicked, setEditClicked] = useState<number | null>(null);
  const [isEditorOpen, setIsEditorOpen] = useState(false);
  const [currentSectionId, setCurrentSectionId] = useState<string | null>(null);

  const [surveyDetails, setSurveyDetails] = useState<ISurveyDetails>();
  const [clientInfo, setClientInfo] = useState<IClientProfile>();
  const [assessmentDT, setAssessmentDT] = useState<string>();
  const [showThankYou, setShowThankYou] = useState<boolean>(false);

  const capabilityArray: ICapabilityScore[] = [];
  const previousCapabilityArray: ICapabilityScore[] = [];

  useEffect(() => {
    const sessionCheck = () => {
      if (!auth.isSessionValid()) {
        auth.logout();
      }
    };
    sessionCheck();
    auth.markLastActivity();
    const intervalId = setInterval(sessionCheck, 15 * 1000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    void (async () => {
      showLoader?.({ show: true, message: "Loading..." });
      const isMaturity: boolean = assessmentType == AssessmentType.MATURITY;

      // Check to stop user from Cross User Access Attempt
      if (
        isMaturity &&
        assessmentId !== null &&
        !assessmentId.startsWith(clientId)
      ) {
        navigate("/", { replace: true });
      }

      try {
        // Assessment Initial Actions
        let question_api: Promise<ISection[]>;
        let response_api: Promise<ISurveyResponse>;
        const clientInfo: IClientProfile =
          await ClientAPI.getClientProfile(clientId);
        setClientInfo(clientInfo);

        if (isMaturity) {
          // Maturity Assessment Initial Actions

          // Fetch All Maturity Assessments
          const maturitySurveys =
            await ClientAPI.fetchAssessmentsHistory(clientId);
          setMaturityAssessments(maturitySurveys);

          // Fetch Maturity Assessment Details
          let surveyDetails = maturitySurveys.find(
            (survey) => survey.assessment_id == assessmentId
          );
          if (surveyDetails == null) {
            navigate("/", { replace: true });
          }
          setSurveyDetails(surveyDetails);

          // Maturity Assessment Question/Answers
          question_api =
            ClientAPI.fetchMaturityAssessmentQuestions() as Promise<ISection[]>;
          response_api = ClientAPI.fetchMaturitySurveyResponse(
            clientId,
            assessmentId
          );

          if (auth.client.roles === Role.ADMIN) {
            // Fetch Recommendation
            var recommendations =
              await ClientAPI.fetchRecommendations(assessmentId);
            setRecommendations(recommendations);

            // Fetch Score
            let body = {
              client_id: clientId,
              assessment_id: assessmentId,
            };
            const scores = await ClientAPI.getCapabilityScores(body);
            scores.domain_scores.forEach((domain: IDomain) => {
              domain.Domain_Capability_Scores.forEach(
                (capability: ICapabilityScore) => {
                  capabilityArray.push({
                    Capability_ID: capability.Capability_ID,
                    Capability_Score: capability.Capability_Score,
                  });
                }
              );
            });
            setScores(capabilityArray);

            // Fetch Previous Score
            if (previousAssessmentId !== "0") {
              let body = {
                client_id: clientId,
                assessment_id: previousAssessmentId,
              };
              const previousScores = await ClientAPI.getCapabilityScores(body);
              previousScores.domain_scores.forEach((domain: IDomain) => {
                domain.Domain_Capability_Scores.forEach(
                  (capability: ICapabilityScore) => {
                    previousCapabilityArray.push({
                      Capability_ID: capability.Capability_ID,
                      Capability_Score: capability.Capability_Score,
                    });
                  }
                );
              });
            }
            setPreviousScores(previousCapabilityArray);
          }

          console.log("MATURITY");
          console.log(surveyDetails);
        } else {
          // Discovery Assessment Initial Actions

          // Discovery Assessment Question/Answers
          question_api =
            ClientAPI.fetchDiscoveryAssessmentQuestions() as Promise<
              ISection[]
            >;
          response_api = ClientAPI.fetchDiscoverySurveyResponse(clientId);

          console.log("DISCOVERY");
        }

        // Setup Sections and Responses
        const sections = await question_api;
        const response = await response_api;
        setAssessmentStatus(response.assessment_status);
        setAssessmentDT(response.discovery_last_modified_date);
        isMaturity
          ? bindMaturityResponse(sections, response)
          : bindDiscoveryResponse(sections, response);
        setSectionItems(sections);
        setActiveMainSection(
          sections[activeSectionId ? parseInt(activeSectionId) - 1 : 0]
        );
      } catch (e) {
        console.log(e);
      }

      showLoader?.({ show: false });
    })();
  }, []);

  const [firstLoading, setFirstLoading] = useState<boolean>(true);
  useEffect(() => {
    showLoader?.({ show: true, message: "Loading Questions ..." });
    if (firstLoading && activeSectionId == null) {
      navigateToPendingSection();
      //setFirstLoading(false);
    } else if (
      nonNullable(activeMainSection) &&
      activeMainSection.subSections?.length &&
      !sectionsChanged
    ) {
      setActiveSubSection(activeMainSection.subSections[0]);
    }
    showLoader?.({ show: false });
  }, [activeMainSection, sectionsChanged]);

  useEffect(() => {
    showLoader?.({
      show: true,
      message: "Fetching Assessment details..",
    });
    setTimeout(() => {
      setFirstLoading(false);
    }, 3000);
  }, []);

  const bindDiscoveryResponse = (
    sections: ISection[],
    discoveryResponse: ISurveyResponse
  ) => {
    const questionSectionMap = getQuestionParentSectionMapping(sections);
    if (Array.isArray(discoveryResponse.survey_responses)) {
      discoveryResponse.survey_responses.forEach((question) => {
        const sectionsIds = questionSectionMap.get(question.question_id);
        if (sectionsIds) {
          const matchingSection = sections.find((s) => s.id === sectionsIds[0]);
          const matchingQuestion = matchingSection?.questions?.find(
            (q) => q.id === question.question_id
          );
          if (matchingQuestion) {
            if (
              matchingQuestion.responseType === ResponseType.FREE_TEXT ||
              matchingQuestion.responseType === ResponseType.SINGLE_OPTION
            ) {
              matchingQuestion.response = question.response;
            } else {
              matchingQuestion.response = question.response?.split(",");
            }
          }
        }
      });
    }
  };

  const bindMaturityResponse = (
    sections: ISection[],
    maturityResponse: ISurveyResponse
  ) => {
    const questionSectionMap = getQuestionParentSectionMapping(sections);
    if (Array.isArray(maturityResponse.survey_responses)) {
      maturityResponse.survey_responses.forEach((question) => {
        const sectionsIds = questionSectionMap.get(question.question_id);
        if (sectionsIds) {
          const parentSection = sections.find((s) => s.id === sectionsIds[0]);
          if (parentSection?.subSections) {
            const subSection = parentSection.subSections.find(
              (s) => s.id === sectionsIds[1]
            );
            const matchingQuestion = subSection?.questions?.find(
              (q) => q.id === question.question_id
            );
            if (matchingQuestion) {
              matchingQuestion.response = question.response;
            }
          }
        }
      });
    }
  };

  const nonNullable = (section: ISection | null | undefined) => {
    return section !== null && section !== undefined;
  };

  const onActiveSubSectionClicked = (clickedSubSection: ISection) => {
    auth.markLastActivity();
    setShowThankYou(false);
    saveAndGoToSection(clickedSubSection, SectionType.SUB);
  };
  const onActiveMainSectionClicked = (clickedMainSection: ISection) => {
    auth.markLastActivity();
    setShowThankYou(false);
    saveAndGoToSection(clickedMainSection, SectionType.MAIN);
  };

  const saveAndGoToSection = (section: ISection, sectionType: SectionType) => {
    if (isDirty) {
      saveSurvey()
        .then(() => {
          setIsDirty(false);
          showLoader?.({
            show: false,
          });
        })
        .catch((err) => {
          showLoader?.({
            show: false,
          });
          console.log("error", err);
        })
        .finally(() => {
          if (sectionType === SectionType.MAIN) {
            setActiveMainSection(section);
          } else {
            setActiveSubSection(section);
          }
          window.scrollTo(0, 0);
        });
    } else {
      if (sectionType === SectionType.MAIN) {
        setActiveMainSection(section);
      } else {
        setActiveSubSection(section);
      }
      window.scrollTo(0, 0);
    }
  };

  const onSectionStateChange = () => {
    setIsDirty(true);
    setSectionItems(sectionItems.slice());
  };

  const exit = () => {
    //console.log(auth.client);
    if (auth.client.roles === Role.ADMIN) {
      navigate("/client-assessment-history", { replace: true });
    } else if (
      assessmentType === AssessmentType.DISCOVERY &&
      (assessmentStatus === AssessmentStatus.IN_PROGRESS ||
        assessmentStatus === null)
    ) {
      navigate("/home", { replace: true });
    } else {
      navigate("/assessment-history", { replace: true });
    }
  };

  const back = () => {
    auth.markLastActivity();
    const prevSectionInfo: ISectionToActivate | undefined = previousSection(
      sectionItems,
      activeMainSection,
      activeSubSection
    );
    if (prevSectionInfo) {
      prevSectionInfo.type === SectionType.MAIN
        ? setActiveMainSection(prevSectionInfo.section)
        : setActiveSubSection(prevSectionInfo.section);
    }
  };

  const next = () => {
    auth.markLastActivity();
    const nextSectionInfo: ISectionToActivate | undefined = nextSection(
      sectionItems,
      activeMainSection,
      activeSubSection
    );
    if (nextSectionInfo) {
      nextSectionInfo.type === SectionType.MAIN
        ? setActiveMainSection(nextSectionInfo.section)
        : setActiveSubSection(nextSectionInfo.section);
      window.scrollTo(0, 0);
    }
  };

  const saveAndNext = () => {
    auth.markLastActivity();
    saveSurvey()
      .then(() => {
        setIsDirty(false);
        showLoader?.({
          show: false,
        });
        next();
      })
      .catch((err) => {
        showLoader?.({
          show: false,
        });
        console.log("error", err);
      });
  };

  const saveAndDirectToPendingSection = () => {
    auth.markLastActivity();
    if (isDirty) {
      saveSurvey()
        .then(() => {
          setIsDirty(false);
          showLoader?.({
            show: false,
          });
        })
        .catch((err) => {
          showLoader?.({
            show: false,
          });
          console.log("error", err);
        })
        .finally(() => {
          navigateToPendingSection();
        });
    } else {
      navigateToPendingSection();
    }
  };

  const handleTabChange = (event, newValue) => {
    auth.markLastActivity();
    setSelectedTab(newValue);
  };

  const navigateToPendingSection = () => {
    auth.markLastActivity();
    var pendingMainSection = getFirstPendingSection(sectionItems);
    if (pendingMainSection == undefined) {
      pendingMainSection = sectionItems[0];
    }
    setActiveMainSection(pendingMainSection);
    var pendingSubSection = getFirstPendingSection(
      pendingMainSection?.subSections ?? []
    );
    if (pendingMainSection?.subSections && pendingSubSection == undefined) {
      pendingSubSection = pendingMainSection?.subSections[0];
    }
    if (pendingSubSection) {
      setTimeout(() => {
        setActiveSubSection(pendingSubSection);
      }, 100);
    }
    window.scrollTo(0, 0);
  };

  const submitSurvey = async () => {
    auth.markLastActivity();
    showLoader?.({
      show: true,
      message: "Submitting survey response..",
    });
    setDisplayConfirmSubmissionDialog(false);
    await saveSurvey(AssessmentStatus.DONE)
      .then(() => {
        setIsDirty(false);
        showLoader?.({
          show: false,
        });
        setShowThankYou(true);
        setAssessmentStatus(AssessmentStatus.DONE);

        // const path =
        //   assessmentType === AssessmentType.DISCOVERY
        //     ? "/discovery-thank-you"
        //     : "/thank-you";
        // const state = { enable: true };
        // navigate(path, { state, replace: true });
      })
      .catch((err) => {
        showLoader?.({
          show: false,
        });
        console.log("error", err);
      });
  };

  const saveSurvey = async (
    status = AssessmentStatus.IN_PROGRESS
  ): Promise<unknown> => {
    auth.markLastActivity();
    const mapping: IQuestionAnswerMapping[] = [];
    sectionItems.forEach((section) => {
      if (section.subSections?.length) {
        section.subSections.forEach((subSection) => {
          const subSectionMapping =
            getQuestionAnswerMappingForSection(subSection);
          mapping.push(...subSectionMapping);
        });
      }
      if (section.questions?.length) {
        const questionMapping = getQuestionAnswerMappingForSection(section);
        mapping.push(...questionMapping);
      }
    });
    showLoader?.({
      show: true,
      message: "Saving survey response..",
    });
    const clientId: string = auth.client ? auth.client.client_id : "";
    const surveyData: IMaturitySurveyPayload = {
      client_id: clientId,
      assessment_id: query.get("id")!,
      surveyresponses: mapping,
      assessment_status: status,
    };

    const saveAPI =
      assessmentType === AssessmentType.MATURITY
        ? await ClientAPI.saveMaturitySurvey(surveyData)
        : await ClientAPI.saveDiscoverySurvey(surveyData);

    return saveAPI;
  };

  usePrompt({ when: isDirty });

  const generateReport = () => {
    auth.markLastActivity();
    saveSurvey(AssessmentStatus.COMPLETED);
    const apiURL = "http://52.2.201.27:5003/create-dynamic-pdf"; // Replace with your actual API URL
    showLoader?.({ show: true, message: "Generating Report.." });
    const clientId: string = auth.client ? auth.client.client_id : "";

    // Define the request body
    const requestBody = {
      client_id: "" + clientId,
      assessment_id: "" + assessmentId,
    };

    axios
      .post(apiURL, requestBody, {
        responseType: "blob", // Important to treat the response as a Blob
      })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const contentDisposition = response.headers["content-disposition"];
        console.log("Content-Disposition:", contentDisposition);
        let filename = "download.pdf"; // Default file name
        if (contentDisposition) {
          const filenameRegex = /filename="([^"]+)"/;
          const matches = filenameRegex.exec(contentDisposition);
          if (matches != null && matches[1]) {
            filename = matches[1];
            console.log("Extracted filename:", filename);
          }
        }
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        showLoader?.({ show: false });
      })
      .catch((error) => {
        console.error("Error generating report:", error);
        showLoader?.({ show: false });
      });
  };

  const handleChildData = (data: string) => {
    auth.markLastActivity();
    setDataFromChild(data);
  };

  const openEditor = (section: ISection) => {
    auth.markLastActivity();
    setIsEditorOpen(true);
    setCurrentSectionId(section.id);
    const containsObject = (obj, subarr) =>
      subarr.some((subObj) => subObj.id === obj.id);
    const mainSection = sectionItems.find((obj) =>
      containsObject(section, obj.subSections)
    );
    setActiveMainSection(mainSection);
    setActiveSubSection(section);
    setSectionsChanged(!sectionsChanged);
  };

  const handleEdit = (sectionId: string, recommendationIndex: number) => {
    auth.markLastActivity();
    setEditClicked(recommendationIndex);
    setCurrentSectionId(sectionId);
    setIsEditorOpen(true);
  };

  const handleDelete = async (
    recommendationNumber: string,
    capability_id: string
  ) => {
    auth.markLastActivity();
    // let body = {
    //   Client_ID: clientId,
    //   Capability_Id: capability_id,
    //   Assessment_ID: assessmentId,
    //   Recommendation_Number: +recommendationNumber,
    // };
    let body: IDeleteRecommendation = {
      client_id: clientId,
      assessment_id: assessmentId,
      capability_id: capability_id,
      recommendation_number: +recommendationNumber,
    };
    await ClientAPI.deleteRecommendation(body);
    var updatedRecommendations = await ClientAPI.fetchRecommendations(
      assessmentId ? assessmentId : ""
    );

    if (!Array.isArray(updatedRecommendations)) {
      updatedRecommendations = [];
    }
    setRecommendations(updatedRecommendations);
  };

  const closeEditor = () => {
    auth.markLastActivity();
    setIsEditorOpen(false);
    setCurrentSectionId(null);
    setEditClicked(null);
  };

  const saveRecommendation = async (id: string) => {
    auth.markLastActivity();
    const newRecommendation = {
      client_id: clientId,
      capability_id: id,
      assessment_id: assessmentId,
      recommendation: dataFromChild,
    };
    await ClientAPI.saveRecommendation(newRecommendation);
    var updatedRecommendations = await ClientAPI.fetchRecommendations(
      assessmentId ? assessmentId : ""
    );
    if (!Array.isArray(updatedRecommendations)) {
      updatedRecommendations = [];
    }
    setRecommendations(updatedRecommendations);
    closeEditor();
  };

  const updateRecommendation = async (
    id: string,
    recommendation_number: string | number
  ) => {
    auth.markLastActivity();
    const newRecommendation = {
      client_id: clientId,
      capability_id: id,
      assessment_id: assessmentId,
      recommendation_number: +recommendation_number,
      recommendation: dataFromChild,
    };
    await ClientAPI.updateRecommendation(newRecommendation);
    var updatedRecommendations = await ClientAPI.fetchRecommendations(
      assessmentId ? assessmentId : ""
    );

    if (!Array.isArray(updatedRecommendations)) {
      updatedRecommendations = [];
    }
    setRecommendations(updatedRecommendations);
    closeEditor();
  };

  const statusText =
    assessmentStatus === AssessmentStatus.COMPLETED
      ? "Completed"
      : assessmentStatus === AssessmentStatus.DONE
      ? "Submitted"
      : "In Progress";

  const IOSSwitch = styled((props: SwitchProps) => (
    <Switch
      focusVisibleClassName=".Mui-focusVisible"
      disableRipple
      {...props}
    />
  ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    "& .MuiSwitch-switchBase": {
      padding: 0,
      margin: 2,
      transitionDuration: "300ms",
      "&.Mui-checked": {
        transform: "translateX(16px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          backgroundColor:
            theme.palette.mode === "dark" ? "#2ECA45" : "#47D7AC",
          opacity: 1,
          border: 0,
        },
        "&.Mui-disabled + .MuiSwitch-track": {
          opacity: 0.5,
        },
      },
      "&.Mui-focusVisible .MuiSwitch-thumb": {
        color: "#33cf4d",
        border: "6px solid #fff",
      },
      "&.Mui-disabled .MuiSwitch-thumb": {
        color:
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: theme.palette.mode === "light" ? 0.7 : 0.3,
      },
    },
    "& .MuiSwitch-thumb": {
      boxSizing: "border-box",
      width: 22,
      height: 22,
    },
    "& .MuiSwitch-track": {
      borderRadius: 26 / 2,
      backgroundColor: theme.palette.mode === "light" ? "#8893A5" : "#39393D",
      opacity: 1,
      transition: theme.transitions.create(["background-color"], {
        duration: 500,
      }),
    },
  }));

  const [isDivVisible, setDivVisibility] = useState(false);

  const toggleVisibility = () => {
    auth.markLastActivity();
    setDivVisibility(!isDivVisible);
  };

  const handleNewAssessmentClick = () => {
    void (async () => {
      showLoader?.({ show: true });
      auth.markLastActivity();
      const newAssessmentId = await ClientAPI.initiateMaturitySurvey(clientId);
      navigate(`/assessment?type=maturity&id=${newAssessmentId}`);
      showLoader?.({ show: false });
    })();
  };

  return (
    nonNullable(activeMainSection) && (
      <>
        <div
          className={
            "" +
            (auth.client?.roles === Role.USER ||
            assessmentType === AssessmentType.DISCOVERY
              ? " pt-6 pb-1"
              : "hidden")
          }
        >
          {/* USER => Discovery Assessment */}
          <div className="w-[89%] m-auto flex">
            <div className="inline pr-5 ">
              <div className="font-bold inline text-xl">
                {clientInfo?.client_name} -{" "}
              </div>
              <div className=" inline text-base">
                {assessmentType === AssessmentType.DISCOVERY
                  ? "Discovery Questions"
                  : "Assessment Questions"}
              </div>
            </div>
            <div className="inline assessment-status-button text-base font-normal ">
              {assessmentStatus === AssessmentStatus.COMPLETED ? (
                <FontAwesomeIcon
                  icon={faStar}
                  className="text-npurple-400 mr-1 w-3"
                />
              ) : assessmentStatus === AssessmentStatus.DONE ? (
                <FontAwesomeIcon
                  icon={checkCircle}
                  className="text-ngreen-600 mr-1 w-3"
                />
              ) : (
                <FontAwesomeIcon
                  icon={solidCircle}
                  className="text-nyellow-600 bg-white mr-1 w-2"
                />
              )}
              {statusText}
            </div>
            <div
              className={
                "inline  ml-auto " +
                (assessmentType === AssessmentType.MATURITY ? "" : "hidden")
              }
            >
              <FormControlLabel
                control={
                  <IOSSwitch
                    defaultChecked={isDivVisible}
                    onChange={toggleVisibility}
                    sx={{ m: 1 }}
                  />
                }
                // control={<IOSSwitch sx={{ m: 1 }} defaultChecked />}
                label="Show All Descriptions"
              />
            </div>
          </div>
        </div>
        <div
          className={
            "border-b-2 w-[89%] m-auto mt-6 " +
            (auth.client?.roles === Role.USER ||
            assessmentType === AssessmentType.DISCOVERY
              ? "hidden"
              : "")
          }
        >
          {/* USER => Maturity Assessment */}
          <div className="">
            <div className="back-link">
              <Link
                to={
                  auth.client.roles === Role.ADMIN
                    ? "/client-assessment-history"
                    : "/assessment-history"
                }
              >
                <div className="text-2xl font-semibold text-nblue-300 pt-6">
                  <FontAwesomeIcon
                    icon={faArrowLeft}
                    className="mr-3 text-sm pb-1"
                  />
                  {surveyDetails?.assessment_name}
                </div>
              </Link>
            </div>

            <div className="admin-maturity-status bg-white  rounded-md flex mt-6 p-6 align-middle mb-8">
              <div className="text-base font-semibold">
                {clientInfo?.client_name}
              </div>
              <FontAwesomeIcon
                icon={solidCircle}
                className="text-nblue-200 bg-white px-4 mt-1 w-2 text-sm"
              />
              <div className="text-base font-normal">
                Last Updated:{" "}
                {assessmentType === "maturity"
                  ? getDate(formatDate(surveyDetails?.last_modified_date))
                  : assessmentDT}
              </div>
              <FontAwesomeIcon
                icon={solidCircle}
                className="text-nblue-200 bg-white w-2 mt-1  px-4"
              />
              <div
                className={
                  "border rounded-2xl px-2 pt-1 text-xs font-extrabold  " +
                  // (surveyDetails?.assessment_status === AssessmentStatus.DONE
                  (assessmentStatus === AssessmentStatus.COMPLETED
                    ? "text-npurple-400 border-npurple-400"
                    : assessmentStatus === AssessmentStatus.DONE
                    ? "text-ngreen-600 border-ngreen-600"
                    : "text-nyellow-600 border-nyellow-600")
                }
              >
                {assessmentStatus === AssessmentStatus.COMPLETED ? (
                  <FontAwesomeIcon
                    icon={faStar}
                    className="text-npurple-400 mr-1 w-3"
                  />
                ) : assessmentStatus === AssessmentStatus.DONE ? (
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 mr-1 w-3 "
                  />
                ) : (
                  <FontAwesomeIcon
                    icon={solidCircle}
                    className="text-nyellow-600 bg-white mr-1 w-2"
                  />
                )}
                {statusText}
              </div>
            </div>

            <Tabs
              value={selectedTab}
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              variant="standard"
              TabIndicatorProps={{
                style: {
                  backgroundColor: "#2D807B",
                },
              }}
            >
              <Tab
                label="Assessment Responses"
                style={{
                  color: selectedTab == 0 ? "#2D807B" : "#4E5E78",
                  textTransform: "capitalize",
                  fontWeight: selectedTab == 0 ? "600" : "200",
                  fontSize: "16px",
                  lineHeight: "24px",
                  fontFamily: "Inter",
                }}
              />
              {assessmentType == "maturity" &&
                auth.client?.roles === Role.ADMIN && (
                  <Tab
                    label="Recommendations"
                    style={{
                      color: selectedTab == 0 ? "#4E5E78" : "#2D807B",
                      textTransform: "capitalize",
                      fontWeight: selectedTab == 0 ? "200" : "600",
                      fontSize: "16px",
                      lineHeight: "24px",
                      fontFamily: "Inter",
                    }}
                  />
                )}
            </Tabs>
          </div>
        </div>
        <div>
          <>
            <TabPanel value={selectedTab} index={0}>
              <div className="md:px-0 w-[92%] m-auto">
                <div className="flex -mt-2">
                  {displayConfirmSubmissionDialog && (
                    <ConfirmSubmission
                      onCancel={() => {
                        setDisplayConfirmSubmissionDialog(false);
                      }}
                      onSubmit={submitSurvey}
                    />
                  )}

                  <div
                    className={
                      "shadow-lg section-navigation px-10 rounded-lg border-t-8 border-ngreen-400 pt-8" +
                      (assessmentType === AssessmentType.MATURITY
                        ? ""
                        : " h-fit py-6 ")
                    }
                  >
                    <ol
                      //className="elative border-l border-ngray-300 h-10"
                      className={
                        "elative border-l border-ngray-300 h-12" +
                        (assessmentType === AssessmentType.MATURITY ||
                        auth.client?.roles === Role.ADMIN
                          ? " hidden "
                          : "")
                      }
                    >
                      <li className="mb-8 ml-6 ">
                        <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                          <FontAwesomeIcon
                            icon={checkCircle}
                            className="text-ngreen-600"
                          />
                        </span>
                        <div className="mb-1 text-lg font-normal cursor-pointer false">
                          <div className="flex section-option">
                            <div className="w-72 text-ngreen-600 font-bold">
                              <Link to="/profile">Company Information</Link>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ol>
                    <Section
                      submitAttempted={submitAttempted}
                      activeSectionId={
                        activeSectionId == null
                          ? activeMainSection.id
                          : activeSectionId
                      }
                      activeSubSectionId={activeSubSection?.id}
                      sectionItems={sectionItems}
                      isSubsection={false}
                      onActiveSubSectionClicked={onActiveSubSectionClicked}
                      onActiveMainSectionClicked={onActiveMainSectionClicked}
                    />
                  </div>
                  <div
                    className={
                      "w-full ml-10 shadow-lg rounded-lg border-t-8 border-ngreen-400 section-content" +
                      (assessmentType === AssessmentType.MATURITY
                        ? ""
                        : "-discovery")
                    }
                  >
                    {showThankYou ? (
                      assessmentType == AssessmentType.DISCOVERY ? (
                        <div className="">
                          <div className="text-4xl text-ngreen-700 font-black py-4">
                            Awesome!
                          </div>
                          <div className="text-xl mt-5 font-normal pb-8">
                            You’ve finished the “Discovery Assessment” and now
                            ready to start your “Assessment Journey”
                          </div>
                          <div className="border-t-2 border-gray-300 -mx-14" />
                          <div className="flex pt-8">
                            <button
                              className="bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold "
                              onClick={handleNewAssessmentClick}
                            >
                              Start Assessment Journey
                            </button>
                            <button
                              className="px-6 py-3 font-semibold mx-4"
                              onClick={() => {
                                navigate("/", { replace: true });
                              }}
                            >
                              Close
                            </button>
                          </div>
                        </div>
                      ) : (
                        <div className="">
                          <div className="text-4xl text-ngreen-700 font-black py-4">
                            Assessment submitted successfully!
                          </div>
                          <div className="text-xl mt-5 font-normal pb-8">
                            Our team will get back to you after analysing your
                            response. You can explore your dashboard to view the
                            results and refer back to your response.
                          </div>
                          <div className="border-t-2 border-gray-300 -mx-14" />
                          <div className="flex pt-8">
                            <button
                              className="bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold "
                              onClick={() => {
                                navigate("/assessment-history", {
                                  replace: true,
                                });
                              }}
                            >
                              Go to Assessment Journey
                            </button>
                            <button
                              className="px-6 py-3 font-semibold mx-4"
                              onClick={() => {
                                navigate("/", { replace: true });
                              }}
                            >
                              Close
                            </button>
                          </div>
                        </div>
                      )
                    ) : (
                      <SectionQuestions
                        assessmentType={assessmentType}
                        sectionTitle={
                          activeSubSection
                            ? activeSubSection.title
                            : activeMainSection.title
                        }
                        sectionId={
                          activeSubSection
                            ? activeSubSection.id
                            : activeMainSection.id
                        }
                        sectionDescription={
                          activeSubSection
                            ? activeSubSection.description
                            : activeMainSection.description
                        }
                        questions={
                          activeSubSection
                            ? activeSubSection.questions ?? []
                            : activeMainSection.questions ?? []
                        }
                        // questiondescrition={
                        //   activeSubSection
                        //     ? activeSubSection.questiondescrition ?? []
                        //     : activeMainSection.questiondescrition ?? []
                        // }
                        sectionsClicked={[]}
                        isLastSection={isActiveSectionFinal(
                          sectionItems,
                          activeMainSection,
                          activeSubSection
                        )}
                        sectionStateChange={onSectionStateChange}
                        back={back}
                        next={next}
                        exit={exit}
                        saveResponses={(isLastSection: boolean) => {
                          if (isLastSection) {
                            setSubmitAttempted(true);
                            const allDone = allSectionsCompleted(sectionItems);
                            //console.log(allDone);
                            allDone
                              ? setDisplayConfirmSubmissionDialog(true)
                              : saveAndDirectToPendingSection();
                          } else {
                            saveAndNext();
                          }
                        }}
                        isDisabled={
                          assessmentStatus === AssessmentStatus.DONE ||
                          assessmentStatus === AssessmentStatus.COMPLETED ||
                          auth.client?.roles === Role.ADMIN
                        }
                        isDivVisible={isDivVisible}
                        toggleVisibility={toggleVisibility}
                        sectionItems={sectionItems}
                        allSectionsCompleted={allSectionsCompleted}
                      />
                    )}
                  </div>
                </div>
              </div>
            </TabPanel>
            <TabPanel value={selectedTab} index={1}>
              <div className="md:px-0 w-[92%]  m-auto">
                <div className="flex mt-5">
                  {displayConfirmSubmissionDialog && (
                    <ConfirmSubmission
                      onCancel={() => {
                        setDisplayConfirmSubmissionDialog(false);
                      }}
                      onSubmit={submitSurvey}
                    />
                  )}

                  <div className="shadow-lg section-navigation px-10 rounded-lg border-t-8 border-ngreen-400">
                    <div className="section-title"></div>
                    <Section
                      submitAttempted={submitAttempted}
                      activeSectionId={activeMainSection.id}
                      activeSubSectionId={activeSubSection?.id}
                      sectionItems={sectionItems}
                      isSubsection={false}
                      onActiveSubSectionClicked={onActiveSubSectionClicked}
                      onActiveMainSectionClicked={onActiveMainSectionClicked}
                    />
                  </div>

                  <div className="w-full ml-10 bg-white shadow-lg rounded-lg border-t-8 border-ngreen-400 mb-12">
                    {assessmentType == "maturity" &&
                      sectionItems
                        ?.reduce((acc, val) => {
                          acc = [...acc, ...val.subSections];
                          return acc;
                        }, [])
                        .map((section: ISection) => {
                          return (
                            <div key={section.id}>
                              <SectionRecommend
                                clientId={clientId}
                                assessmentId={assessmentId}
                                section={section}
                                recommendations={recommendations}
                                currentSectionId={section.id}
                                isEditorOpen={isEditorOpen}
                                editClicked={editClicked}
                                scores={scores}
                                previousScores={previousScores}
                                openEditor={openEditor}
                                saveRecommendation={saveRecommendation}
                                handleChildData={handleChildData}
                                closeEditor={closeEditor}
                                updateRecommendation={updateRecommendation}
                                handleEdit={handleEdit}
                                handleDelete={handleDelete}
                              />
                            </div>
                          );
                        })}
                    <div
                      style={{
                        boxShadow: "0px 0px 12px 0px #0000001A",
                      }}
                      className="bg-white p-4 flex justify-end shadow-lg foot-menu"
                    >
                      <div className="w-30">
                        <button
                          className="px-5 py-2 rounded-md bg-ngray-100 text-black mr-10"
                          onClick={exit}
                        >
                          <FontAwesomeIcon icon={faEnvelope} />
                          <span className="ml-2">Save as Draft</span>
                        </button>
                        <button
                          className="bg-ngreen-600 text-white rounded-md px-5 py-2 mr-10"
                          style={{ backgroundColor: "#47D7AC" }}
                          onClick={generateReport}
                        >
                          Generate Report
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </TabPanel>
          </>
        </div>
      </>
    )
  );
};

const AssessmentPage = withLoader(Assessment);

export { AssessmentPage };
