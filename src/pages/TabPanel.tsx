import { Box } from "@mui/material";

const TabPanel = (props: any) => {
  const { children, value, index } = props;
  return (
    <>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`tabpanel-${index}`}
        aria-labelledby={`tab-${index}`}
      >
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    </>
  );
};

export { TabPanel };
