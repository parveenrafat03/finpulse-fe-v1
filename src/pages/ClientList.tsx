import { useEffect, useState } from "react";
import { withLoader } from "../components";
import { IWithLoaderProps } from "../components/withLoader";
import { ClientAPI } from "../apis";
import { IClientProfile } from "./Profile";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../hooks";
import "../pages-css/clientList.css";
import IconSort from "../assets/icons/icon-sort.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faEye,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import { Menu, MenuItem } from "@mui/material";

const ClientList = ({ showLoader }: IWithLoaderProps) => {
  const [initialClientsData, setInitialClientsData] = useState<
    IClientProfile[]
  >([]);
  const [clients, setClients] = useState<IClientProfile[]>([]);
  const [isFocused, setIsFocused] = useState(false);
  const [sortOption, setSortOption] = useState<string>("Latest");
  //const [layout, setLayout] = useState<string>("list");
  const layout = "list";
  const [anchorEl, setAnchorEl] = useState<
    HTMLImageElement | SVGSVGElement | null
  >(null);
  const open = Boolean(anchorEl);
  const auth = useAuth();
  const navigate = useNavigate();

  let filterTimeout: number;
  const filterClients = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value) {
      setClients(initialClientsData);
    } else {
      clearTimeout(filterTimeout);

      filterTimeout = setTimeout(() => {
        const filteredList = initialClientsData.filter(
          (client: IClientProfile) =>
            client.client_name
              .toLowerCase()
              .indexOf(event.target.value.toLowerCase()) !== -1
        );
        setClients(filteredList);
      }, 500);
    }
  };

  const navigateToClientAssessments = (clientId: string) => {
    auth.setClientId(clientId);
    navigate("/assessment-history");
  };

  const handleSortChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSortOption(event.target.value);
    console.log(event.target.value);
  };

  // const setListLayout = () => {
  //   setLayout("list");
  // };

  // const setGridLayout = () => {
  //   setLayout("grid");
  // };

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  const handleMenuOpenSVG = (event: React.MouseEvent<SVGSVGElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    void (async () => {
      showLoader?.({ show: true });
      const clientsList = await ClientAPI.getClientsList();
      setInitialClientsData(clientsList);
      setClients(clientsList);
      showLoader?.({ show: false });
    })();
  }, [showLoader]);

  return (
    <div className="page-container w-[89%]">
      <div className="page-title">All Assessment</div>
      <div className="page-menu">
        <div className={"menu-search " + (isFocused ? "focused" : "")}>
          <FontAwesomeIcon icon={faSearch} className="menu-search-icon" />
          <input
            className="menu-search-text"
            placeholder="Search..."
            onFocus={handleFocus}
            onBlur={handleBlur}
            onChange={filterClients}
          />
        </div>
        <div className="menu-sort">
          <img src={IconSort} />
          <span>Sort by :</span>
          <select value={sortOption} onChange={handleSortChange}>
            <option value="Latest">Latest</option>
            <option value="Oldest">Oldest</option>
          </select>
        </div>
      </div>
      <div className="page-content">
        {clients.length === 0 ? (
          <div className="no-data">No data found</div>
        ) : layout === "list" ? (
          <div className="layout-list">
            <div className="list-row header">
              <div className="list-col col-6">Name</div>
              <div className="list-col col-4">Last Updated on</div>
              <div className="list-col col-2"></div>
            </div>
            {clients.map((client: IClientProfile, index: number) => (
              <div
                key={index + 1}
                className={
                  "list-row " +
                  (index % 2 === 0 ? "list-row-odd" : "list-row-even")
                }
              >
                <div className="list-col col-6">
                  <span
                    className="data-name"
                    onClick={() =>
                      navigateToClientAssessments(client.client_id!)
                    }
                  >
                    {client.client_name}
                  </span>
                </div>
                <div className="list-col col-4">
                  {client.last_updated_on && ""}
                  <span className="data-date">27th Sep, 2023</span>
                  <span className="data-time">02:13 PM</span>
                </div>
                <div className="list-col col-2 actions">
                  <button
                    className="action-navigate"
                    onClick={() =>
                      navigateToClientAssessments(client.client_id!)
                    }
                  >
                    View Details
                  </button>
                  <div className="action-menu">
                    {/* <img
                      src={IconDotMenu}
                      className="action-menu-icon"
                      aria-controls={open ? "basic-menu" : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? "true" : undefined}
                      onClick={handleMenuOpen}
                      id="menu-icon"
                    /> */}
                    <FontAwesomeIcon
                      icon={faEllipsisVertical}
                      className="cursor-pointer text-xl opacity-50"
                      aria-controls={open ? "basic-menu" : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? "true" : undefined}
                      onClick={handleMenuOpenSVG}
                      id="menu-icon"
                    />
                    <Menu
                      id="basic-menu"
                      anchorEl={anchorEl}
                      open={open}
                      onClose={handleMenuClose}
                      MenuListProps={{ "aria-labelledby": "menu-icon" }}
                      anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                      transformOrigin={{ vertical: "top", horizontal: "right" }}
                      sx={{
                        "& .MuiPaper-root": {
                          border: "none",
                          boxShadow: "0px 0px 3px 0px rgba(0,0,0,0.1)",
                        },
                      }}
                    >
                      <MenuItem
                        onClick={() =>
                          navigateToClientAssessments(client.client_id!)
                        }
                      >
                        <FontAwesomeIcon
                          icon={faEye}
                          style={{ marginRight: "8px" }}
                        />
                        <span className="action-dropdown-item">
                          View Details
                        </span>
                      </MenuItem>
                    </Menu>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <div className="layout-grid">Layout Grid</div>
        )}
      </div>
    </div>
    // <div className="md:my-10 w-full px-5 md:px-0 w-[89%] m-auto py-8 bg-white">
    //   <h2 className="text-2xl mb-10 px-10 font-medium">All Assessments</h2>
    //   <div className="px-10 relative">
    //     <FontAwesomeIcon
    //       icon={faSearch}
    //       className="tex-xl text-slate-400 absolute left-14 top-[0.8rem]"
    //     />
    //     <input
    //       className="w-1/2 pl-12 border border-slate-300 rounded-3xl mb-10 py-2 pr-3"
    //       placeholder="Search..."
    //       onChange={filterClients}
    //     />
    //   </div>
    //   <table className="w-full">
    //     <thead className="text-left bg-ngray-100">
    //       <tr>
    //         <td className="px-10 py-3">Name</td>
    //         <td className="px-10 py-3">Last Updated On</td>
    //         <td className="px-10 py-3"></td>
    //       </tr>
    //     </thead>
    //     <tbody className="text-left py-3">
    //       {clients.length === 0 ? (
    //         <tr>
    //           <td className="py-5 pl-10" colSpan={3}>
    //             No data found.
    //           </td>
    //         </tr>
    //       ) : (
    //         clients.map((client: IClientProfile, index: number) => (
    //           <tr
    //             key={index + 1}
    //             className={index % 2 === 1 ? "bg-ngreen-200 bg-opacity-10" : ""}
    //           >
    //             <td className="px-10 py-3">
    //               <button
    //                 className="text-ngreen-600"
    //                 onClick={() =>
    //                   navigateToClientAssessments(client.client_id!)
    //                 }
    //               >
    //                 {client.client_name}
    //               </button>
    //             </td>
    //             <td className="px-10 py-3">{client.last_updated_on && ""}</td>
    //             <td className="px-10 py-5 text-right">
    //               <button
    //                 className="bg-ngreen-600 text-white rounded-md px-5 py-2"
    //                 onClick={() =>
    //                   navigateToClientAssessments(client.client_id!)
    //                 }
    //               >
    //                 View Details
    //               </button>
    //             </td>
    //           </tr>
    //         ))
    //       )}
    //     </tbody>
    //   </table>
    // </div>
  );
};

const ClientListPage = withLoader(ClientList);

export { ClientListPage };
