import { Link, useNavigate } from "react-router-dom";
import tile1 from "/src/assets/images/dashboard/tile1.png";
import tile2 from "/src/assets/images/dashboard/tile2.png";
import tile3 from "/src/assets/images/dashboard/tile3.png";
import tile4 from "/src/assets/images/dashboard/tile4.png";
import tile5 from "/src/assets/images/dashboard/tile5.png";
import tile6 from "/src/assets/images/dashboard/tile6.png";
import cloud from "/src/assets/images/dashboard/cloud.png";
import step1 from "/src/assets/images/dashboard/step1.svg";
import step1Icon from "/src/assets/images/dashboard/step1_icon.png";
import step2 from "/src/assets/images/dashboard/step2.svg";
import step2Icon from "/src/assets/images/dashboard/step2_icon.png";
import step3 from "/src/assets/images/dashboard/step3.svg";
import step3Icon from "/src/assets/images/dashboard/step3_icon.png";
import step4 from "/src/assets/images/dashboard/step4.svg";
import step4Icon from "/src/assets/images/dashboard/step4_icon.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { useAuth } from "../hooks";
import { useEffect, useState } from "react";
import { ClientAPI } from "../apis";
import { AssessmentStatus, IAssessmentDetails } from "../interfaces";
import { withLoader } from "../components";
import { IWithLoaderProps } from "../components/withLoader";
import { ISurveyResponse } from "../apis/clientAPI";
import "../pages-css/home.css";
import { Role } from "../typings";
import { IClientProfile } from "./Profile";

interface HomeProps {
  setShowLogin: (value: boolean) => void;
}

type ComplexProps = HomeProps & IWithLoaderProps;

const Home: React.FC<ComplexProps> = ({ setShowLogin, showLoader }) => {
  const [assessmentBtnText, setAssessmentBtnText] = useState("Get Started");
  const [assessmentBtnPath, setAssessmentBtnPath] = useState("/login");

  const auth = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    setShowLogin(true);
    void (async () => {
      if (auth.client) {
        if (auth.client.roles === Role.ADMIN) {
          navigate("/clients");
        }
        showLoader?.({ show: true, message: "Checking assessments status.." });
        setAssessmentBtnText("Go to Assessment Journey");
        setAssessmentBtnPath("/assessment-history");

        let clientProfile: IClientProfile = null;
        let discoveryAssessment: ISurveyResponse = null;

        try {
          [clientProfile, discoveryAssessment] = await Promise.all([
            ClientAPI.getClientProfile(auth.client!.client_id),
            ClientAPI.fetchDiscoverySurveyResponse(auth.client!.client_id),
          ]);
        } catch (error) {
          console.error("Error fetching discovery assessment:", error);
        }

        if (clientProfile == null || clientProfile.customer_name == null) {
          setAssessmentBtnText("Get Started");
          setAssessmentBtnPath("/profile");
        } else if (
          discoveryAssessment == null ||
          discoveryAssessment.assessment_status === null ||
          discoveryAssessment.assessment_status === AssessmentStatus.IN_PROGRESS
        ) {
          setAssessmentBtnText("Start Assessment");
          // setAssessmentBtnPath("/assessment-history");
          setAssessmentBtnPath("/assessment?type=discovery");
        }
        showLoader?.({ show: false });
      }
    })();
  }, [auth, showLoader]);

  return (
    <>
      <section className="relative w-full bg-white">
        <div className="flex items-center text-white h-50rem w-full bg-[url('/src/assets/images/dashboard/Homepage.svg')] bg-no-repeat bg-center bg-cover before:content-[''] before:absolute before:top-0 before:bottom-0 before:left-0 before:right-0 ">
          <div className="z-10 px-5 md:px-0 w-[89%]  m-auto mt-16">
            <div>
              <div className="page-label text-ngreen-700 font-black ">
                Propel your FinOps potential
              </div>

              <p className="sub-label font-thin ">
                Assess . Adapt . Accelerate
              </p>
            </div>
            <div className="font-bold text-md text-ngreen-700 pt-5 text-4xl">
              According to FinOps Foundation
            </div>
            <p className=" pt-2 text-md leading-10  text-black font-medium max-w-xl text-2xl ">
              ”FinOps is an evolving cloud financial management discipline and
              cultural practice that enables organizations to get maximum
              business value by helping engineering, finance, technology and
              business teams to collaborate on data-driven spending decisions”
            </p>
            <div className="flex items-center gap-5 pt-7">
              <Link to={assessmentBtnPath}>
                <button className="text-3xl bg-nblue-500  py-2 px-10 border border-nblue-500 rounded-lg h-24 start-button-style ">
                  {assessmentBtnText}
                </button>
              </Link>
            </div>
          </div>
        </div>
        <div className="absolute -left-72 bottom-20 opacity-20">
          {/* <svg
            xmlns="http://www.w3.org/2000/svg"
            width="450"
            height="242"
            viewBox="0 0 165 242"
            fill="none"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M76.3107 46.9005C74.2016 46.9005 72.457 47.6112 71.0954 49.0338C66.5449 53.7879 63.9861 67.8489 71.7544 91.8287C78.8832 113.834 92.6855 137.556 111.035 159.519C113.444 147.198 114.768 133.615 114.768 119.272C114.768 77.1064 94.4335 53.229 81.6156 48.0827C79.6514 47.295 77.8804 46.9005 76.3107 46.9005ZM184.618 81.3557C183.046 90.1911 182.197 99.6108 182.197 109.393C182.197 147.871 199.188 183.772 214.438 191.927C219.049 194.394 221.92 193.414 223.919 192.158C227.315 189.219 229.255 184.398 229.686 177.823C231.263 153.771 212.725 114.679 184.618 81.3557ZM122.643 172.527C165.426 217.386 211.418 232.992 235.621 224.98C248.038 220.87 259.447 208.85 267.748 191.136C276.805 171.805 281.593 147.455 281.593 120.718C281.593 92.4681 276.158 66.2309 266.288 46.8373C257.003 28.5925 244.788 18.5438 231.894 18.5438C213.424 18.5438 197.276 37.0554 188.709 64.4645C223.261 102.381 245.823 148.94 243.868 178.753C243.168 189.429 239.292 197.945 232.659 203.376L232.101 203.789C224.752 208.694 216.099 208.932 207.736 204.461C185.817 192.737 167.984 150.09 167.984 109.393C167.984 95.0648 169.667 81.151 172.868 68.348C126.23 19.8606 80.9417 2.81184 51.2476 22.7632C28.9387 37.7879 14.5189 76.2405 14.5189 120.718C14.5189 178.178 36.7231 223.189 65.0706 223.189C82.7806 223.189 98.0502 204.758 106.83 176.363C84.003 150.967 66.7645 122.544 58.2338 96.2079C49.8837 70.4295 50.829 49.6536 60.8294 39.207C67.3257 32.4186 76.8328 30.8477 86.9114 34.8945C107.816 43.2861 128.981 74.9651 128.981 119.272C128.981 138.181 126.785 156.343 122.643 172.527ZM221.707 241.251C191 241.251 151.867 221.389 117.885 187.94C116.285 192.305 114.519 196.463 112.595 200.378C100.872 224.252 83.9938 237.4 65.0706 237.4C46.1013 237.4 29.0894 224.544 17.1685 201.202C6.29521 179.911 0.307159 151.329 0.307159 120.718C0.307159 70.8907 16.7867 28.8375 43.3149 10.9699C61.7045 -1.38451 84.8264 -2.94736 110.184 6.44699C131.72 14.4268 154.878 30.4153 177.653 52.961C179.77 47.3536 182.218 42.0636 184.978 37.1681C196.915 15.9931 213.577 4.33099 231.894 4.33099C268.33 4.33099 295.804 54.3675 295.804 120.718C295.804 149.525 290.553 175.963 280.617 197.166C275.742 207.57 269.879 216.371 263.189 223.324C256.161 230.629 248.39 235.725 240.088 238.473C234.436 240.344 228.26 241.251 221.707 241.251Z"
              fill="#47D7AC"
            />
          </svg> */}
        </div>
        <div className="absolute right-0 -bottom-32 opacity-20">
          {/* <svg
            xmlns="http://www.w3.org/2000/svg"
            width="165"
            height="242"
            viewBox="0 0 165 242"
            fill="none"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M76.3107 46.9005C74.2016 46.9005 72.457 47.6112 71.0954 49.0338C66.5449 53.7879 63.9861 67.8489 71.7544 91.8287C78.8832 113.834 92.6855 137.556 111.035 159.519C113.444 147.198 114.768 133.615 114.768 119.272C114.768 77.1064 94.4335 53.229 81.6156 48.0827C79.6514 47.295 77.8804 46.9005 76.3107 46.9005ZM184.618 81.3557C183.046 90.1911 182.197 99.6108 182.197 109.393C182.197 147.871 199.188 183.772 214.438 191.927C219.049 194.394 221.92 193.414 223.919 192.158C227.315 189.219 229.255 184.398 229.686 177.823C231.263 153.771 212.725 114.679 184.618 81.3557ZM122.643 172.527C165.426 217.386 211.418 232.992 235.621 224.98C248.038 220.87 259.447 208.85 267.748 191.136C276.805 171.805 281.593 147.455 281.593 120.718C281.593 92.4681 276.158 66.2309 266.288 46.8373C257.003 28.5925 244.788 18.5438 231.894 18.5438C213.424 18.5438 197.276 37.0554 188.709 64.4645C223.261 102.381 245.823 148.94 243.868 178.753C243.168 189.429 239.292 197.945 232.659 203.376L232.101 203.789C224.752 208.694 216.099 208.932 207.736 204.461C185.817 192.737 167.984 150.09 167.984 109.393C167.984 95.0648 169.667 81.151 172.868 68.348C126.23 19.8606 80.9417 2.81184 51.2476 22.7632C28.9387 37.7879 14.5189 76.2405 14.5189 120.718C14.5189 178.178 36.7231 223.189 65.0706 223.189C82.7806 223.189 98.0502 204.758 106.83 176.363C84.003 150.967 66.7645 122.544 58.2338 96.2079C49.8837 70.4295 50.829 49.6536 60.8294 39.207C67.3257 32.4186 76.8328 30.8477 86.9114 34.8945C107.816 43.2861 128.981 74.9651 128.981 119.272C128.981 138.181 126.785 156.343 122.643 172.527ZM221.707 241.251C191 241.251 151.867 221.389 117.885 187.94C116.285 192.305 114.519 196.463 112.595 200.378C100.872 224.252 83.9938 237.4 65.0706 237.4C46.1013 237.4 29.0894 224.544 17.1685 201.202C6.29521 179.911 0.307159 151.329 0.307159 120.718C0.307159 70.8907 16.7867 28.8375 43.3149 10.9699C61.7045 -1.38451 84.8264 -2.94736 110.184 6.44699C131.72 14.4268 154.878 30.4153 177.653 52.961C179.77 47.3536 182.218 42.0636 184.978 37.1681C196.915 15.9931 213.577 4.33099 231.894 4.33099C268.33 4.33099 295.804 54.3675 295.804 120.718C295.804 149.525 290.553 175.963 280.617 197.166C275.742 207.57 269.879 216.371 263.189 223.324C256.161 230.629 248.39 235.725 240.088 238.473C234.436 240.344 228.26 241.251 221.707 241.251Z"
              fill="#47D7AC"
            />
          </svg> */}
        </div>
      </section>
      <section className="w-full bg-white">
        <div className="px-5 md:px-0 w-[89%] pt-16 m-auto">
          <h1 className="text-4xl font-semibold">
            FinOps - Domains & Capabilities
          </h1>
          <div className="bg-npurple-100 p-5 mt-7 bg-opacity-50 italic font-medium text-sm tracking-wide rounded ">
            FinOps domains represents an interdependent sphere of activities,
            which provide a high-level overview of functional activities that
            are relevant for a EinOns practice. Each domain has a list of
            capabilities.
          </div>
          <div className="bg-ngreen-100 p-5 mt-5 bg-opacity-50 italic font-medium text-sm tracking-wide rounded ">
            FinOps capabilities represent functional area of activity in support
            of their corresponding domain. <br />
            FinOps Capabilities enable, educate about cloud Cost management and
            define actionable tasks and improve the FinOps maturity
          </div>
          <div className="grid grid-col-1 md:grid-cols-2 xl:grid-cols-3 gap-5 mt-6">
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile1} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Understanding cloud usage and cost
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">Cost Analysis</li>
                <li className="ml-8">Data Analysis and Showback</li>
                <li className="ml-8">Managing Shared Cost</li>
                <li className="ml-8">
                  Data Ingestion and <br />
                  Normalization
                </li>
              </ul>
            </div>
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile2} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Performance tracking & benchmarking
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">Measuring Unit cost</li>
                <li className="ml-8">Forecasting</li>
                <li className="ml-8">Budget Management</li>
              </ul>
            </div>
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile3} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Real time decision making
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">Managing anomalies</li>
              </ul>
            </div>
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile4} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Cloud usage optimization
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">Onboarding Workloads</li>
                <li className="ml-8">
                  Resource Utilization and <br />
                  Efficiency
                </li>
              </ul>
            </div>
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile5} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Cloud rate Optimization
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">
                  Managing commitment-
                  <br />
                  based discounts
                </li>
              </ul>
            </div>
            <div className="p-7 pb-10 shadow-[0_0_12px_0_rgba(0,0,0,0.15)] rounded-lg">
              <img src={tile6} className="pb-7" />
              <h1 className="text-2xl font-medium pb-5 font-semibold">
                Organizational alignment
              </h1>
              <ul className="list-disc leading-8">
                <li className="ml-8">Establishing FinOps culture</li>
                <li className="ml-8">
                  Chargeback and Finance
                  <br /> integration
                </li>
                <li className="ml-8">
                  IT Asset Management
                  <br /> Integration
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="w-full bg-white">
        <div className="flex space-between mt-16 items-center text-white h-96 w-full bg-[url('/src/assets/images/dashboard/Homepage2.svg')] bg-no-repeat bg-center bg-cover before:content-[''] before:absolute before:top-0 before:bottom-0 before:left-0 before:right-0 ">
          <div className="z-10 px-5 md:px-0 w-[89%]  m-auto flex justify-between">
            <div className="page-label-2 font-black">
              <p className="text-ngreen-700 page-label-2 inline">Experience</p>{" "}
              data-driven
              <br /> spending decisions
            </div>
            <div className="flex items-end">
              <Link to={assessmentBtnPath}>
                <button className="text-3xl bg-nblue-500  py-2 px-6 border border-nblue-500 rounded-lg h-24 start-button-style ">
                  {assessmentBtnText}
                </button>
              </Link>
            </div>
          </div>
        </div>
      </section>
      <section className="w-full bg-white">
        <div className="relative px-5 md:px-0 w-[89%] pt-16 m-auto flex gap-10 items-center">
          <div className="w-full md:w-7/12 md:bg-none md:before:hidden bg-[url('/src/assets/images/dashboard/cloud.png')] bg-no-repeat bg-center before:content-[''] before:absolute before:top-0 before:bottom-0 before:left-0 before:right-0 before:bg-white before:bg-opacity-80">
            <h1 className="text-4xl font-bold relative">
              Key Highlights of FinOps Maturity
            </h1>
            <div className="relative flex items-center gap-3 pt-7">
              <FontAwesomeIcon
                icon={faCheckCircle}
                className="text-2xl text-ngreen-400"
              />
              <p className="text-lg">
                Designed in conjunction with the FinOps Framework from FinOps
                Foundation, so that you do not miss any best practices.
              </p>
            </div>
            <div className="relative flex items-center gap-3 pt-7">
              <FontAwesomeIcon
                icon={faCheckCircle}
                className="text-2xl text-ngreen-400"
              />
              <p className="text-lg">
                Provides a measurable and meaningful analysis of your current
                operating Fin0ps maturity.
              </p>
            </div>
            <div className="relative flex items-center gap-3 pt-7">
              <FontAwesomeIcon
                icon={faCheckCircle}
                className="text-2xl text-ngreen-400"
              />
              <p className="text-lg">
                Helps you understand your current maturity stage so that you
                know what key areas to focus upon for the next stage.
              </p>
            </div>
            <div className="relative flex items-center gap-3 pt-7">
              <FontAwesomeIcon
                icon={faCheckCircle}
                className="text-2xl text-ngreen-400"
              />
              <p className="text-lg">
                Provides a list of actionable recommendations for each FinOps
                domain to help you progress to the next maturity stage.
              </p>
            </div>
            <div className="relative flex items-center gap-3 pt-7">
              <FontAwesomeIcon
                icon={faCheckCircle}
                className="text-2xl text-ngreen-400"
              />
              <p className="text-lg">
                Provides a quantifiable mechanism to track your efforts on
                improving FinOps maturity in the organization
              </p>
            </div>
          </div>
          <div className="hidden md:block w-5/12">
            <img src={cloud} className="m-auto" />
          </div>
        </div>
      </section>
      <section className="relative w-full bg-white">
        <div className="px-5 md:px-0 w-[89%] py-16 m-auto bg-[url('/src/assets/images/dashboard/scribble.svg')] bg-no-repeat bg-center bg-cover before:content-[''] before:absolute before:top-0 before:bottom-0 before:left-0 before:right-0 before:bg-white before:bg-opacity-90">
          <h1 className="relative text-4xl font-semibold">
            Stages of FinOps Maturity
          </h1>
          <div className="relative grid grid-cols-2 md:grid-cols-4 pt-7">
            <div className="">
              <div className="ml-14 lg:ml-20 mb-5 w-20 h-20 flex items-center justify-center">
                <img src={step1Icon} />
              </div>
              <div className="relative mb-5">
                <img src={step1} />
                <div className="absolute top-[40%] left-[26%]">Pre-Crawl</div>
              </div>
            </div>
            <div className="">
              <div className="ml-14 lg:ml-20 mb-5 w-20 h-20">
                <img src={step2Icon} />
              </div>
              <div className="relative mb-5">
                <img src={step2} />
                <div className="absolute top-[40%] left-[30%]">Crawl</div>
              </div>
            </div>
            <div className="">
              <div className="ml-14 lg:ml-20 mb-5 w-20 h-20">
                <img src={step3Icon} />
              </div>
              <div className="relative mb-5">
                <img src={step3} />
                <div className="absolute top-[40%] left-[31%]">Walk</div>
              </div>
            </div>
            <div className="">
              <div className="ml-14 lg:ml-20 mb-5 w-20 h-20">
                <img src={step4Icon} />
              </div>
              <div className="relative mb-5">
                <img src={step4} />
                <div className="absolute top-[40%] left-[32%]">Run</div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

const HomePage = withLoader(Home);

export { HomePage };
