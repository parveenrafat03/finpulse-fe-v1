import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useNavigate } from "react-router-dom";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { useState, useEffect, useMemo } from "react";
import { SectionCore } from "../components/SectionCore";
import { SectionItem } from "../components/SectionItem";
const ThankYou = () => {
  const navigate = useNavigate();
  return (
    <div className="md:px-0 w-[89%]  m-auto pt-5">
      <div className="inline pr-5">
        <div className="font-bold inline text-xl">
          {localStorage.getItem("company-name")}{" "}
        </div>
        <div className=" inline text-base">- Assessment Questions</div>
      </div>
      <div className="inline assessment-status-button text-base font-normal py-2">
        <FontAwesomeIcon
          icon={checkCircle}
          className="text-ngreen-600 mr-1 w-3"
        />
        Submitted
      </div>
      <div className="flex mt-5">
        <div className="shadow-lg section-navigation px-10 rounded-lg border-t-8 border-ngreen-400 h-fit pb-6 pt-10">
          <div>
            <div className="">
              <ol className="relative border-l border-ngray-300">
                <div className="elative border-l border-ngray-300 "></div>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 -mt-2">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer   text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 ">
                        Understanding cloud usage and cost
                      </div>
                      <div className="w-1"></div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 -mt-2">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer  text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 ">
                        Performance tracking &amp; benchmarking
                      </div>
                      <div className="w-1"></div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 -mt-2">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer  text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 ">Real time decision making</div>
                      <div className="w-1"></div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 -mt-2">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer  text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 ">Cloud usage optimization</div>
                      <div className="w-1">
                        {/* <svg
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="chevron-down"
                          className="svg-inline--fa fa-chevron-down text-black text-right"
                          role="img"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                        >
                          <path
                            fill="currentColor"
                            d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"
                          ></path>
                        </svg> */}
                      </div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 -mt-2">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer  text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 ">Cloud rate optimization</div>
                      <div className="w-1"></div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
                <li className="mb-8 ml-6">
                  <span className="absolute flex items-center justify-center w-6 h-7 rounded-full  -ml-9 ">
                    <FontAwesomeIcon
                      icon={checkCircle}
                      className="text-ngreen-600"
                    />
                  </span>
                  <div className="mb-1 text-lg font-normal cursor-pointer  text-ngreen-600 font-semibold  ">
                    <span className=""></span>
                    <div className="flex section-option">
                      <div className="w-72 mt-1">Organizational alignment</div>
                      <div className="w-1"></div>
                    </div>
                    <div className="clear-both"></div>
                  </div>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div className="w-full pb-72 ml-10 shadow-lg rounded-lg border-t-8 border-ngreen-400 section-content-discovery  pb-24 overflow-hidden">
          <div className="">
            <div className="text-4xl text-ngreen-700 font-black py-4">
              Assessment submitted successfully!
            </div>
            <div className="text-xl mt-5 font-normal pb-8">
              Our team will get back to you after analysing your response. You
              can explore your dashboard to view the results and refer back to
              your response.
            </div>
            <div className="border-t-2 border-gray-300 -mx-14" />
            <div className="flex pt-8">
              <button
                className="bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold "
                onClick={() => {
                  navigate("/assessment-history", { replace: true });
                }}
              >
                Go to Assessment Journey
              </button>
              <button
                className="px-6 py-3 font-semibold mx-4"
                onClick={() => {
                  navigate("/", { replace: true });
                }}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { ThankYou };
