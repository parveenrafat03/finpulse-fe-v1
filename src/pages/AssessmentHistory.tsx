import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import * as yup from "yup";
import { FormContext, useAuth, useForm } from "../hooks";
import { IAuthContextType, ILoginForm } from "../hooks/useAuth";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import iconCircleExclamation from "../assets/icons/icon-circle-exclamation.svg";
import iconDotMenu from "../assets/icons/icon-dot-menu.svg";
import IconSort from "../assets/icons/icon-sort.svg";
import { IWithLoaderProps, withLoader } from "../components/withLoader";
import "../pages-css/assessmentHistory.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowLeft,
  faCircle,
  faCircleCheck,
  faCircleExclamation,
  faEllipsisVertical,
  faPenToSquare,
  faSpinner,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import {
  AssessmentStatus,
  AssessmentType,
  IAssessmentDetails,
} from "../interfaces";
import { IClientProfile } from "./Profile";
import { ClientAPI } from "../apis";
import { ISurveyResponse } from "../apis/clientAPI";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { faCircle as blankCircle } from "@fortawesome/free-regular-svg-icons/faCircle";
import { faCircle as solidCircle } from "@fortawesome/free-solid-svg-icons/faCircle";
import { Role } from "../typings/enums";
import { Menu, MenuItem } from "@mui/material";
import {
  faEdit,
  faEye,
  faFileAlt,
  faTrashCan,
} from "@fortawesome/free-regular-svg-icons";
import { IAssessmentLinkProps } from "../components/AssessmentList";
import { formatDate, getFormattedDate } from "../utils/assessment_utils";

interface AssessmentHistoryProps {}

type ComplexProps = AssessmentHistoryProps & IWithLoaderProps;

const AssessmentHistory: React.FC<ComplexProps> = ({ showLoader }) => {
  const [sortOrder, setSortOrder] = useState<string>("ASC");
  const [clientInfo, setClientInfo] = useState<IClientProfile>(null);
  const [discoveryAssessments, setDiscoveryAssessments] =
    useState<IAssessmentDetails[]>();
  const [maturityAssessments, setMaturityAssessments] =
    useState<IAssessmentDetails[]>();
  const auth = useAuth();
  const navigate = useNavigate();
  const clientId = auth.userId;

  useEffect(() => {
    const sessionCheck = () => {
      if (!auth.isSessionValid()) {
        auth.logout();
      }
    };
    sessionCheck();
    auth.markLastActivity();
    const intervalId = setInterval(sessionCheck, 15 * 1000);
    return () => clearInterval(intervalId);
  }, []);

  const moveToViewDiscovery = () => {
    navigate("/assessment?type=discovery");
  };

  const sortByStartDate = () => {
    auth.markLastActivity();
    var data = maturityAssessments;
    if (sortOrder === "ASC") {
      data.sort((a, b) => a.created_date.localeCompare(b.created_date));
    } else {
      data.sort((a, b) => b.created_date.localeCompare(a.created_date));
    }
    setMaturityAssessments(data);
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  const sortByLastUpdated = () => {
    auth.markLastActivity();
    var data = maturityAssessments;
    if (sortOrder === "ASC") {
      data.sort((a, b) =>
        a.last_modified_date.localeCompare(b.last_modified_date)
      );
    } else {
      data.sort((a, b) =>
        b.last_modified_date.localeCompare(a.last_modified_date)
      );
    }
    setMaturityAssessments(data);
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  const handleNewAssessmentClick = () => {
    void (async () => {
      showLoader?.({ show: true });
      auth.markLastActivity();
      const newAssessmentId = await ClientAPI.initiateMaturitySurvey(clientId);
      const newAssessmentName = `Assessment ${
        (maturityAssessments?.length ?? 0) + 1
      }`;
      const newAssessment: IAssessmentDetails = {
        client_id: clientId,
        assessment_id: newAssessmentId!,
        assessment_name: newAssessmentName,
        assessment_status: AssessmentStatus.IN_PROGRESS,
        created_date: new Date().toISOString(),
        last_modified_date: new Date().toISOString(),
      };

      if (maturityAssessments) {
        maturityAssessments.unshift(newAssessment);
        setMaturityAssessments(maturityAssessments);
        navigate(`/assessment?type=maturity&id=${newAssessmentId}`);
      } else {
        setMaturityAssessments([newAssessment]);
        navigate(`/assessment?type=maturity&id=${newAssessmentId}`);
      }
      showLoader?.({ show: false });
    })();
  };

  const assessmentButtonDisabled = (): boolean => {
    const discoverDone =
      discoveryAssessments?.[0]?.assessment_status === AssessmentStatus.DONE;
    const maturityPending = maturityAssessments?.some(
      (m) => m.assessment_status === AssessmentStatus.IN_PROGRESS
    );
    return maturityPending ?? !discoverDone;
  };

  useEffect(() => {
    void (async () => {
      if (clientId === "") {
        navigate("/clients");
      }
      try {
        showLoader?.({ show: true, message: "Checking assessments status.." });
        //auth.setClientId(clientId);
        const [clientInfo, discoverySurvey, maturitySurveys]: [
          IClientProfile,
          ISurveyResponse,
          IAssessmentDetails[],
        ] = await Promise.all([
          ClientAPI.getClientProfile(clientId),
          ClientAPI.fetchDiscoverySurveyResponse(clientId),
          ClientAPI.fetchAssessmentsHistory(clientId),
        ]);
        setClientInfo(clientInfo);
        const assessmentDetail: IAssessmentDetails = {
          client_id: clientId,
          assessment_id: "",
          assessment_name: "",
          assessment_status: discoverySurvey.assessment_status!,
          created_date: new Date().toISOString(),
          last_modified_date: new Date().toISOString(),
        };
        setDiscoveryAssessments([assessmentDetail]);
        maturitySurveys.sort((a, b) =>
          b.assessment_id.localeCompare(a.assessment_id)
        );
        maturitySurveys.forEach((value, index) => {
          maturitySurveys[index].previousAssessmentId =
            index < maturitySurveys.length - 1
              ? maturitySurveys[index + 1].assessment_id
              : "0";
        });
        setMaturityAssessments(maturitySurveys);
        showLoader?.({ show: false });
      } catch (error) {
        console.log(error);
        showLoader?.({ show: false });
      }
    })();
  }, [clientId, showLoader]);

  const [anchorEl, setAnchorEl] = useState<HTMLImageElement | null>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLImageElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const LinkToAssessment = ({
    assessmentType,
    assessmentId,
    previousAssessmentId,
    children,
  }: IAssessmentLinkProps) => (
    <Link
      to={
        assessmentType === AssessmentType.DISCOVERY
          ? `/assessment?type=${assessmentType.toLowerCase()}`
          : `/assessment?type=${assessmentType.toLowerCase()}&id=${assessmentId}&pid=${previousAssessmentId}`
      }
    >
      {children}
    </Link>
  );

  return (
    <div className="ah-page-container w-[89%]">
      <div className="ah-header">
        <div className="ah-header-left">
          <div className="ah-page-title">Assessment Journey</div>
        </div>
        <div className="ah-header-right">
          <button
            className="ah-view-discovery-report"
            onClick={moveToViewDiscovery}
          >
            View Discovery Report
          </button>
          {auth.client!.roles !== Role.ADMIN && (
            <button
              onClick={handleNewAssessmentClick}
              className="ah-start-assessment btn-menu"
              disabled={assessmentButtonDisabled()}
            >
              Start your Assessment
            </button>
          )}
        </div>
      </div>
      <div className="ah-info">
        <div className="ah-info-left">
          <span>
            <b>{clientInfo?.client_name}</b>
          </span>
          <span className="ah-separator"></span>
          <span>{clientInfo?.email_address}</span>
          <span className="ah-separator"></span>
          <span>{clientInfo?.customer_name}</span>
          <span className="ah-separator"></span>
          <span>{clientInfo?.locations}</span>
        </div>
        <div className="ah-info-right">
          <div className="ml-auto text-ngreen-500 text-sm ">
            <Link to="/profile">
              {" "}
              <FontAwesomeIcon
                icon={faPenToSquare}
                className="text-ngreen-500 bg-white mt-1 mx-2 ml-auto"
              />
              Edit Company Details{" "}
            </Link>
          </div>
        </div>
      </div>
      <div className="ah-title">Maturity Assessments</div>
      <div className="ah-content">
        {maturityAssessments?.length === 0 ? (
          <div className="ah-no-data">No data found</div>
        ) : (
          <div className="ah-layout-list">
            <div className="ah-list-row ah-theader">
              <div className="col-3">Name</div>
              <div className="col-2">
                Start Date <img src={IconSort} onClick={sortByStartDate} />
              </div>
              <div className="col-2">
                Last Updated <img src={IconSort} onClick={sortByLastUpdated} />
              </div>
              <div className="col-2">Status</div>
              <div className="col-2">Action</div>
            </div>
            {maturityAssessments?.map(
              (assessment: IAssessmentDetails, index: number) => (
                <div
                  key={index + 1}
                  id={assessment.assessment_id}
                  className={
                    "ah-list-row x-" +
                    (index % 2 === 0 ? "list-row-odd" : "list-row-even")
                  }
                >
                  <div className="ah-list-col col-3">
                    {assessment.assessment_name}
                  </div>
                  <div className="ah-list-col col-2">
                    <span> {getFormattedDate(assessment.created_date)}</span>
                  </div>
                  <div className="ah-list-col col-2">
                    <span>
                      {" "}
                      {assessment.last_modified_date
                        ? getFormattedDate(assessment.last_modified_date)
                        : ""}
                    </span>
                  </div>
                  <div className="ah-list-col col-2">
                    {assessment.assessment_status ===
                    AssessmentStatus.IN_PROGRESS ? (
                      <div className="inprogress">
                        <FontAwesomeIcon
                          icon={faCircle}
                          className="text-nyellow-600"
                        />{" "}
                        In Progress
                      </div>
                    ) : assessment.assessment_status ===
                      AssessmentStatus.DONE ? (
                      <div className="submitted">
                        <FontAwesomeIcon
                          icon={faCircleCheck}
                          className="text-ngreen-600"
                        />{" "}
                        Submitted
                      </div>
                    ) : (
                      <div className="completed">
                        <FontAwesomeIcon icon={faStar} />
                        Completed
                      </div>
                    )}
                  </div>
                  <div className="ah-list-col col-2 ah-actions">
                    {assessment.assessment_status ===
                    AssessmentStatus.COMPLETED ? (
                      <button
                        className="ah-action-navigate"
                        onClick={async () => {
                          showLoader?.({
                            show: true,
                            message: "Generating Report..",
                          });
                          await ClientAPI.generateReport(
                            clientId,
                            assessment.assessment_id
                          );
                          showLoader?.({ show: false });
                        }}
                      >
                        View Report
                      </button>
                    ) : (
                      <button
                        className="ah-action-navigate"
                        onClick={() => {
                          navigate(
                            "/assessment?type=maturity&id=" +
                              assessment.assessment_id +
                              "&pid=0"
                          );
                        }}
                      >
                        {assessment.assessment_status ===
                        AssessmentStatus.IN_PROGRESS
                          ? "Resume Assessment"
                          : "View Response"}
                      </button>
                    )}
                    <div>
                      <div className="ah-dropdown">
                        <img
                          src={iconDotMenu}
                          className="cursor-pointer text-xl opacity-50 ah-dropbtn"
                        />
                        <div className="ah-dropdown-content">
                          {assessment.assessment_status ===
                          AssessmentStatus.IN_PROGRESS ? (
                            <>
                              <a
                                onClick={() => {
                                  navigate(
                                    "/assessment?type=maturity&id=" +
                                      assessment.assessment_id +
                                      "&pid=0"
                                  );
                                }}
                              >
                                <FontAwesomeIcon
                                  icon={faEdit}
                                  className="text-ngreen-600"
                                />
                                <span>Resume Assessment</span>
                              </a>
                              <a
                                onClick={async () => {
                                  showLoader?.({
                                    show: true,
                                    message: "Deleting Assessment...",
                                  });
                                  await ClientAPI.deleteSurveyResponse(
                                    assessment.assessment_id
                                  );
                                  document
                                    .getElementById(assessment.assessment_id)
                                    .remove();
                                  showLoader?.({ show: false });
                                }}
                              >
                                <FontAwesomeIcon
                                  icon={faTrashCan}
                                  className="text-red-600"
                                />
                                <span>Delete Assessment</span>
                              </a>
                            </>
                          ) : (
                            <>
                              <a
                                onClick={() => {
                                  navigate(
                                    "/assessment?type=maturity&id=" +
                                      assessment.assessment_id +
                                      "&pid=0"
                                  );
                                }}
                              >
                                <FontAwesomeIcon
                                  icon={faEye}
                                  className="text-ngreen-600"
                                />
                                <span>View Response</span>
                              </a>
                              {assessment.assessment_status ===
                              AssessmentStatus.COMPLETED ? (
                                <a
                                  onClick={async () => {
                                    showLoader?.({
                                      show: true,
                                      message: "Generating Report...",
                                    });
                                    await ClientAPI.generateReport(
                                      clientId,
                                      assessment.assessment_id
                                    );
                                    showLoader?.({ show: false });
                                  }}
                                >
                                  <FontAwesomeIcon
                                    icon={faFileAlt}
                                    className="text-ngreen-600"
                                  />
                                  <span>View Report</span>
                                </a>
                              ) : (
                                <></>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
        )}
      </div>
    </div>
  );
};

const AssessmentHistoryPage = withLoader(AssessmentHistory);

export { AssessmentHistoryPage };
