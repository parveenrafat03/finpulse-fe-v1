import { useNavigate } from "react-router-dom";
import { useAuth } from "../hooks";
import { Role } from "../typings";
import { useEffect } from "react";
import { ClientAPI } from "../apis";

const LandingPage = () => {
  const navigate = useNavigate();
  const auth = useAuth();

  useEffect(() => {
    if (auth.client?.roles === Role.ADMIN) {
      navigate("/clients");
    } else {
      if (!sessionStorage.getItem("profilePageForced")) {
        void (async () => {
          const response = await ClientAPI.getClientProfile(
            auth.client!.client_id,
          );
          sessionStorage.setItem("profilePageForced", "true");
          navigate(response ? "/home" : "/profile");
        })();
      } else {
        navigate("/home");
      }
    }
  }, [auth.client, navigate]);

  return null;
};

export { LandingPage };
