import { useEffect, useState } from "react";
import { withLoader } from "../components";
import { IWithLoaderProps } from "../components/withLoader";
import { ClientAPI } from "../apis";
import { IClientProfile } from "./Profile";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../hooks";
import "../pages-css/allClients.css";
import IconSort from "../assets/icons/icon-sort.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faEye,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import { Menu, MenuItem } from "@mui/material";
import { formatDate, getFormattedDate } from "../utils/assessment_utils";

interface AllClientsProps {
  setClientId: (clientId: string) => void;
}

type ComplexProps = AllClientsProps & IWithLoaderProps;

const AllClients: React.FC<ComplexProps> = ({ setClientId, showLoader }) => {
  const [initialClientsData, setInitialClientsData] = useState<
    IClientProfile[]
  >([]);
  const [clients, setClients] = useState<IClientProfile[]>([]);
  const [isFocused, setIsFocused] = useState(false);
  const [sortOption, setSortOption] = useState<string>("Last Updated");
  const [sortOrder, setSortOrder] = useState<string>("ASC");
  const auth = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    const sessionCheck = () => {
      if (!auth.isSessionValid()) {
        auth.logout();
      }
    };
    sessionCheck();
    auth.markLastActivity();
    const intervalId = setInterval(sessionCheck, 15 * 1000);
    return () => clearInterval(intervalId);
  }, []);

  let filterTimeout: number;
  const filterClients = (event: React.ChangeEvent<HTMLInputElement>) => {
    auth.markLastActivity();
    if (!event.target.value) {
      setClients(initialClientsData);
    } else {
      clearTimeout(filterTimeout);

      filterTimeout = setTimeout(() => {
        const filteredList = initialClientsData.filter(
          (client: IClientProfile) =>
            client.client_name
              .toLowerCase()
              .indexOf(event.target.value.toLowerCase()) !== -1 ||
            client.customer_name
              .toLowerCase()
              .indexOf(event.target.value.toLowerCase()) !== -1 ||
            client.locations
              .toLowerCase()
              .indexOf(event.target.value.toLowerCase()) !== -1
        );
        setClients(filteredList);
      }, 500);
    }
  };

  const navigateToClientAssessments = (clientId: string) => {
    setClientId(clientId);
    auth.setClientId(clientId);
    navigate("/client-assessment-history");
  };

  const handleSort = () => {
    auth.markLastActivity();
    if (sortOption === "Country") {
      sortByCountry();
    } else if (sortOption === "Last Updated") {
      sortByLastUpdatedDate();
    }
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  const sortByCountry = () => {
    var data = clients;
    if (sortOrder === "ASC") {
      data.sort((a, b) => a.locations.localeCompare(b.locations));
    } else {
      data.sort((a, b) => b.locations.localeCompare(a.locations));
    }
    setClients(data);
  };

  // Needs updation
  const sortByLastUpdatedDate = () => {
    var data = clients;
    if (sortOrder === "ASC") {
      data.sort((a, b) => a.last_updated_on.localeCompare(b.last_updated_on));
    } else {
      data.sort((a, b) => b.last_updated_on.localeCompare(a.last_updated_on));
    }
    setClients(data);
  };

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  useEffect(() => {
    void (async () => {
      try {
        showLoader?.({ show: true });
        const clientsList = await ClientAPI.getClientsList();
        clientsList.sort((a, b) =>
          ("" + b.last_updated_on).localeCompare("" + a.last_updated_on)
        );
        setInitialClientsData(clientsList);
        setClients(clientsList);
      } catch (error) {
        console.log(error);
      }
      showLoader?.({ show: false });
    })();
  }, [showLoader]);

  const [isDropdownOpen, setDropdownOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState("Last Updated");

  const toggleDropdown = () => {
    setDropdownOpen(!isDropdownOpen);
  };

  const handleOptionClick = (value: string) => {
    auth.markLastActivity();
    setSortOption(value);
    setSelectedValue(value);
    setDropdownOpen(false);
    if (value === "Country") {
      sortByCountry();
    } else if (value === "Last Updated") {
      sortByLastUpdatedDate();
    }
    setSortOrder(sortOrder === "ASC" ? "DESC" : "ASC");
  };

  function getDateAfterNDays(n: number) {
    const currentDate = new Date();
    const futureDate = new Date(currentDate);
    futureDate.setDate(currentDate.getDate() + n);

    return futureDate;
  }

  return (
    <div className="ac-page-container w-[89%]">
      <div className="ac-page-title">All Clients</div>
      <div className="ac-page-menu">
        <div className={"ac-menu-search " + (isFocused ? "ac-focused" : "")}>
          <FontAwesomeIcon icon={faSearch} className="ac-menu-search-icon" />
          <input
            className="ac-menu-search-text"
            placeholder="Search..."
            onFocus={handleFocus}
            onBlur={handleBlur}
            onChange={filterClients}
          />
        </div>
        <div className="ac-menu-sort">
          {/* <img src={IconSort} /> */}
          <span>Sort by :</span>
          <div className={`custom-dropdown ${isDropdownOpen ? "open" : ""}`}>
            <div className="selected-value" onClick={toggleDropdown}>
              {selectedValue}
              <span>🞃</span>
            </div>
            <div className="dropdown-content">
              <div onClick={() => handleOptionClick("Last Updated")}>
                Last Updated
              </div>
              <div onClick={() => handleOptionClick("Country")}>Country</div>
            </div>
          </div>
        </div>
      </div>
      <div className="ac-page-content">
        {clients.length === 0 ? (
          <div className="ac-no-data">No data found</div>
        ) : (
          <div className="ac-layout-list">
            <div className="ac-list-row ac-theader">
              <div className="col-3">Company</div>
              <div className="col-3">Stakeholder</div>
              <div className="col-2">
                Last Updated
                {sortOption === "Last Updated" ? (
                  <img src={IconSort} onClick={handleSort} />
                ) : (
                  <></>
                )}
              </div>
              <div className="col-2">
                Country
                {sortOption === "Country" ? (
                  <img src={IconSort} onClick={handleSort} />
                ) : (
                  <></>
                )}
              </div>
              <div className="col-2"></div>
            </div>
            {clients.map((client: IClientProfile, index: number) => (
              <div
                key={index + 1}
                className={
                  "ac-list-row x-" +
                  (index % 2 === 0 ? "list-row-odd" : "list-row-even")
                }
              >
                <div className="ac-list-col col-3">
                  <a
                    onClick={() =>
                      navigateToClientAssessments(client.client_id!)
                    }
                  >
                    {client.client_name}
                  </a>
                </div>
                <div className="ac-list-col col-3">{client.customer_name}</div>
                <div className="ac-list-col col-2">
                  <span>
                    {client.last_updated_on
                      ? getFormattedDate(client.last_updated_on)
                      : ""}
                  </span>
                </div>
                <div className="ac-list-col col-2">
                  <span>{client.locations}</span>
                </div>
                <div className="ac-list-col col-2 ac-actions">
                  <button
                    className="ac-action-navigate"
                    onClick={() =>
                      navigateToClientAssessments(client.client_id!)
                    }
                  >
                    View Details
                  </button>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
    // <div className="md:my-10 w-full px-5 md:px-0 w-[89%] m-auto py-8 bg-white">
    //   <h2 className="text-2xl mb-10 px-10 font-medium">All Assessments</h2>
    //   <div className="px-10 relative">
    //     <FontAwesomeIcon
    //       icon={faSearch}
    //       className="tex-xl text-slate-400 absolute left-14 top-[0.8rem]"
    //     />
    //     <input
    //       className="w-1/2 pl-12 border border-slate-300 rounded-3xl mb-10 py-2 pr-3"
    //       placeholder="Search..."
    //       onChange={filterClients}
    //     />
    //   </div>
    //   <table className="w-full">
    //     <thead className="text-left bg-ngray-100">
    //       <tr>
    //         <td className="px-10 py-3">Name</td>
    //         <td className="px-10 py-3">Last Updated On</td>
    //         <td className="px-10 py-3"></td>
    //       </tr>
    //     </thead>
    //     <tbody className="text-left py-3">
    //       {clients.length === 0 ? (
    //         <tr>
    //           <td className="py-5 pl-10" colSpan={3}>
    //             No data found.
    //           </td>
    //         </tr>
    //       ) : (
    //         clients.map((client: IClientProfile, index: number) => (
    //           <tr
    //             key={index + 1}
    //             className={index % 2 === 1 ? "bg-ngreen-200 bg-opacity-10" : ""}
    //           >
    //             <td className="px-10 py-3">
    //               <button
    //                 className="text-ngreen-600"
    //                 onClick={() =>
    //                   navigateToClientAssessments(client.client_id!)
    //                 }
    //               >
    //                 {client.client_name}
    //               </button>
    //             </td>
    //             <td className="px-10 py-3">{client.last_updated_on && ""}</td>
    //             <td className="px-10 py-5 text-right">
    //               <button
    //                 className="bg-ngreen-600 text-white rounded-md px-5 py-2"
    //                 onClick={() =>
    //                   navigateToClientAssessments(client.client_id!)
    //                 }
    //               >
    //                 View Details
    //               </button>
    //             </td>
    //           </tr>
    //         ))
    //       )}
    //     </tbody>
    //   </table>
    // </div>
  );
};

const AllClientsPage = withLoader(AllClients);

export { AllClientsPage };
