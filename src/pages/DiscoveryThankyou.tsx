import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useNavigate } from "react-router-dom";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { faCircle as solidCircle } from "@fortawesome/free-solid-svg-icons/faCircle";
import { useState, useEffect, useMemo } from "react";
import { withLoader } from "../components";
import { IWithLoaderProps } from "../components/withLoader";
import { ClientAPI } from "../apis";
import { useAuth } from "../hooks";

interface DiscoveryThankYouProps {
  enable?: boolean;
}

type ComplexProps = DiscoveryThankYouProps & IWithLoaderProps;

const DiscoveryThankYou: React.FC<ComplexProps> = ({
  showLoader,
  enable = true,
}) => {
  const auth = useAuth();
  const navigate = useNavigate();
  const clientId = auth.userId;

  const handleNewAssessmentClick = () => {
    void (async () => {
      showLoader?.({ show: true });
      const newAssessmentId = await ClientAPI.initiateMaturitySurvey(clientId);
      navigate(`/assessment?type=maturity&id=${newAssessmentId}`);
      showLoader?.({ show: false });
    })();
  };

  return (
    <div className="md:px-0 w-[89%]  m-auto pt-5">
      <div className="inline pr-5">
        <div className="font-bold inline text-xl">
          {localStorage.getItem("company-name")}{" "}
        </div>
        <div className=" inline text-base">- Discovery Questions</div>
      </div>
      <div className="inline assessment-status-button text-base font-normal py-2">
        <FontAwesomeIcon
          icon={checkCircle}
          className="text-ngreen-600 mr-1 w-3"
        />
        Submitted
      </div>
      <div className="flex mt-5">
        <div className="shadow-lg section-navigation px-10 rounded-lg border-t-8 border-ngreen-400 h-fit pb-6 pt-10">
          <div>
            <ol className="elative border-l border-ngray-300">
              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 "
                  />{" "}
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-extrabold">
                      Company Information
                    </div>
                  </div>
                </div>
              </li>

              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 "
                  />
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-extrabold">
                      Cloud platform details
                    </div>
                  </div>
                </div>
              </li>

              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 "
                  />
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-extrabold">
                      Planning
                    </div>
                  </div>
                </div>
              </li>

              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 "
                  />
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-extrabold">
                      Modernization
                    </div>
                  </div>
                </div>
              </li>

              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <FontAwesomeIcon
                    icon={checkCircle}
                    className="text-ngreen-600 "
                  />
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-extrabold">
                      Cloud Cost
                    </div>
                  </div>
                </div>
              </li>
            </ol>
          </div>
        </div>
        <div className="w-full  pb-72  ml-10 shadow-lg rounded-lg border-t-8 border-ngreen-400 section-content-discovery  pb-24 overflow-hidden">
          <div className="">
            <div className="text-4xl text-ngreen-700 font-black py-4">
              Awesome!
            </div>
            <div className="text-xl mt-5 font-normal pb-8">
              You’ve finished the “Discovery Assessment” and now ready to start
              your “Assessment Journey”
            </div>
            <div className="border-t-2 border-gray-300 -mx-14" />
            <div className="flex pt-8">
              <button
                className={
                  "bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold " +
                  (!enable ? "opacity-40" : "")
                }
                disabled={!enable}
                onClick={handleNewAssessmentClick}
              >
                Start Assessment Journey
              </button>
              <button
                className="px-6 py-3 font-semibold mx-4"
                onClick={() => {
                  navigate("/", { replace: true });
                }}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const DiscoveryThankyou = withLoader(DiscoveryThankYou);

export { DiscoveryThankyou };
