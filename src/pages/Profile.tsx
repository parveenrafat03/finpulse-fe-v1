import { InputLabel, Stack, TextField, MenuItem, Select } from "@mui/material";
import { withLoader } from "../components";
import { FormContext, useForm } from "../hooks";
import { IWithLoaderProps } from "../components/withLoader";
import * as yup from "yup";
import { ClientAPI } from "../apis";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import {
  faBorderAll,
  faFloppyDisk,
  faXmark,
  faCircleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import "../pages-css/profile.css";
import { styled } from "@mui/system";
import { AssessmentStatus } from "../interfaces/assessment-details.interface";
import { ISurveyResponse } from "../apis/clientAPI";

const MyStyledSelect = styled(Select)({
  width: "100%",
  borderColor: "#2D807B",
});

export interface IClientProfile {
  client_id?: string;
  client_name: string;
  customer_name: string;
  email_address: string;
  locations: string;
  potential_application_of_cloud: string;
  customer_role?: string;
  contact_no?: string;
  last_updated_on?: string;
}

const loginFormSchema: yup.ObjectSchema<IClientProfile> = yup.object({
  client_name: yup.string().required(),
  customer_name: yup.string().required(),
  email_address: yup.string().required().email(),
  locations: yup.string().required(),
  potential_application_of_cloud: yup.string(),
  customer_role: yup.string(),
  contact_no: yup.string(),
  client_id: yup.string(),
  last_updated_on: yup.string(),
});

interface ProfileProps {
  refreshHeader: boolean;
  setRefreshHeader: (value: boolean) => void;
}

type ComplexProps = ProfileProps & IWithLoaderProps;

const Profile = ({
  showLoader,
  refreshHeader,
  setRefreshHeader,
}: ComplexProps) => {
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    // Fetch the list of countries
    fetch("https://restcountries.com/v2/all")
      .then((response) => response.json())
      .then((data) => {
        // Extract country names from the response
        const countryNames = data.map((country) => country.name);
        setCountries(countryNames);
      })
      .catch((error) => console.error("Error fetching countries:", error));
  }, []);

  const navigate = useNavigate();
  const [fetchedData, setFetchedData] = useState<IClientProfile | null>(null);
  const auth = useAuth();
  const clientId: string = auth.client ? auth.client.client_id : "";

  const form: FormContext<IClientProfile> = useForm<IClientProfile>({
    initialFormData: {
      client_name: fetchedData?.client_name ?? "",
      customer_name: fetchedData?.customer_name ?? "",
      email_address: fetchedData?.email_address ?? "",
      locations: fetchedData?.locations ?? "",
      potential_application_of_cloud: "",
      //   fetchedData?.potential_application_of_cloud ?? " ",
      customer_role: fetchedData?.customer_role ?? "",
      contact_no: fetchedData?.contact_no ?? "",
    },
    validations: loginFormSchema,
    onSubmit: async () => {
      showLoader?.({
        show: true,
        message: "Saving the details..",
      });
      form.values.client_id = clientId;
      await ClientAPI.saveProfile(form.values);
      setRefreshHeader(!refreshHeader);
      navigate(assessmentBtnPath, { replace: true });
      showLoader?.({
        show: false,
      });
    },
  });

  const [assessmentBtnText, setAssessmentBtnText] = useState("Save & Exit");
  const [assessmentBtnPath, setAssessmentBtnPath] = useState(
    "/assessment-history"
  );
  const [exitBtnPath, setExitBtnPath] = useState("/assessment-history");
  const [showTicks, setShowTicks] = useState<boolean>(false);
  const [section1Done, setSection1Done] = useState<boolean | null>(null);
  const [section2Done, setSection2Done] = useState<boolean | null>(null);
  const [section3Done, setSection3Done] = useState<boolean | null>(null);
  const [section4Done, setSection4Done] = useState<boolean | null>(null);

  useEffect(() => {
    const sessionCheck = () => {
      if (!auth.isSessionValid()) {
        auth.logout();
      }
    };
    sessionCheck();
    auth.markLastActivity();
    const intervalId = setInterval(sessionCheck, 15 * 1000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    void (async () => {
      try {
        showLoader?.({
          show: true,
          message: "Loading company details..",
        });
        const response = await ClientAPI.getClientProfile(clientId);
        setFetchedData(response);

        form.setFormData({
          client_name: response?.client_name ?? "",
          customer_name: response?.customer_name ?? "",
          email_address: response?.email_address ?? "",
          locations: response?.locations ?? "",
          potential_application_of_cloud: "",
          //   response?.potential_application_of_cloud ?? "",
          customer_role: response?.customer_role ?? "",
          contact_no: response?.contact_no ?? "",
        });
      } catch (error) {
        console.error("Error fetching data", error);
      }

      let clientProfile: IClientProfile = null;
      let discoveryAssessment: ISurveyResponse = null;

      try {
        [clientProfile, discoveryAssessment] = await Promise.all([
          ClientAPI.getClientProfile(auth.client!.client_id),
          ClientAPI.fetchDiscoverySurveyResponse(auth.client!.client_id),
        ]);
      } catch (error) {
        console.error("Error fetching discovery assessment:", error);
      }
      if (
        discoveryAssessment == null ||
        discoveryAssessment.assessment_status === null ||
        discoveryAssessment.assessment_status === AssessmentStatus.IN_PROGRESS
      ) {
        setAssessmentBtnText("Save & Continue to Discovery Assessment");
        setAssessmentBtnPath("/assessment?type=discovery");
        setExitBtnPath("/home");
        setShowTicks(false);
        console.log(discoveryAssessment);
        if (discoveryAssessment.survey_responses.length == 0) {
          setSection1Done(null);
          setSection2Done(null);
          setSection3Done(null);
          setSection4Done(null);
        } else {
          await setSectionState(discoveryAssessment);
        }
      } else {
        setSection1Done(true);
        setSection2Done(true);
        setSection3Done(true);
        setSection4Done(true);
        setShowTicks(true);
      }
      showLoader?.({
        show: false,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clientId]);

  const setSectionState = async (discoveryAssessment: ISurveyResponse) => {
    const questions = await ClientAPI.fetchDiscoveryAssessmentQuestions();
    const responses: { [key: string]: string } = {};
    discoveryAssessment.survey_responses.forEach((item) => {
      responses[item.question_id] = item.response;
    });
    const setter = {
      1: setSection1Done,
      2: setSection2Done,
      3: setSection3Done,
      4: setSection4Done,
    };
    for (var i = 1; i <= 4; i++) {
      var done = 0;
      var pending = 0;
      questions[i - 1].questions.forEach((question) => {
        if (responses[question.id].length > 0) {
          done++;
        } else {
          pending++;
        }
      });
      setter[i](done == 0 ? null : pending == 0);
    }
  };

  const exit = () => {
    navigate(exitBtnPath, { replace: true });
  };

  const navigation = (asid: number, title: string, isDone: boolean | null) => {
    return (
      <li className="mb-8 ml-6">
        <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
          {isDone == null ? (
            <div className="profile-non-check "></div>
          ) : isDone ? (
            <FontAwesomeIcon icon={checkCircle} className="text-ngreen-600" />
          ) : (
            <FontAwesomeIcon
              icon={faCircleExclamation}
              className="text-nyellow-600"
            />
          )}
        </span>
        <div className="mb-1 text-lg font-normal cursor-pointer false">
          <div className="flex section-option">
            <div className="w-72">
              {fetchedData == null ? (
                <>{title}</>
              ) : (
                <Link to={"/assessment?type=discovery&asid=" + asid}>
                  {title}
                </Link>
              )}
            </div>
          </div>
        </div>
      </li>
    );
  };

  return (
    <div className="md:px-0 w-[89%]  m-auto">
      <div className="pt-4 text-lg font-bold tracking-2">
        <h2>Welcome, let’s capture company information quickly !</h2>
      </div>
      <div className="flex mt-5">
        <div className="shadow-lg section-navigation px-10 rounded-lg border-t-8 border-ngreen-400 h-fit pb-6 pt-10">
          <div>
            <ol className="elative border-l border-ngray-300">
              <li className="mb-8 ml-6">
                <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
                  <div className="profile-check "></div>
                </span>
                <div className="mb-1 text-lg font-normal cursor-pointer false">
                  <div className="flex section-option">
                    <div className="w-72 text-ngreen-600 font-bold">
                      Company Information
                    </div>
                  </div>
                </div>
              </li>
              {navigation(1, "Cloud platform details", section1Done)}
              {navigation(2, "Planning", section2Done)}
              {navigation(3, "Modernization", section3Done)}
              {navigation(4, "Cloud Cost", section4Done)}
            </ol>
          </div>
        </div>
        <div className="w-full ml-10 shadow-lg rounded-lg border-t-8 border-ngreen-400 section-content-discovery  pb-24 ">
          <h1 className="text-xl font-bold">Company Information</h1>
          <h1 className="text-sm opacity-70 font-thin pt-2">
            <div className="inline font-bold opacity-50">ⓘ</div> Please provide
            company details filling up the form below and continue your
            discovery journey
          </h1>
          <form className="mt-10" onSubmit={(e) => void form.handleSubmit(e)}>
            <Stack spacing={1}>
              <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">Company Name*</span>
              </InputLabel>
              <TextField
                name="client_name"
                variant="outlined"
                margin="normal"
                value={form.values.client_name}
                onChange={form.handleChange}
                error={form.formErrors.client_name}
                helperText={
                  form.formErrors.client_name ? "Client name is required" : " "
                }
              />
              <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">StakeHolder Name*</span>
              </InputLabel>
              <TextField
                name="customer_name"
                variant="outlined"
                margin="normal"
                value={form.values.customer_name}
                onChange={form.handleChange}
                error={form.formErrors.customer_name}
                helperText={
                  form.formErrors.customer_name
                    ? "Customer name is required"
                    : " "
                }
              />

              <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">Country*</span>
              </InputLabel>
              <div className="profile-country-select pb-6">
                <select
                  className="border border-gray-500  p-4 rounded-md "
                  // labelId="country-select-label"
                  id="country-select"
                  name="locations"
                  // variant="outlined"
                  value={form.values.locations}
                  onChange={form.handleSelectChange}
                  // label="Select Country"
                  // error={form.formErrors.locations}
                  required
                  // helperText={
                  //   form.formErrors.locations ? "Country is required" : " "
                  // }
                >
                  {" "}
                  <option value="" disabled hidden>
                    Select Country
                  </option>
                  {countries.map((country, index) => (
                    <option key={index} value={country}>
                      {country}
                    </option>
                  ))}
                </select>
              </div>
              {/* <TextField
                name="locations"
                variant="outlined"
                margin="normal"
                value={form.values.locations}
                onChange={form.handleChange}
                error={form.formErrors.locations}
                helperText={
                  form.formErrors.locations ? "Country is required" : " "
                }
              /> */}

              <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">Email*</span>
              </InputLabel>
              <TextField
                name="email_address"
                variant="outlined"
                margin="normal"
                value={form.values.email_address}
                onChange={form.handleChange}
                error={form.formErrors.email_address}
                helperText={
                  form.formErrors.email_address
                    ? "Email address is required"
                    : " "
                }
              />

              {/* <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">Customer Role</span>
              </InputLabel>
              <TextField
                name="customer_role"
                variant="outlined"
                margin="normal"
                value={form.values.customer_role}
                onChange={form.handleChange}
                helperText=" "
              />
              <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">Contact Number</span>
              </InputLabel>
              <TextField
                name="contact_no"
                variant="outlined"
                margin="normal"
                value={form.values.contact_no}
                onChange={form.handleChange}
                helperText=" "
              /> */}

              {/* <InputLabel htmlFor={"username"}>
                <span className="text-nblue-200">
                  Potential Application of Cloud*
                </span>
              </InputLabel>
              <TextField
                name="potential_application_of_cloud"
                variant="outlined"
                margin="normal"
                value={form.values.potential_application_of_cloud}
                onChange={form.handleChange}
                error={form.formErrors.potential_application_of_cloud}
                helperText={
                  form.formErrors.potential_application_of_cloud
                    ? "Application is required"
                    : " "
                }
              /> */}
            </Stack>

            <div
              style={{
                boxShadow: "0px 0px 12px 0px #0000001A",
              }}
              className="bg-white p-2 flex justify-end shadow-lg
              user-detail-foot-menu flex justify-between py-4 px-4"
            >
              {/* <div className="mt-5 text-center "> */}
              <div className="flex">
                <button
                  className="px-5 py-3 rounded-md bg-ngray-100 text-black text-xl mr-10 "
                  onClick={exit}
                >
                  {/* <FontAwesomeIcon icon={faXmark} /> */}
                  <span className="font-bold px-2">Exit</span>
                </button>
              </div>
              <div className="flex">
                <button
                  className="p-6 rounded-md bg-ngreen-500 text-white text-xl"
                  type="submit"
                >
                  <span className="">{assessmentBtnText}</span>
                </button>
              </div>

              {/* <div className="w-30">
                <button className="px-5 py-2 rounded-md bg-ngray-100 text-black mr-10">
                  <span className="ml-2">Save as Draft</span>
                </button>
                <button
                  className="bg-ngreen-600 text-white rounded-md px-5 py-2 mr-10"
                  style={{ backgroundColor: "#47D7AC" }}
                >
                  Generate Report
                </button>
              </div> */}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

const ProfilePage = withLoader(Profile);

export { ProfilePage };
