import { ISection } from "../interfaces/section.interface";

const isLastSection = (sections: ISection[], sectionIndex: number) => {
  return sectionIndex === sections.length - 1;
};

const goToNextSubSection = (
  activeMainSection: ISection,
  activeSubSectionIndex: number,
): ISection => {
  const nextActiveSubSectionIndex = activeSubSectionIndex + 1;
  const subSections = activeMainSection.subSections ?? [];
  const nextActiveSubSection = subSections[nextActiveSubSectionIndex];
  return nextActiveSubSection;
};

const goToNextMainSection = (
  sectionItems: ISection[],
  activeMainSection: ISection,
): ISection => {
  const activeMainSectionIndex = sectionItems.findIndex((sectionItem) => {
    return sectionItem.id === activeMainSection.id;
  });
  const nextActiveMainSectionIndex = activeMainSectionIndex + 1;
  const nextActivemainSection = sectionItems[nextActiveMainSectionIndex];
  return nextActivemainSection;
};

const goToPrevSubSection = (
  activeMainSection: ISection,
  activeSubSectionIndex: number,
): ISection => {
  const prevSubSectionIndex = activeSubSectionIndex - 1;
  const subSections = activeMainSection.subSections ?? [];
  const prevSubSection = subSections[prevSubSectionIndex];
  return prevSubSection;
};

const goToPrevMainSection = (
  sectionItems: ISection[],
  activeMainSection: ISection,
): ISection => {
  const activeMainSectionIndex = sectionItems.findIndex((sectionItem) => {
    return sectionItem.id === activeMainSection.id;
  });
  const prevMainSectionIndex = activeMainSectionIndex - 1;
  const prevMainSection = sectionItems[prevMainSectionIndex];
  return prevMainSection;
};

const nextSection = (
  sectionItems: ISection[],
  activeMainSection: ISection,
  activeSubSection: ISection | undefined,
): ISectionToActivate | undefined => {
  // If we have subsections then below code handles that
  if (activeSubSection && activeMainSection.subSections) {
    const activeSubSectionIndex = activeMainSection.subSections.findIndex(
      (subSection) => {
        return subSection.id === activeSubSection.id;
      },
    );

    if (activeSubSectionIndex >= 0) {
      // if its not the last section go to next sub section
      if (
        !isLastSection(activeMainSection.subSections, activeSubSectionIndex)
      ) {
        const subSectionToActivate = goToNextSubSection(
          activeMainSection,
          activeSubSectionIndex,
        );
        return {
          section: subSectionToActivate,
          type: SectionType.SUB,
        };
      } else {
        /* if its last section check if the main section is complete or not
          if yes jump to next main section
          else jump to pending sub section
          */
        if (
          isActiveSectionFinal(
            sectionItems,
            activeMainSection,
            activeSubSection,
          )
        ) {
          // Direct to thank you page
        } else {
          const nextMainSection = goToNextMainSection(
            sectionItems,
            activeMainSection,
          );
          return {
            section: nextMainSection,
            type: SectionType.MAIN,
          };
        }
      }
    }
  } else {
    // This code handles when we do not have sub sections
    const activeMainSectionIndex = sectionItems.findIndex((section) => {
      return section.id === activeMainSection.id;
    });
    if (!isLastSection(sectionItems, activeMainSectionIndex)) {
      const nextMainSection = goToNextMainSection(
        sectionItems,
        activeMainSection,
      );
      return {
        section: nextMainSection,
        type: SectionType.MAIN,
      };
    }
  }
};

const previousSection = (
  sectionItems: ISection[],
  activeMainSection: ISection,
  activeSubSection: ISection | undefined,
): ISectionToActivate | undefined => {
  const activeMainSectionIndex = sectionItems.findIndex((section) => {
    return section.id === activeMainSection.id;
  });
  if (activeSubSection && activeMainSection.subSections) {
    const activeSubSectionIndex = activeMainSection.subSections.findIndex(
      (subSection) => {
        return subSection.id === activeSubSection.id;
      },
    );

    if (activeSubSectionIndex === 0) {
      if (activeMainSectionIndex > 0) {
        const prevMainSection = goToPrevMainSection(
          sectionItems,
          activeMainSection,
        );
        return {
          section: prevMainSection,
          type: SectionType.MAIN,
        };
      }
    } else if (activeSubSectionIndex > 0) {
      const prevSubSection = goToPrevSubSection(
        activeMainSection,
        activeSubSectionIndex,
      );
      return {
        section: prevSubSection,
        type: SectionType.SUB,
      };
    }
  } else {
    const activeMainSectionIndex = sectionItems.findIndex((section) => {
      return section.id === activeMainSection.id;
    });
    if (activeMainSectionIndex > 0) {
      const prevMainSection = goToPrevMainSection(
        sectionItems,
        activeMainSection,
      );
      return {
        section: prevMainSection,
        type: SectionType.MAIN,
      };
    }
  }
};

const isActiveSectionFinal = (
  sectionItems: ISection[],
  activeMainSection: ISection,
  activeSubSection: ISection | undefined,
) => {
  const activeMainSectionIndex = sectionItems.findIndex((section) => {
    return section.id === activeMainSection.id;
  });
  const isMainSectionFinal = isLastSection(
    sectionItems,
    activeMainSectionIndex,
  );
  if (
    activeMainSection.subSections?.length &&
    activeSubSection &&
    isMainSectionFinal
  ) {
    const activeSubSectionIndex = activeMainSection.subSections.findIndex(
      (subSection) => {
        return subSection.id === activeSubSection.id;
      },
    );
    const isSubSectionFinal = isLastSection(
      activeMainSection.subSections,
      activeSubSectionIndex,
    );
    return isSubSectionFinal;
  } else {
    return isMainSectionFinal;
  }
};

export interface ISectionToActivate {
  section: ISection;
  type: SectionType;
}

export enum SectionType {
  MAIN,
  SUB,
}

export { nextSection, previousSection, isActiveSectionFinal };
