export const formatDate = (inputDate: string | undefined) => {
  if (inputDate === undefined) {
    return "";
  }
  const date = new Date(inputDate);

  const getOrdinal = (n: any) => {
    const suffixes = ["th", "st", "nd", "rd"];
    const v = n % 100;
    return n + (suffixes[(v - 20) % 10] || suffixes[v] || suffixes[0]);
  };

  const formattedDate = `${getOrdinal(date.getDate())} ${date.toLocaleString(
    "default",
    { month: "short" }
  )}, ${date.getFullYear()}`;

  const formattedTime = date.toLocaleString("en-US", {
    hour: "numeric",
    minute: "numeric",
    hour12: true,
  });

  return `${formattedDate}_${formattedTime.toLowerCase()}`;
};

export const getFormattedDate = (inputDate: string | undefined) => {
  if (inputDate === undefined) {
    return "";
  }
  const date = new Date(inputDate);

  const getOrdinal = (n: any) => {
    const suffixes = ["th", "st", "nd", "rd"];
    const v = n % 100;
    return (
      (n < 10 ? "0" : "") +
      n +
      (suffixes[(v - 20) % 10] || suffixes[v] || suffixes[0])
    );
  };

  const formattedDate = `${getOrdinal(date.getDate())} ${date.toLocaleString(
    "default",
    { month: "short" }
  )}, ${date.getFullYear()}`;

  return `${formattedDate}`;
};

export const getDate = (input: string) => {
  const pos = input.indexOf("_");
  return input.substring(0, pos);
};

export const getTime = (input: string) => {
  const pos = input.indexOf("_");
  return input.substring(pos + 1);
};
