import {
  IQuestion,
  ISection,
  SectionState,
} from "../interfaces/section.interface";

const isQuestionAnswered = (question: IQuestion) => {
  if (!question.response) {
    return false;
  } else if (
    Array.isArray(question.response) &&
    question.response.length === 0
  ) {
    return false;
  } else {
    return true;
  }
};

const noOfPendingQuestionsInSection = (sectionItem: ISection): number => {
  return (
    sectionItem.questions?.filter((q) => !isQuestionAnswered(q)).length ?? 0
  );
};

const noOfPendingSections = (sections: ISection[]): number => {
  return sections.filter((section) => {
    return noOfPendingQuestionsInSection(section) > 0;
  }).length;
};

const noOfCompletedQuestionsInSection = (sectionItem: ISection): number => {
  return (
    sectionItem.questions?.filter((q) => isQuestionAnswered(q)).length ?? 0
  );
};

const noOfCompletedSections = (sections: ISection[]): number => {
  return sections.filter((section) => {
    return noOfPendingQuestionsInSection(section) === 0;
  }).length;
};

const isSectionCompleted = (section: ISection) => {
  const pendingQuestions = noOfPendingQuestionsInSection(section) > 0;
  const pendingSectionsRes = noOfPendingSections(section.subSections ?? []) > 0;
  return !(pendingQuestions || pendingSectionsRes);
};

const isSectionPartialFilled = (section: ISection) => {
  const pendingQuestions = noOfPendingQuestionsInSection(section) > 0;
  const pendingSections = noOfPendingSections(section.subSections ?? []) > 0;
  const completedQuestions = noOfCompletedQuestionsInSection(section) > 0;
  const completedSections =
    noOfCompletedSections(section.subSections ?? []) > 0;
  const partialQuestions = pendingQuestions && completedQuestions;
  const partialSections = pendingSections && completedSections;
  return partialQuestions || partialSections;
};

const getSectionItemState = (
  sectionItems: ISection[],
  sectionId: string,
  activeSectionId: string,
): SectionState => {
  const sectionItem = sectionItems.find(
    (sectionItem) => sectionItem.id === sectionId,
  );
  if (sectionItem) {
    const sectionCompleted = isSectionCompleted(sectionItem);
    const partialSection = isSectionPartialFilled(sectionItem);
    const isSectionActive = sectionItem.id === activeSectionId;
    return sectionCompleted
      ? SectionState.FINISHED
      : partialSection
      ? SectionState.PARTIALLY_FINISHED
      : isSectionActive
      ? SectionState.IN_PROGRESS
      : SectionState.PENDING;
  }
  return SectionState.PENDING;
};

const getQuestionAnswerMappingForSection = (
  section: ISection,
): IQuestionAnswerMapping[] => {
  const mapping: IQuestionAnswerMapping[] = [];
  section.questions?.forEach((question) => {
    mapping.push({
      question_id: question.id,
      response: question.response
        ? Array.isArray(question.response)
          ? question.response.join(",")
          : question.response
        : "",
    });
  });
  return mapping;
};

const getQuestionParentSectionMapping = (
  sections: ISection[],
): Map<string, string[]> => {
  const mapping = new Map<string, string[]>();
  sections.forEach((section) => {
    if (section.questions?.length) {
      section.questions.forEach((q) => mapping.set(q.id, [section.id]));
    }
    if (section.subSections?.length) {
      section.subSections.forEach((subSection) => {
        subSection.questions?.forEach((q) =>
          mapping.set(q.id, [section.id, subSection.id]),
        );
      });
    }
  });
  return mapping;
};

const allSectionsCompleted = (sections: ISection[]): boolean => {
  return sections.every((section) => {
    return isSectionCompleted(section);
  });
};

const getFirstPendingSection = (sections: ISection[]): ISection | undefined => {
  return sections.find((section) => {
    return !isSectionCompleted(section);
  });
};

export interface IQuestionAnswerMapping {
  question_id: string;
  response: string | null;
  discovery_survey_status?: string;
}

export {
  isQuestionAnswered,
  getSectionItemState,
  isSectionCompleted,
  getQuestionAnswerMappingForSection,
  getQuestionParentSectionMapping,
  allSectionsCompleted,
  getFirstPendingSection,
};
