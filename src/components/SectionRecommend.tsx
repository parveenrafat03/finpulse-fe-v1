import React, { useEffect, useState } from "react";
import FormControlLabel from "@mui/material/FormControlLabel";
import { ISection } from "../interfaces/section.interface";
import Switch, { SwitchProps } from "@mui/material/Switch";
import TextEditor from "../TextEditor";
import FormGroup from "@mui/material/FormGroup";
import { ClientAPI } from "../apis";
import { Tabs, Tab, Slider } from "@mui/material";
import { styled } from "@mui/material/styles";
import EditIcon from "../components/EditIcon";
import DeleteIcon from "../components/DeleteIcon";
import SaveIcon from "../components/SaveIcon";
import Checkbox from "@mui/material/Checkbox";
import { CustomSlider } from "../components/customSlider";
import {
  ICapabilityScore,
  IRecommendation,
} from "../interfaces/assessment-details.interface";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk } from "@fortawesome/free-solid-svg-icons";
import { ITargetScorePayload } from "../apis/clientAPI";

export interface SectionRecommendProps {
  clientId: string;
  assessmentId: string;
  section: ISection;
  recommendations: IRecommendation[];
  currentSectionId: string;
  isEditorOpen: boolean;
  editClicked: number | null;
  scores: ICapabilityScore[];
  previousScores: ICapabilityScore[];
  openEditor: (section: ISection) => void;
  saveRecommendation: (id: string) => void;
  handleChildData: (data: string) => void;
  closeEditor: () => void;
  updateRecommendation: (
    id: string,
    recommendation_number: string | number
  ) => void;
  handleEdit: (sectionId: string, recommendationIndex: number) => void;
  handleDelete: (recommendationNumber: string, capability_id: string) => void;
}

const SectionRecommend: React.FC<SectionRecommendProps> = (props) => {
  // props.scores?.filter((ob) => ob.Capability_ID == props.section.id)[0]?.Capability_Score
  const getScores = (): number[] => {
    var [currentScore, targetScore, pCurrentScore, pTargetScore] = [0, 0, 0, 0];
    var data: any;
    if (props.scores.length > 0) {
      data = props.scores?.filter(
        (ob) => ob.Capability_ID == props.section.id
      )[0];
      if (data !== null || data !== undefined) {
        currentScore = parseFloat("" + data.Capability_Score);
      }
    }
    if (props.previousScores.length > 0) {
      data = props.previousScores?.filter(
        (ob) => ob.Capability_ID == props.section.id
      )[0];
      if (data !== null || data !== undefined) {
        pCurrentScore = parseFloat("" + data.Capability_Score);
      }
    }
    return [currentScore, targetScore, pCurrentScore, pTargetScore];
  };

  const [switchActive, setSwitchActive] = useState(false);
  const [checkActive, setCheckActive] = useState(false);
  const [sliderData, setSliderData] = useState({
    min: 0,
    max: 10,
    currentScore: getScores()[0],
    targetScore: 0,
    previousCurrentScore: getScores()[2],
    previousTargetScore: 8.75,
    showTargetScore: switchActive,
    showPreviousScores: checkActive,
  });

  useEffect(() => {
    setSliderData((prevData) => ({
      ...prevData, // Spread the previous state
      currentScore: getScores()[0], // Update the targetScore property
      previousCurrentScore: getScores()[2], // Update the targetScore property
    }));
  }, [props]);

  const setTargetScore = (value: number) => {
    setSliderData((prevData) => ({
      ...prevData, // Spread the previous state
      targetScore: value, // Update the targetScore property
    }));
  };

  const saveTargetScore = async () => {
    const payload: ITargetScorePayload = {
      client_id: props.clientId,
      assessment_id: props.assessmentId,
      capability_id: props.currentSectionId,
      capability_target_score: sliderData.targetScore.toString(),
    };
    await ClientAPI.submitTargetScore(payload);
    console.log("Target Score -> " + payload);
  };

  const RangeSlider = styled(Slider)({
    color: "#47D7AC",
    height: 8,
    width: "40%",
    "& .MuiSlider-track": {
      border: "none",
    },
    "& .MuiSlider-thumb": {
      height: 7,
      width: 7,
      backgroundColor: "#47D7AC",
      "&:focus, &:hover, &.Mui-active, &.Mui-focusVisible": {
        boxShadow: "inherit",
      },
      "&:before": {
        display: "none",
      },
    },
    "& .MuiSlider-valueLabel": {
      lineHeight: 1.2,
      fontSize: 12,
      background: "unset",
      padding: 0,
      width: 32,
      height: 32,
      backgroundColor: "#47D7AC",
      translate: "0 60px",
      "&:before": { top: "-6px" },
    },
  });

  const IOSSwichAction = () => {
    var value = false;
    var newTargetScore = sliderData.targetScore;
    if (switchActive) {
      setSwitchActive(false);
      value = false;
      newTargetScore = 0;
    } else {
      setSwitchActive(true);
      value = true;
    }
    if (newTargetScore !== sliderData.targetScore) {
      saveTargetScore();
    }
    setSliderData((prevData) => ({
      ...prevData, // Spread the previous state
      showTargetScore: value, // Update the showTargetScore property
      targetScore: newTargetScore, // Update the showTargetScore property
    }));
  };

  const checkboxAction = () => {
    var value = false;
    if (checkActive) {
      setCheckActive(false);
      value = false;
    } else {
      setCheckActive(true);
      value = true;
    }
    setSliderData((prevData) => ({
      ...prevData, // Spread the previous state
      showPreviousScores: value, // Update the showPreviousScores property
    }));
  };

  const IOSSwitch = styled((props: SwitchProps) => (
    <Switch
      focusVisibleClassName=".Mui-focusVisible"
      disableRipple
      onClick={IOSSwichAction}
      {...props}
    />
  ))(({ theme }) => ({
    width: 37,
    height: 21,
    padding: 0,
    "& .MuiSwitch-switchBase": {
      padding: 0,
      margin: 2,
      transitionDuration: "300ms",
      "&.Mui-checked": {
        transform: "translateX(16px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          backgroundColor:
            theme.palette.mode === "dark" ? "#8893A5" : "#47D7AC",
          opacity: 1,
          border: 0,
        },
        "&.Mui-disabled + .MuiSwitch-track": {
          opacity: 0.5,
        },
      },
      "&.Mui-focusVisible .MuiSwitch-thumb": {
        color: "#33cf4d",
        border: "6px solid #fff",
      },
      "&.Mui-disabled .MuiSwitch-thumb": {
        color:
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: theme.palette.mode === "light" ? 0.7 : 0.3,
      },
      '.MuiSlider-mark[data-index="1"]': {
        // backbackgroundColor: theme.palette.mode === 'dark' ? '#8893A5' : '#47D7AC',
        backbackgroundColor: "blue",
        opacity: 1,
        border: 0,
        height: 10,
        width: 3,
      },
      '.MuiSlider-mark[data-index="2"]': {
        //backbackgroundColor: theme.palette.mode === 'dark' ? '#8893A5' : '#47D7AC',
        backbackgroundColor: "red",
        opacity: 1,
        border: 0,
        height: 10,
        width: 3,
      },
    },
    "& .MuiSwitch-thumb": {
      boxSizing: "border-box",
      width: 17,
      height: 17,
    },
    "& .MuiSwitch-track": {
      borderRadius: 21 / 2,
      backgroundColor: theme.palette.mode === "light" ? "#8893A5" : "#8893A5",
      opacity: 1,
      transition: theme.transitions.create(["background-color"], {
        duration: 500,
      }),
    },
  }));

  return (
    <div
      id={props.section.id}
      key={props.section.id}
      className="section-recommend "
    >
      <div className="flex justify-between">
        <div className="heading">{props.section?.title}</div>
        <div className="heading-menu">
          <div className="menu-icon">
            {switchActive ? (
              <FontAwesomeIcon
                icon={faFloppyDisk}
                onClick={saveTargetScore}
                className=""
              />
            ) : (
              <></>
            )}
          </div>
          <div className="menu-form">
            <FormControlLabel
              control={<IOSSwitch sx={{ m: 1 }} checked={switchActive} />}
              label="Set Target Score"
            />
            <FormControlLabel
              control={
                <Checkbox
                  sx={{
                    color: "grey",
                    "&.Mui-checked": {
                      color: "turquoise",
                    },
                  }}
                  checked={checkActive}
                  onClick={checkboxAction}
                />
              }
              label="Show previous score"
            />
          </div>
        </div>
      </div>
      <div className="score">
        <CustomSlider {...sliderData} setTargetScore={setTargetScore} />
      </div>
      <div className="rec-heading">Recommendation</div>
      <div>
        {props.recommendations?.map((recommendation, index) => {
          const isLastRecommendation =
            index === props.recommendations?.length - 1;
          const isEditing = props.editClicked === index;
          if (recommendation.capability_id === props.section?.id) {
            return (
              <>
                <div
                  className={
                    !isLastRecommendation
                      ? "recommend-box recommend-box-border"
                      : "recommend-box"
                  }
                >
                  {!isEditing ? (
                    <>
                      <div className="flex items-center justify-between">
                        <div className="recommend-title">
                          {"Recommendation " +
                            recommendation.recommendation_number}
                        </div>
                        <div className="recommend-menu flex gap-10">
                          <div
                            className="cursor-pointer flex items-center"
                            onClick={() =>
                              props.handleEdit(props.section.id, index)
                            }
                          >
                            <EditIcon />
                            <div>Edit</div>
                          </div>
                          <div
                            className="cursor-pointer flex items-center"
                            onClick={() =>
                              props.handleDelete(
                                recommendation.recommendation_number,
                                props.section.id
                              )
                            }
                          >
                            <DeleteIcon />
                            <div>Delete</div>
                          </div>
                        </div>
                      </div>
                      <div
                        className="recommend-text"
                        dangerouslySetInnerHTML={{
                          __html: recommendation.recommendation,
                        }}
                      ></div>
                    </>
                  ) : (
                    <>
                      <TextEditor
                        onDataFromChild={props.handleChildData}
                        initialText={recommendation.recommendation}
                      />
                      <div className="flex gap-5 items-center justify-end recommend-foot-menu">
                        <button onClick={props.closeEditor}>Cancel</button>
                        <button
                          className="flex gap-3 items-center bg-white text-ngreen-400 border border-ngreen-400 rounded-md"
                          onClick={() =>
                            props.updateRecommendation(
                              props.currentSectionId,
                              recommendation.recommendation_number
                            )
                          }
                        >
                          <SaveIcon />
                          <div>Save</div>
                        </button>
                      </div>
                    </>
                  )}
                </div>
              </>
            );
          } else {
            return <></>;
          }
        })}
        {props.isEditorOpen &&
          props.editClicked == null &&
          props.currentSectionId == props.section.id && (
            <div className="mt-6">
              <TextEditor
                onDataFromChild={props.handleChildData}
                initialText={""}
              />
              <div className="flex gap-5 items-center justify-end recommend-foot-menu">
                <button onClick={props.closeEditor}>Cancel</button>
                <button
                  className="flex gap-3 items-center bg-white text-ngreen-400 border border-ngreen-400 rounded-md"
                  onClick={() =>
                    props.saveRecommendation(props.currentSectionId)
                  }
                >
                  <SaveIcon />
                  <div>Save</div>
                </button>
              </div>
            </div>
          )}
      </div>
      <button
        className="add-recommend"
        onClick={() => props.openEditor(props.section)}
      >
        + Add Recommendation
      </button>
    </div>
  );
};

export default SectionRecommend;
