const MaturitySubmitted = () => {
  return (
    <div className="">
      <div className="text-4xl text-ngreen-700 font-black	py-4">
        Assessment submitted successfully!
      </div>
      <div className="text-xl mt-5 font-normal pb-8">
        Our team will get back to you after analysing your response. You can
        explore your dashboard to view the results and refer back to your
        response.
      </div>
      <div className="border-t-2 border-gray-300 -mx-14" />
      <div className="flex pt-8">
        <div className="bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold ">
          Go to Assessment Journey{" "}
        </div>
        <div className="px-6 py-3 font-semibold mx-4">Close</div>
      </div>
    </div>
  );
};

export { MaturitySubmitted };
