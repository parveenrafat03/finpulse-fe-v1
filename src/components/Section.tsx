import { useState, useEffect, useMemo } from "react";
import { ISection } from "../interfaces/section.interface";
import { getSectionItemState } from "../utils/questionaire";
import { SectionCore } from "./SectionCore";
import { SectionItem } from "./SectionItem";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { AssessmentType } from "../interfaces";
import { useLocation } from "react-router-dom";

// function useQuery() {
//   const { search } = useLocation();
//   return useMemo(() => new URLSearchParams(search), [search]);
// }
// const query = useQuery();
// const assessmentType = query.get("type") as AssessmentType;
// console.log(assessmentType);

const Section = ({
  submitAttempted,
  sectionItems,
  isSubsection,
  activeSectionId,
  activeSubSectionId,
  onActiveSubSectionClicked,
  onActiveMainSectionClicked,
}: ISectionProp) => {
  const sectionClick = (id: string) => {
    console.log(id);
    const clickedSection = sectionItems.find((sectionItem) => {
      return sectionItem.id === id;
    });
    if (clickedSection) {
      if (isSubsection) {
        onActiveSubSectionClicked(clickedSection);
      } else {
        onActiveMainSectionClicked(clickedSection);
      }
    }
  };

  const sectionItemComponents = sectionItems.map((sectionItem) => {
    return (
      activeSectionId && (
        <SectionItem
          {...sectionItem}
          submitAttempted={submitAttempted}
          isActive={sectionItem.id === activeSectionId}
          titleClick={sectionClick}
          key={sectionItem.id}
          state={getSectionItemState(
            sectionItems,
            sectionItem.id,
            activeSectionId
          )}
          isSubsectionItem={isSubsection}
          activeSubSectionId={activeSubSectionId}
          subSectionClicked={onActiveSubSectionClicked}
          mainSectionClicked={onActiveMainSectionClicked}
        ></SectionItem>
      )
    );
  });

  return (
    <SectionCore isSubsection={isSubsection}>
      <div className="elative border-l border-ngray-300 "></div>
      {/* <ol
        className="elative border-l border-ngray-300 "
        // className={
        //   "elative border-l border-ngray-300 " +
        //   (assessmentType === " maturity" ? " hidden " : "")
        // }
      >
        <li className="mb-8 ml-6">
          <span className="absolute flex items-center justify-center w-6 h-6 rounded-full -ml-9 -mt-1">
            <FontAwesomeIcon icon={checkCircle} className="text-ngreen-600" />
          </span>
          <div className="mb-1 text-lg font-normal cursor-pointer false">
            <div className="flex section-option">
              <div className="w-72 text-ngreen-600 font-bold">
                Company Information
              </div>
            </div>
          </div>
        </li>
      </ol> */}
      {sectionItemComponents}
    </SectionCore>
  );
};

export { Section };

interface ISectionProp {
  submitAttempted: boolean;
  sectionItems: ISection[];
  activeSectionId?: string;
  activeSubSectionId?: string;
  isSubsection?: boolean;
  onActiveMainSectionClicked: (activeMainSection: ISection) => void;
  onActiveSubSectionClicked: (activeSubSection: ISection) => void;
}
