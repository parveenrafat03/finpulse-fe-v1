import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  AssessmentStatus,
  AssessmentType,
  IAssessmentDetails,
} from "../interfaces";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { Menu, MenuItem } from "@mui/material";
import { ReactNode, useState } from "react";
import { faEdit, faTrashCan } from "@fortawesome/free-regular-svg-icons";
import { Link } from "react-router-dom";
import { Role } from "../typings";
import { useAuth } from "../hooks";

const LinkToAssessment = ({
  assessmentType,
  assessmentId,
  previousAssessmentId,
  children,
}: IAssessmentLinkProps) => (
  <Link
    to={
      assessmentType === AssessmentType.DISCOVERY
        ? `/assessment?type=${assessmentType.toLowerCase()}`
        : `/assessment?type=${assessmentType.toLowerCase()}&id=${assessmentId}&pid=${previousAssessmentId}`
    }
  >
    {children}
  </Link>
);

const AssessmentRows = ({
  assessments,
  assessmentType,
}: IAssessmentListProps) => {
  const [anchorEl, setAnchorEl] = useState<SVGSVGElement | null>(null);
  //const [previousAssessmentId, setPreviousAssessmentId] =
  useState<string>(null);
  //const [currentAssessmentId, setCurrentAssessmentId] = useState<string>("0");
  const open = Boolean(anchorEl);
  const auth = useAuth();

  const handleClick = (event: React.MouseEvent<SVGSVGElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  var previousAssessmentId = "0";
  var currentAssessmentId = "0";
  const assessmentRowsHTML = assessments.map((assessment, index) => {
    previousAssessmentId = currentAssessmentId;
    currentAssessmentId = assessment.assessment_id;
    return (
      <tr
        key={assessment.assessment_id + index}
        className={index % 2 === 1 ? "list-row-even" : "list-row-odd"}
      >
        <td className="al-table-tb-td">
          <LinkToAssessment
            assessmentType={assessmentType}
            assessmentId={assessment.assessment_id}
            previousAssessmentId={previousAssessmentId}
          >
            {assessmentType === AssessmentType.DISCOVERY
              ? "Discovery"
              : `${assessment.assessment_name}`}
          </LinkToAssessment>
        </td>
        <td className="al-table-tb-td">
          <div className="date">
            {new Date(assessment.created_date).toLocaleDateString()}
          </div>
          <div className="time">
            {new Date(assessment.created_date).toLocaleTimeString()}
          </div>
        </td>
        <td className="al-table-tb-td">
          <div className="date">
            {new Date(assessment.last_modified_date).toLocaleDateString()}
          </div>
          <div className="time">
            {new Date(assessment.last_modified_date).toLocaleTimeString()}
          </div>
        </td>
        <td className="al-table-tb-td">
          <div
            className={
              assessment.assessment_status === AssessmentStatus.DONE
                ? "btn-status btn-status-done"
                : "btn-status btn-status-progress"
            }
          >
            {assessment.assessment_status === AssessmentStatus.DONE
              ? "Completed"
              : "Survey In-progress"}
          </div>
          <div>
            <span className="date mini-dt">
              {new Date(assessment.last_modified_date).toLocaleDateString()}
            </span>
            <span className="time mini-dt">
              ({new Date(assessment.last_modified_date).toLocaleTimeString()})
            </span>
          </div>
        </td>
        <td className="al-table-tb-td">
          <div className="flex items-center justify-between">
            <LinkToAssessment
              assessmentType={assessmentType}
              assessmentId={assessment.assessment_id}
              previousAssessmentId={previousAssessmentId}
            >
              <button
                className={
                  assessment.assessment_status === AssessmentStatus.DONE
                    ? "btn-action btn-action-done"
                    : "btn-action btn-action-progress"
                }
              >
                {auth.client!.roles === Role.ADMIN ||
                assessment.assessment_status === AssessmentStatus.DONE
                  ? "View Response"
                  : "Resume Assessment"}
              </button>
            </LinkToAssessment>
            <div>
              <FontAwesomeIcon
                icon={faEllipsisVertical}
                className="cursor-pointer text-xl opacity-50"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                id="menu-icon"
              />
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "menu-icon",
                }}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
              >
                <MenuItem onClick={handleClose}>
                  <LinkToAssessment
                    assessmentType={assessmentType}
                    assessmentId={assessment.assessment_id}
                    previousAssessmentId={previousAssessmentId}
                  >
                    <FontAwesomeIcon
                      icon={faEdit}
                      className="mr-3 text-ngreen-600"
                    />
                    <span className="action-dropdown-item">
                      Edit Assessment
                    </span>
                  </LinkToAssessment>
                </MenuItem>
                <MenuItem onClick={handleClose}>
                  <FontAwesomeIcon
                    icon={faTrashCan}
                    className="mr-3 text-red-600"
                  />
                  <span className="action-dropdown-item">
                    Delete Assessment
                  </span>
                </MenuItem>
              </Menu>
            </div>
          </div>
        </td>
      </tr>
    );
  });

  return assessmentRowsHTML;
};

const AssessmentList = ({
  assessmentType,
  assessments,
}: IAssessmentListProps) => {
  return assessments.length === 0 ? (
    ""
  ) : (
    <div className="border al-container">
      <div className="al-title">
        {assessmentType === AssessmentType.DISCOVERY
          ? "Discovery Phase Questionnaire"
          : "Maturity Assessment"}
      </div>
      <table className="al-table">
        <thead className="al-table-th">
          <tr>
            <td className="al-table-th-td">Name</td>
            <td className="al-table-th-td">Start Date</td>
            <td className="al-table-th-td">Last Updated On</td>
            <td className="al-table-th-td">Status</td>
            <td className="al-table-th-td"></td>
          </tr>
        </thead>
        <tbody className="al-table-tb">
          <AssessmentRows
            assessments={assessments}
            assessmentType={assessmentType}
          />
        </tbody>
      </table>
    </div>
  );
};

export { AssessmentList };

interface IAssessmentListProps {
  assessmentType: AssessmentType;
  assessments: IAssessmentDetails[];
}

export interface IAssessmentLinkProps {
  assessmentType: AssessmentType;
  assessmentId: string;
  previousAssessmentId: string;
  children: ReactNode;
}
