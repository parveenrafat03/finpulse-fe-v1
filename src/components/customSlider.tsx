import { useEffect, useRef, useState } from "react";
import "./customSliderStyle.css";

export interface CustomSliderProps {
  min: number;
  max: number;
  currentScore: number | null;
  targetScore: number | null;
  previousCurrentScore: number | null;
  previousTargetScore: number | null;
  showTargetScore: boolean;
  showPreviousScores: boolean;
  setTargetScore: (value: number) => void;
}

const CustomSlider: React.FC<CustomSliderProps> = (props) => {
  const divRef = useRef<HTMLDivElement>(null);
  const [width, setWidth] = useState(0);
  const [csWidth, setCsWidth] = useState(0);
  const [tsWidth, setTsWidth] = useState(0);
  const [pcsWidth, setPcsWidth] = useState(0);
  const [ptsWidth, setPtsWidth] = useState(0);

  const csStyle: React.CSSProperties = {
    width: `${csWidth}px`,
  };
  const tsStyle: React.CSSProperties = {
    width: `${tsWidth}px`,
  };
  const pcsStyle: React.CSSProperties = {
    width: `${pcsWidth}px`,
  };
  const ptsStyle: React.CSSProperties = {
    width: `${ptsWidth}px`,
  };
  const csNotchStyle: React.CSSProperties = {
    left: `${csWidth}px`,
  };
  const tsNotchStyle: React.CSSProperties = {
    left: `${tsWidth}px`,
  };
  const pcsNotchStyle: React.CSSProperties = {
    left: `${pcsWidth}px`,
  };
  const ptsNotchStyle: React.CSSProperties = {
    left: `${ptsWidth}px`,
  };

  const [isDragging, setIsDragging] = useState(false);
  const [leftOffset, setLeftOffset] = useState<number>(0);
  const [initialLeft, setInitialLeft] = useState<number>(0);
  const sliderRef = useRef<HTMLDivElement>(null);

  const handleMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    setIsDragging(true);
    setInitialLeft(e.clientX - leftOffset);
  };

  //   const handleMouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
  //     if (isDragging && sliderRef.current) {
  //       var newPosition = e.clientX - initialLeft;
  //       if (newPosition < 0) {
  //         newPosition = 0;
  //       } else if (newPosition > width) {
  //         newPosition = width;
  //       }
  //       setLeftOffset(newPosition);
  //       sliderRef.current.style.left = `${newPosition}px`;
  //       const newScore = Number(
  //         (props.min + (newPosition / width) * (props.max - props.min)).toFixed(2)
  //       );
  //       props.setTargetScore(newScore);
  //     }
  //   };

  const handleMouseMove = (e: MouseEvent) => {
    if (isDragging && sliderRef.current) {
      var newPosition = e.clientX - initialLeft;
      if (newPosition < 0) {
        //setLeftOffset(0);
        newPosition = 0;
        console.log(newPosition);
      } else if (newPosition > width) {
        //setLeftOffset(width);
        newPosition = width;
      }
      //   } else {
      //
      //   }
      setLeftOffset(newPosition);
      //   console.log(newPosition);
      //   console.log(width);
      const newScore = Number(
        (props.min + (newPosition / width) * (props.max - props.min)).toFixed(2)
      );
      props.setTargetScore(newScore);
    }
  };

  const handleMouseUp = () => {
    setIsDragging(false);
  };

  const placeElements = () => {
    if (divRef.current) {
      var width: number = divRef.current.offsetWidth;
      var range = props.max - props.min;
      var csw = 0,
        tsw = 0,
        pcsw = 0,
        ptsw = 0;
      if (props.currentScore != null) {
        csw = ((props.currentScore - props.min) / range) * width;
      }
      if (props.targetScore != null) {
        tsw = ((props.targetScore - props.min) / range) * width;
      }
      if (props.previousCurrentScore != null) {
        pcsw = ((props.previousCurrentScore - props.min) / range) * width;
      }
      if (props.previousTargetScore != null) {
        ptsw = ((props.previousTargetScore - props.min) / range) * width;
      }
      setWidth(width);
      setCsWidth(csw);
      setTsWidth(tsw);
      setPcsWidth(pcsw);
      setPtsWidth(ptsw);
      setLeftOffset(tsw);
    }
  };

  useEffect(() => {
    placeElements();
  }, [divRef, props]);

  useEffect(() => {
    window.addEventListener("resize", placeElements);
    return () => {
      window.removeEventListener("resize", placeElements);
    };
  }, [props]);

  useEffect(() => {
    if (isDragging) {
      document.addEventListener("mousemove", handleMouseMove);
      document.addEventListener("mouseup", handleMouseUp);

      return () => {
        document.removeEventListener("mousemove", handleMouseMove);
        document.removeEventListener("mouseup", handleMouseUp);
      };
    }
  }, [isDragging, handleMouseMove, handleMouseUp]);

  return (
    <div className="slider-wrapper">
      <div className="slider-title">Score</div>
      <div className="slider-container">
        <div className="slider-track" ref={divRef}>
          <div className="slider slider-cs" style={csStyle}></div>
          {props.showTargetScore ? (
            <>
              <div className="slider slider-ts" style={tsStyle}></div>
            </>
          ) : (
            <></>
          )}
          {props.showPreviousScores ? (
            <>
              <div className="slider slider-pcs" style={pcsStyle}></div>
              <div className="slider slider-pts" style={ptsStyle}></div>
            </>
          ) : (
            <></>
          )}
        </div>
        <div className="slider-notch">
          <div className="label-min">{props.min}</div>
          <div className="notch notch-cs" style={csNotchStyle}>
            <div className="notch-label">{props.currentScore}</div>
          </div>
          {props.showPreviousScores ? (
            <>
              <div className="notch notch-pcs" style={pcsNotchStyle}>
                <div className="notch-label">{props.previousCurrentScore}</div>
              </div>
              <div className="notch notch-pts" style={ptsNotchStyle}>
                <div className="notch-label">{props.previousTargetScore}</div>
              </div>
            </>
          ) : (
            <></>
          )}
          {props.showTargetScore ? (
            <>
              <div className="notch notch-ts" style={tsNotchStyle}>
                <div className="notch-label">{props.targetScore}</div>
              </div>
            </>
          ) : (
            <></>
          )}
          <div className="label-max">{props.max}</div>
        </div>
      </div>
      {props.showTargetScore ? (
        <>
          <div className="parentDiv">
            <div
              className="slidableDiv"
              ref={sliderRef}
              style={{ left: `${leftOffset}px` }}
              onMouseDown={handleMouseDown}
              //   onMouseMove={handleMouseMove}
              onMouseUp={handleMouseUp}
            ></div>
          </div>
        </>
      ) : (
        <></>
      )}
      <div className="legends">
        <div className="legend">
          <div className="legend-box tas"></div>
          <div className="legend-text">Target Assessment Score</div>
        </div>
        <div className="legend">
          <div className="legend-box cas"></div>
          <div className="legend-text">Current Assessment Score</div>
        </div>
        <div className="legend">
          <div className="legend-box pas"></div>
          <div className="legend-text">Previous Assessment Score</div>
        </div>
        <div className="legend">
          <div className="legend-box ptas"></div>
          <div className="legend-text">Previous Target Assessment Score</div>
        </div>
      </div>
    </div>
  );
};

export { CustomSlider };
