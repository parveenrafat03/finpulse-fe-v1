import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const ConfirmSubmission = ({ onCancel, onSubmit }: IConfirmSubmissionProps) => {
  return (
    <div>
      <Dialog
        open={true}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className=""
        sx={{
          backdropFilter: "blur(2px) sepia(5%)",
        }}
        PaperProps={{ sx: { borderRadius: "10px" } }}
      >
        <DialogTitle id="alert-dialog-title" className="flex">
          <div className=" text-ngreen-600 pt-3 font-extrabold">
            You’re about to submit
          </div>
          <button className="ml-auto w-6 h-6 mt-auto" onClick={onCancel}>
            <FontAwesomeIcon icon={faXmark} className=" w-6 h-6 mt-auto" />
          </button>
        </DialogTitle>
        <DialogContentText id="alert-dialog-description" className="px-8 py-5">
          <div className="text-base font-extrabold ">
            Thanks for your response. Once response is submitted,
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br /> you’ll not be able to make changes.
          </div>
          <div className="mt-8 text-base  font-bold">
            Do you want to continue to submit.{" "}
          </div>
        </DialogContentText>

        <div className="text-right mb-8 mx-8">
          <button
            className="px-5 py-2 rounded-md text-nblue-600 bg-gray-100  text-base mr-5"
            onClick={onCancel}
          >
            No
          </button>
          <button
            className="px-5 py-2 rounded-md bg-ngreen-500 text-white text-base"
            onClick={onSubmit}
          >
            Yes
          </button>
        </div>
      </Dialog>
    </div>
  );
};

export { ConfirmSubmission };

interface IConfirmSubmissionProps {
  onCancel: () => void;
  onSubmit: () => void;
}
