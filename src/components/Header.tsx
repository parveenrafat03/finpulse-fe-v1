import { useState, MouseEvent, useEffect } from "react";
import { Button, Menu, MenuItem } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons/faChevronDown";
import Logo from "../assets/logo.svg";
import { useAuth } from "../hooks";
import { Link, useNavigate } from "react-router-dom";
import { ClientAPI } from "../apis";
import { IClientProfile } from "../pages/Profile";

interface HeaderProps {
  showLogin: boolean;
  refreshHeader: boolean;
}

const Header: React.FC<HeaderProps> = ({ showLogin, refreshHeader }) => {
  // Variables: To holding states
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [clientProfile, setClientProfile] = useState<IClientProfile | null>(
    null
  );
  const navigate = useNavigate();
  const auth = useAuth();
  const clientId: string = auth.userId !== "" ? auth.userId : "";
  const isOpen = Boolean(anchorEl);

  // Function: To calculate 2 letter name of customer
  const customerInitials = (customerName?: string): string => {
    if (customerName) {
      const names: string[] = customerName.split(" ");

      return names.length > 1
        ? names[0].charAt(0) + names[1].charAt(0)
        : names[0].substring(0, 2);
    }
    return "";
  };

  // Function: To show drop-down menu
  const handleMenuClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  // Function: To hide drop-down menu
  const handleClose = () => {
    setAnchorEl(null);
  };

  // Function: To handle logout
  const handleLogout = () => {
    handleClose();
    auth.logout();
    //navigate("/", { replace: true });
    window.location.reload();
  };

  useEffect(() => {
    void (async () => {
      try {
        if (clientId.length > 0) {
          const response = await ClientAPI.getClientProfile(clientId);
          setClientProfile(response);
          console.log(response);
        } else {
          setClientProfile(null);
        }
      } catch (error) {
        console.error("Error fetching data", error);
      }
    })();
  }, [clientId, refreshHeader]);

  return (
    <header className="w-full bg-ngreen-700 py-2 sticky top-0 z-30">
      <div className="flex items-center px-5 md:px-0 w-[89%] m-auto">
        <Link to={"/"}>
          <div className="header-banner">
            <img src={Logo} alt="nagarro logo" />
            <div className="app-name">FinPulse</div>
            <div className="org-name">by nagarro</div>
          </div>
        </Link>
        <div className="ml-auto relative text-white">
          {!auth.client && showLogin && (
            <Link to={"/login"}>
              <button className="login-button">Login</button>
            </Link>
          )}

          {auth.client && (
            <>
              <Button
                id="basic-button"
                aria-controls={isOpen ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={isOpen ? "true" : undefined}
                onClick={handleMenuClick}
                color="inherit"
              >
                <div className="rounded-full leading-4 text-xs bg-nsilver-100 px-2 py-2 mr-3 text-white">
                  {customerInitials(clientProfile?.customer_name)}
                </div>
                <div className="capitalize">{clientProfile?.customer_name}</div>
                <FontAwesomeIcon icon={faChevronDown} className="pl-3" />
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={isOpen}
                onClose={handleClose}
                anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
                transformOrigin={{ vertical: "top", horizontal: "right" }}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
              </Menu>
            </>
          )}
        </div>
      </div>
    </header>
  );
};

export { Header };
