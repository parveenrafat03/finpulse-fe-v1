const QuestionSubmitted = () => {
  return (
    <div className="">
      <div className="text-4xl text-ngreen-700 font-black	py-4">Awesome!</div>
      <div className="text-xl mt-5 font-normal pb-8">
        You’ve finished the “Discovery Assessment” and now ready to start your
        “Assessment Journey”
      </div>
      <div className="border-t-2 border-gray-300 -mx-14" />
      <div className="flex pt-8">
        <div className="bg-ngreen-500 rounded-lg px-6 py-3 text-white font-semibold ">
          Start Assessment
        </div>
        <div className="px-6 py-3 font-semibold mx-4">Close</div>
      </div>
    </div>
  );
};

export { QuestionSubmitted };
