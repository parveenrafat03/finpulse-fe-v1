import { IQuestion, ResponseType } from "../interfaces/section.interface";
import React, { useState, useEffect } from "react";

import {
  Checkbox,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";
import { isQuestionAnswered } from "../utils/questionaire";

const Question = ({
  sectionId,
  question,
  index,
  onResponseChange,
  saveClicked,
  isDisabled,
  isDivVisible,
  toggleVisibility,
}: IQuestionProps) => {
  const multiAnswerOptions = () => {
    return question.options?.map((option) => {
      question.response = (question.response ?? []) as string[];
      return (
        <FormControlLabel
          className="question-option"
          key={`${sectionId}-${question.id}-${option.id}`}
          disabled={isDisabled}
          control={
            <Checkbox
              value={option.id}
              checked={question.response.includes(option.id)}
              onChange={(evt, checked) => {
                console.log("evt", evt);
                const currentResponse: string[] = question.response as string[];
                if (checked) {
                  currentResponse.push(option.id);
                } else {
                  currentResponse.splice(currentResponse.indexOf(option.id), 1);
                }
                onResponseChange();
              }}
            />
          }
          label={option.option}
        />
      );
    });
  };

  const singleAnswerOptions = () => {
    return (
      <FormControl>
        <RadioGroup name="options-group" value={question.response}>
          {question.options?.map((option) => {
            return (
              <FormControlLabel
                disabled={isDisabled}
                key={`${sectionId}-${question.id}-${option.id}`}
                value={option.id}
                control={
                  <Radio
                    sx={{
                      "&.Mui-checked": {
                        color: "#2d807b",
                      },
                    }}
                  />
                }
                label={option.option}
                className={`${
                  "question-option " +
                  (saveClicked && !isQuestionAnswered(question)
                    ? "text-red-700"
                    : "")
                }`}
                onChange={(evt, checked) => {
                  console.log("evt", evt);
                  if (checked) {
                    question.response = option.id;
                  }
                  onResponseChange();
                }}
              />
            );
          })}
        </RadioGroup>
      </FormControl>
    );
  };

  const freeTextAnswer = () => {
    return (
      <TextField
        disabled={isDisabled}
        name="Response"
        label="Response*"
        variant="outlined"
        margin="normal"
        value={question.response ?? ""}
        onChange={(
          event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>
        ) => {
          question.response = event.currentTarget.value.trimStart();
          onResponseChange();
        }}
      />
    );
  };

  const [isDivLocalVisible, setDivLocalVisibility1] = useState(false);

  const toggleLocalVisibility = () => {
    console.log(isDivVisible);
    console.log(isDivLocalVisible);
    setDivLocalVisibility1(!isDivLocalVisible);
  };

  useEffect(() => {
    setDivLocalVisibility1(isDivVisible);
  }, [isDivVisible]);

  return (
    <div className="section-question">
      <div
        className={`${
          saveClicked && !isQuestionAnswered(question) ? "text-red-700" : ""
        }`}
      >
        <span>Question {index}</span>
        <div className="text-xl">{question.question}</div>
        {/* <p className="font-bold text-black">{question.question}</p> */}
        <h2
          className="text-sm text-ngreen-800 font-normal cursor-pointer mt-3"
          onClick={toggleLocalVisibility}
        >
          {isDivLocalVisible ? "Hide Description" : "Show Description"}
        </h2>
        <h2
          className={`bg-gray-100 rounded-lg p-3 mt-2 text-sm ${
            isDivLocalVisible ? "block" : "hidden"
          }`}
        >
          {question.questiondescription}
        </h2>
      </div>
      <div
        className={`${
          saveClicked && !isQuestionAnswered(question) ? "text-red-700" : ""
        }`}
      >
        <div className="font-bold">
          {index}. {question.question}
        </div>
      </div>
      {question.responseType === ResponseType.MULTI_OPTION &&
        multiAnswerOptions()}
      {question.responseType === ResponseType.SINGLE_OPTION &&
        singleAnswerOptions()}
      {question.responseType === ResponseType.FREE_TEXT && freeTextAnswer()}
    </div>
  );
};

export { Question };

interface IQuestionProps {
  isDivVisible: boolean;
  toggleVisibility: () => void;
  sectionId: string;
  index: number;
  question: IQuestion;
  saveClicked: boolean;
  onResponseChange: () => void;
  isDisabled?: boolean;
}
