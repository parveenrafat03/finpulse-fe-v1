import { ReactNode } from "react";

const SectionCore = ({ isSubsection, children }: ISectionCoreProps) => {
  const divClasses = isSubsection ? "mt-8 ml-5" : "";
  const olClasses = isSubsection
    ? "relative border-ngray-300"
    : "relative border-l border-ngray-300";
  return (
    <div>
      <div className={divClasses}>
        <ol className={olClasses}>{children}</ol>
      </div>
    </div>
  );
};

export { SectionCore };

interface ISectionCoreProps {
  isSubsection?: boolean;
  children: ReactNode[];
}
