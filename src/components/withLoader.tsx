import { useState, ComponentType, useCallback } from "react";
import { Loader } from "./Loader";

export interface ILoaderState {
  show: boolean;
  message?: string;
}

export interface IWithLoaderProps {
  showLoader?: (_: ILoaderState) => void;
}

const withLoader = <T extends object>(
  Component: ComponentType<T>,
): React.FC<T & IWithLoaderProps> => {
  const HOC = (props: IWithLoaderProps) => {
    const [loaderState, setLoaderState] = useState<ILoaderState>({
      show: false,
    });

    const showLoader = useCallback((state: ILoaderState) => {
      setLoaderState(state);
    }, []);

    return (
      <>
        {loaderState.show && <Loader message={loaderState.message} />}
        <Component {...(props as T)} showLoader={showLoader} />
      </>
    );
  };

  return HOC;
};

export { withLoader };
