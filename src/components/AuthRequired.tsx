import { ReactNode } from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../hooks";
import { Role } from "../typings";

interface AuthProps {
  roles: Role | Role[];
  children: ReactNode;
}

const AuthRequired = ({ roles, children }: AuthProps) => {
  const auth = useAuth();

  if (!auth.client) {
    return <Navigate to="/" />;
  }

  if (!roles.includes(auth.client.roles)) {
    return <Navigate to="/unauthorized" />;
  }

  return children;
};

export { AuthRequired };
