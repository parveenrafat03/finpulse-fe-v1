import { faCompass } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Loader = ({ message = "Loading.." }: { message?: string }) => (
  <>
    <div className="fixed top-0 left-0 bottom-0 right-0 bg-black bg-opacity-80 z-20"></div>
    <div className="fixed w-full top-[45%] ml-auto z-30 text-center">
      <FontAwesomeIcon
        icon={faCompass}
        spin
        className="text-ngreen-400 text-6xl"
      />
      <div className="mt-3 text-white">{message}</div>
    </div>
  </>
);

export { Loader };
