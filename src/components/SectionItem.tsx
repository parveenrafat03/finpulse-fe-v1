import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck as checkCircle } from "@fortawesome/free-solid-svg-icons/faCircleCheck";
import { faCircle as blankCircle } from "@fortawesome/free-regular-svg-icons/faCircle";
import { faCircle as solidCircle } from "@fortawesome/free-solid-svg-icons/faCircle";
import { ISection, SectionState } from "../interfaces/section.interface";
import { Section } from "./Section";
import {
  faChevronDown,
  faChevronUp,
  faCircleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import "../pages-css/profile.css";
const SectionItem = ({
  id,
  title,
  state,
  isActive,
  subSections,
  isSubsectionItem,
  activeSubSectionId,
  submitAttempted,
  subSectionClicked,
  mainSectionClicked,
  titleClick,
}: ISectionItemProps) => {
  return (
    <li className="mb-8 ml-6">
      <span className="absolute flex items-center justify-center w-6 h-7 rounded-full -left-3 -mt-1">
        {state === SectionState.FINISHED && (
          <FontAwesomeIcon icon={checkCircle} className="text-ngreen-600" />
        )}
        {state === SectionState.IN_PROGRESS && (
          <div className="text-ngreen-600 profile-check " />
        )}
        {state === SectionState.PENDING && (
          <FontAwesomeIcon
            icon={solidCircle}
            className="text-ngray-400 bg-white"
          />
        )}
        {state === SectionState.PARTIALLY_FINISHED && (
          <FontAwesomeIcon
            icon={faCircleExclamation}
            className="text-nyellow-600"
          />
        )}
      </span>
      <div
        onClick={() => {
          titleClick(id);
        }}
        className={`mb-1 text-lg font-normal cursor-pointer ${
          isActive ? " " : ""
        } ${
          state === SectionState.FINISHED ? "text-ngreen-600 font-semibold" : ""
        } ${state === SectionState.PENDING ? "text-nred-500" : ""} ${
          state === SectionState.PARTIALLY_FINISHED
            ? "text-nyellow-600 font-semibold"
            : ""
        }`}
      >
        <span
          className={
            isSubsectionItem
              ? "absolute w-9 h-px border-ngray-300 border-t -left-11 mt-3"
              : ""
          }
        ></span>
        <div className="flex section-option">
          <div
            className={
              "w-72 " +
              (submitAttempted && state !== SectionState.FINISHED
                ? "text-red-500"
                : "")
            }
          >
            {title}
          </div>
          <div className="w-1">
            {subSections?.length ? (
              isActive ? (
                <FontAwesomeIcon
                  icon={faChevronUp}
                  className="text-ngreen-600 text-right"
                />
              ) : (
                <FontAwesomeIcon
                  icon={faChevronDown}
                  className="text-black text-right"
                />
              )
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="clear-both"></div>
      </div>
      {subSections?.length && isActive && (
        <Section
          submitAttempted={submitAttempted}
          activeSectionId={activeSubSectionId}
          sectionItems={subSections}
          isSubsection={true}
          onActiveSubSectionClicked={subSectionClicked}
          onActiveMainSectionClicked={mainSectionClicked}
        />
      )}
    </li>
  );
};

export { SectionItem };

interface ISectionItemProps extends ISection {
  submitAttempted: boolean;
  isActive: boolean;
  state: SectionState;
  isSubsectionItem?: boolean;
  activeSubSectionId?: string;
  subSectionClicked: (subSection: ISection) => void;
  mainSectionClicked: (section: ISection) => void;
  titleClick: (id: string) => void;
}
