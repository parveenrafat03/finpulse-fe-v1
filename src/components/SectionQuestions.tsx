import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IQuestion, ISection } from "../interfaces/section.interface";
import { Question } from "./Question";
import {
  faArrowLeft,
  faFloppyDisk,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

const SectionQuestions = ({
  assessmentType,
  sectionId,
  sectionDescription,
  questions,
  sectionsClicked,
  isLastSection,
  sectionStateChange,
  saveResponses,
  back,
  next,
  exit,
  isDisabled,
  sectionTitle,
  isDivVisible,
  toggleVisibility,
  sectionItems,
  allSectionsCompleted,
}: ISectionQuestionProps) => {
  const questionResponseChanged = () => {
    sectionStateChange();
  };

  const getBtnName = (): string => {
    if (isDisabled) {
      return isLastSection ? `Close` : `Next`;
    } else {
      return isLastSection && allSectionsCompleted(sectionItems)
        ? `Submit`
        : `Save & Next`;
    }
  };
  const questionComponents = questions.map((question, index) => {
    return (
      <Question
        sectionId={sectionId}
        key={index + 1}
        index={index + 1}
        question={question}
        saveClicked={sectionsClicked.includes(sectionId)}
        onResponseChange={questionResponseChanged}
        isDisabled={isDisabled}
        isDivVisible={isDivVisible}
        toggleVisibility={toggleVisibility}
      />
    );
  });
  const navigate = useNavigate();

  return (
    <div className="">
      <div className="font-bold text-base">{sectionTitle}</div>
      <h1 className="text-sm opacity-70 font-thin pt-2 pb-6">
        {assessmentType === "discovery" ? (
          <>
            <div className="inline font-bold opacity-50">ⓘ</div> Please provide
            details filling up the form below and continue your discovery
            journey
          </>
        ) : (
          <></>
        )}
      </h1>
      {sectionDescription && (
        <div className="bg-ngray-100 rounded-md section-description text-sm">
          {sectionDescription}
        </div>
      )}
      {questionComponents}
      <div
        className="shadow-lg section-foot-menu"
        style={{ boxShadow: "0px 0px 12px 0px #0000001A" }}
      >
        <button
          className="float-left px-5 py-2 rounded-md bg-ngray-100 text-black text-xl"
          onClick={() => {
            back();
          }}
        >
          <FontAwesomeIcon icon={faArrowLeft} />
          <span className="ml-2">Back</span>
        </button>

        <div className="float-right">
          {!(isDisabled && isLastSection) && (
            <button
              className="px-5 py-2 rounded-md bg-ngray-100 text-black text-xl mr-10"
              onClick={() => {
                exit();
              }}
            >
              <span className="ml-2">Exit</span>
            </button>
          )}

          <button
            className="px-5 py-2 rounded-md bg-ngreen-400 text-black text-xl"
            onClick={() => {
              const updatedSectionClicked = sectionsClicked.slice();
              if (!updatedSectionClicked.includes(sectionId)) {
                updatedSectionClicked.push(sectionId);
              }
              if (isDisabled) {
                if (isLastSection) {
                  exit();
                } else {
                  next();
                }
              } else {
                const name = getBtnName();
                saveResponses(isLastSection);
                // if (getBtnName() === "Submit") {
                //   navigate("/discovery-thank-you", { replace: true });
                // }
              }
            }}
          >
            <span className="ml-2 text-white">{getBtnName()}</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export { SectionQuestions };

interface ISectionQuestionProps {
  assessmentType: string;
  sectionTitle: string;
  isDivVisible: boolean;
  toggleVisibility: () => void;
  sectionId: string;
  sectionDescription: string;
  questions: IQuestion[];
  isLastSection: boolean;
  sectionStateChange: () => void;
  saveResponses: (isSubmit: boolean) => void;
  back: () => void;
  next: () => void;
  exit: () => void;
  sectionsClicked: string[];
  isDisabled: boolean;
  sectionItems: ISection[];
  allSectionsCompleted: (items: ISection[]) => boolean;
}
