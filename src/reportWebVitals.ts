import {
  onCLS,
  onFID,
  onLCP,
  onFCP,
  onTTFB,
  CLSReportCallback,
  FIDReportCallback,
  FCPReportCallback,
  LCPReportCallback,
  TTFBReportCallback,
} from "web-vitals";

const reportWebVitals = (
  callback:
    | CLSReportCallback
    | FIDReportCallback
    | FCPReportCallback
    | LCPReportCallback
    | TTFBReportCallback,
) => {
  onCLS(callback as CLSReportCallback);
  onFID(callback as FIDReportCallback);
  onFCP(callback as FCPReportCallback);
  onLCP(callback as LCPReportCallback);
  onTTFB(callback as TTFBReportCallback);
};

export { reportWebVitals };
