export enum Role {
  USER = "user",
  ADMIN = "admin",
}

export enum Mode {
  VIEW = "view",
  EDIT = "edit",
}
