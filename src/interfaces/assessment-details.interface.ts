export enum AssessmentStatus {
  IN_PROGRESS = "IN_PROGRESS",
  DONE = "DONE",
  COMPLETED = "COMPLETED",
}

export enum AssessmentType {
  DISCOVERY = "discovery",
  MATURITY = "maturity",
}

export interface IAssessmentDetails {
  assessment_id: string;
  //assessment_name?: string;
  assessment_name: string;
  assessment_status: AssessmentStatus;
  client_id: string;
  created_date: string;
  last_modified_date: string;
  previousAssessmentId?: string;
}

export interface ICapabilityScore {
  Capability_ID: string;
  Capability_Score: string | number;
}

export interface IDomain {
  DOMAIN_ID: string;
  Domain_Capability_Scores: ICapabilityScore[];
  Domain_Score: number;
}
export interface IRecommendation {
  assessment_id: string;
  capability_id: string;
  client_id: string;
  last_modified_date: string;
  recommendation: string;
  recommendation_number: string;
}

export interface ISurveyDetails {
  assessment_id: string;
  assessment_name: string;
  assessment_status: string;
  client_id: string;
  created_date: string;
  last_modified_date: string;
}
