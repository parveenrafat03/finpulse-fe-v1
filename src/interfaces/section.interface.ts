export interface ISection {
  id: string;
  title: string;
  description: string;
  questions?: IQuestion[];
  subSections?: ISection[];
}

export interface IQuestion {
  id: string;
  title: string;
  question: string;
  questiondescription: string;
  options?: IOption[];
  responseType: ResponseType;
  response: IQuestionResponse;
}

interface IOption {
  id: string;
  option: string;
}

export type IQuestionResponse = string | string[] | Nullable;
type Nullable = null | undefined;

export enum ResponseType {
  SINGLE_OPTION,
  MULTI_OPTION,
  FREE_TEXT,
}

export enum SectionState {
  PENDING,
  PARTIALLY_FINISHED,
  IN_PROGRESS,
  FINISHED,
}
