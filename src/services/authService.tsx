import { CONFIG } from "../utils";

const storeData = (key: string, value: any) => {
  localStorage.setItem(key, value);
};

const retrieveData = (key: string): any => {
  return localStorage.getItem(key);
};

const removeData = (key: string) => {
  localStorage.removeItem(key);
};

export const setLastLogin = (timestamp: Number) => {
  storeData(CONFIG.LOCAL_STORAGE_LAST_LOGIN, JSON.stringify(timestamp));
};

export const getLastLogin = (): Number => {
  const timestamp = retrieveData(CONFIG.LOCAL_STORAGE_LAST_LOGIN);
  if (timestamp !== null) {
    return JSON.parse(timestamp);
  }
  return 0;
};

export const setLastActivity = (timestamp: Number) => {
  storeData(CONFIG.LOCAL_STORAGE_LAST_ACTIVITY, JSON.stringify(timestamp));
};

export const getLastActivity = (): Number => {
  const timestamp = retrieveData(CONFIG.LOCAL_STORAGE_LAST_ACTIVITY);
  if (timestamp !== null) {
    return JSON.parse(timestamp);
  }
  return 0;
};
