/*
This is a custom hook which abstracts various aspects of form handling e.g. form field updates, validations etc. It expects
FormConfig<T> as input and returns FormContext<T> where T is an object for consisting the form fields.
 
Note:
  - Each input field should have a "name" attribute same as the one defined in T object.
  - Validations are optional and are hendled using yup schema validation library. Please pass ObjectSchema<Partial<T>> in FormConfig<T>.
 
Please use Login.tsx as reference.
*/

import { SelectChangeEvent } from "@mui/material";
import React, { ChangeEventHandler, ReactNode, useState } from "react";
import { ObjectSchema, ValidationError } from "yup";

interface IFormConfig<T> {
  initialFormData: T;
  onSubmit: (_: T) => Promise<void>;
  validations?: ObjectSchema<Partial<T>>;
  onValidation?: (_: T, __?: string) => boolean;
}

type FormErrors<T> = {
  [key in keyof T]?: boolean;
};

interface IFormContext<T> {
  values: T;
  formErrors: FormErrors<T>;
  handleChange: (
    _: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  handleSelectChange: (_: React.FormEvent<HTMLSelectElement>) => void;

  handleSubmit: (_: React.FormEvent<HTMLFormElement>) => Promise<void>;
  setFormData: (data: T) => void;
}

const useForm = <T extends object>(
  formConfig: IFormConfig<T>
): IFormContext<T> => {
  const [formData, setFormData] = useState<T>(formConfig.initialFormData);
  const [formErrors, setFormErrors] = useState<FormErrors<T>>({});

  const validateForm = (values: T, field?: string): boolean => {
    let errors: FormErrors<T>;

    if (field) {
      const { [field as keyof FormErrors<T>]: _, ...rest } = formErrors;
      errors = rest as FormErrors<T>;
    } else {
      errors = {
        ...formErrors,
      };
    }

    if (formConfig.validations) {
      try {
        field
          ? formConfig.validations.validateSyncAt(field, values, {
              abortEarly: false,
            })
          : formConfig.validations.validateSync(values, { abortEarly: false });
      } catch (error: unknown) {
        if (error instanceof ValidationError) {
          errors = error.inner.reduce(
            (acc: FormErrors<T>, error: ValidationError) => {
              if (error.path) {
                return {
                  ...acc,
                  [error.path]: true,
                };
              }
              return acc;
            },
            errors
          );
        }
      }
    }

    setFormErrors(errors);

    return !Object.keys(errors).some((key: string) => errors[key as keyof T]);
  };

  return {
    values: formData,
    formErrors: formErrors,
    handleChange: (
      e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
      const updatedFormData: T = {
        ...formData,
        [e.currentTarget.name]: e.currentTarget.value,
      };
      setFormData(updatedFormData);
      validateForm(updatedFormData, e.currentTarget.name);
    },
    handleSelectChange: (e: React.FormEvent<HTMLSelectElement>) => {
      const updatedFormData: T = {
        ...formData,
        [e.currentTarget.name]: e.currentTarget.value,
      };
      setFormData(updatedFormData);
      validateForm(updatedFormData, e.currentTarget.name);
    },
    setFormData: setFormData,
    handleSubmit: async (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      const isFormValid = validateForm(formData);
      if (isFormValid) {
        await formConfig.onSubmit(formData);
      }
    },
  };
};

export { useForm, type IFormContext as FormContext };
