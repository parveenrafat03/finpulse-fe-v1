import { ReactNode, createContext, useMemo, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from ".";
import { CONFIG } from "../utils";
import toast from "react-hot-toast";
import { Role } from "../typings";
import { getConfiguration } from "../apis/clientAPI";
import * as authService from "../services/authService";

export interface ILoginForm {
  username: string;
  password: string;
}

export interface IClientInfo {
  client_id: string;
  roles: Role;
}

export interface IAuthContextType {
  userId: string;
  client: IClientInfo | null;
  login: (_: ILoginForm) => Promise<void>;
  logout: () => void;
  setClientId: (_: string) => void;
  isSessionValid: () => boolean;
  markLastActivity: () => void;
}

const AuthContext = createContext<IAuthContextType | null>(null);

const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [userId, setUserId] = useState<string>(
    "" + localStorage.getItem("user_id")
  );
  const [client, setClient] = useLocalStorage<IClientInfo | null>(
    CONFIG.LOCAL_STORAGE_CLIENT_KEY,
    null
  );
  const navigate = useNavigate();

  const login = async (data: ILoginForm) => {
    const API_URL = (await getConfiguration()).api;
    const response = await fetch(API_URL.login, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      toast.error("Failed to login. Please try after some time.");
    } else {
      const responseData = await response.json();
      if (responseData.statusCode !== 200) {
        toast.error(
          "Failed to login. Please try with correct username/password!"
        );
      } else {
        const clientInfo = responseData.body as IClientInfo;
        setClient(clientInfo);
        setUserId(clientInfo.client_id);
        localStorage.setItem("user_id", clientInfo.client_id);
        authService.setLastLogin(getCurrentTimeStamp());
        authService.setLastActivity(getCurrentTimeStamp());
        toast.success("Logged in successfully!");
        navigate("/", { replace: true });
      }
    }
  };

  const logout = () => {
    setUserId("");
    localStorage.setItem("user_id", "");
    setClient(null);
    sessionStorage.clear();
    navigate("/", { replace: true });
  };

  const setClientId = (clientId: string) => {
    client!.client_id = clientId;
    setClient(client);
  };

  const isSessionValid = (): boolean => {
    const current = getCurrentTimeStamp();
    const idleTime = getMinutes(authService.getLastActivity(), current);
    const totalTime = getMinutes(authService.getLastLogin(), current);
    // console.log("idleTime => " + idleTime);
    // console.log("idleTime => " + idleTime);
    // console.log("totalTime => " + totalTime);
    return idleTime < CONFIG.IDLE_TIME && totalTime < CONFIG.TOTAL_TIME;
  };

  const markLastActivity = () => {
    // console.log(authService.getLastActivity());
    // console.log(authService.getLastLogin());
    authService.setLastActivity(getCurrentTimeStamp());
  };

  const getCurrentTimeStamp = (): Number => {
    return Math.floor(Date.now() / 1000);
  };

  const getMinutes = (timestamp1: Number, timestamp2: Number): number => {
    const differenceInSeconds = Math.abs(
      timestamp1.valueOf() - timestamp2.valueOf()
    );
    const differenceInMinutes = Math.floor(differenceInSeconds / 60);
    return differenceInMinutes;
  };

  const value: IAuthContextType = useMemo(
    () => ({
      userId,
      client,
      login,
      logout,
      setClientId,
      isSessionValid,
      markLastActivity,
    }),
    // TO-DO: Fix this
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [client]
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

const useAuth = () => {
  // TO-DO: Fix this
  // eslint-disable-next-line @typescript-eslint/non-nullable-type-assertion-style
  return useContext(AuthContext) as IAuthContextType;
};

// TO-DO: Fix this
// eslint-disable-next-line react-refresh/only-export-components
export { useAuth, AuthProvider };
