import { useState } from "react";

const useLocalStorage = <T extends object | null>(
  key: string,
  defaultValue: T
): [T | null, React.Dispatch<T>] => {
  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const value = localStorage.getItem(key);
      if (value) {
        return JSON.parse(value) as T;
      } else {
        localStorage.setItem(key, JSON.stringify(defaultValue));
        return defaultValue;
      }
    } catch (err) {
      return defaultValue;
    }
  });

  const setValue = (newValue: T) => {
    try {
      localStorage.setItem(key, JSON.stringify(newValue));
    } catch (err) {
      console.log(`Failed to save item: ${key} in local storage`);
    }
    setStoredValue(newValue);
  };

  return [storedValue, setValue];
};

export { useLocalStorage };
