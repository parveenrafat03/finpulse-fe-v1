import { useEffect, useCallback, useRef } from "react";
import {
  useBeforeUnload,
  unstable_useBlocker as useBlocker,
} from "react-router-dom";

export const FormPrompt = ({ hasUnsavedChanges }: IUseConfirmProps) => {
  const onLocationChange = useCallback(() => {
    if (hasUnsavedChanges) {
      return !window.confirm(
        "You have unsaved changes, are you sure you want to leave?",
      );
    }
    return false;
  }, [hasUnsavedChanges]);

  usePrompt(onLocationChange, hasUnsavedChanges);
  useBeforeUnload(
    useCallback(
      (event) => {
        if (hasUnsavedChanges) {
          event.preventDefault();
          event.returnValue = "";
        }
      },
      [hasUnsavedChanges],
    ),
    { capture: true },
  );

  return null;
};

function usePrompt(
  onLocationChange: () => boolean,
  hasUnsavedChanges: boolean,
) {
  const blocker = useBlocker(hasUnsavedChanges ? onLocationChange : false);
  const prevState = useRef(blocker.state);

  useEffect(() => {
    if (blocker.state === "blocked") {
      blocker.reset();
    }
    prevState.current = blocker.state;
  }, [blocker]);
}

interface IUseConfirmProps {
  hasUnsavedChanges: boolean;
}

export interface IConfirmationDialogProp {
  title: string;
  description: string;
  hasUnsavedChanges: boolean;
  confirmButton: {
    label: string;
    handler: () => void;
  };
  cancelButton: {
    label: string;
    handler: () => void;
  };
}
