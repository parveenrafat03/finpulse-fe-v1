import { useEffect } from "react";
import { unstable_useBlocker as useBlocker } from "react-router-dom";
import { useConfirm } from "material-ui-confirm";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  customDialog: {
    padding: "14px",
  },
  customDialogTitle: {
    color: "#13294B",
    fontFamily: "Inter",
    fontWeight: 600,
  },
  customDialogContent: {
    color: "#000000",
    fontFamily: "Inter",
    fontWeight: 500,
    lineHeight: "48px",
  },
  customDialogActions: {
    justifyContent: "right",
  },
  customOkButton: {
    color: "#13294B",
    backgroundColor: "#EFF1F4",
    "&:hover": {
      backgroundColor: "#EFF1F4",
      opacity: 0.8,
    },
  },
  customCancelButton: {
    color: "#FFFFFF",
    backgroundColor: "#3AAB94",
    "&:hover": {
      backgroundColor: "#3AAB94",
      opacity: 0.8,
    },
  },
}));

export const usePrompt = ({ when }: { when: boolean }) => {
  const confirm = useConfirm();
  const blocker = useBlocker(when);
  const classes = useStyles();

  useEffect(() => {
    if (blocker.state === "blocked") {
      if (when) {
        confirm({
          title: "You’re about to exit",
          description:
            "You have unsaved changes. Click CANCEL to continue answering the survey or OK to discard all the unsaved changes and move away.",
          dialogProps: { classes: { paper: classes.customDialog } },
          titleProps: { className: classes.customDialogTitle },
          contentProps: { className: classes.customDialogContent },
          dialogActionsProps: { className: classes.customDialogActions },
          confirmationButtonProps: { className: classes.customOkButton },
          cancellationButtonProps: { className: classes.customCancelButton },
        })
          .then(() => {
            // This timeout is needed to avoid a weird "race" on POP navigations
            // between the `window.history` revert navigation and the result of
            // `window.confirm`
            //
            // eslint-disable-next-line @typescript-eslint/unbound-method
            setTimeout(blocker.proceed, 0);
          })
          .catch(() => {
            blocker.reset();
          });
      } else {
        blocker.proceed();
      }
    }
  }, [blocker, confirm, when, classes]);

  useEffect(() => {
    if (blocker.state === "blocked" && !when) {
      blocker.reset();
    }
  }, [blocker, when]);
};
