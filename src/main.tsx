import React from "react";
import ReactDOM from "react-dom/client";
import { App } from "./App";
import "./index.css";
import { reportWebVitals } from "./reportWebVitals";

const rootElement = document.getElementById("root");

if (!rootElement) throw new Error("No element found with id 'root'");

ReactDOM.createRoot(rootElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);

reportWebVitals(console.log);
