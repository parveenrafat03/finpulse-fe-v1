# Finops Maturity Assessment

This is a React app for Finops maturity assessment tool.

## Get Started

To start the application please run the below commands.

```
npm install
npm run dev
```
